import React, { Component } from 'react';

import { connect } from 'react-redux';
import { ToastContainer } from 'react-toastify';
import Loading from 'react-loading-bar';
import ValidationMessage from './components/popups/ValidationMessage';

class App extends Component {
  componentDidCatch(error, info) {
    console.log(error);
  }

  componentDidUpdate(prevProps, prevState) {
    if (this.props.location !== prevProps.location) {
      document.documentElement.scrollTop = 0;
    }
  }

  render() {
    return (
      <React.Fragment>
        <ValidationMessage />
        <ToastContainer />
        <Loading
          show={this.props.loadingBar.show}
          color="#ff5e3a"
          showSpinner={false}
        />
        {this.props.children}
      </React.Fragment>
    );
  }
}

export default connect(state => ({ loadingBar: state.loadingBar }))(App);
