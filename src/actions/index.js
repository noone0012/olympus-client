export const toggleValidationMessageState = (message = null) => ({
  type: 'TOGGLE_VALIDATION_MESSAGE_STATE',
  payload: { message }
});

export const toggleChatState = (open, user, viewer) => ({
  type: 'TOGGLE_CHAT_STATE',
  payload: { open, user, viewer }
});
