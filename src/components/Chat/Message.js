import React, { Component } from 'react';
import moment from 'moment';
import { createFragmentContainer, graphql } from 'react-relay';

class Message extends Component {
  date: String;
  amISender: Boolean;

  componentWillMount() {
    const { message: { createdAt, from, to }, user, viewer } = this.props;

    const time = new Date(createdAt).getTime();
    this.date = moment(time).format('h:mm a');

    if (viewer.id === from && user.id === to) {
      this.amISender = true;
    } else if (viewer.id === to && user.id === from) {
      this.amISender = false;
    }
  }

  render() {
    const { message, user } = this.props;

    if (typeof this.amISender !== 'boolean') {
      return null;
    }

    return (
      <li className={this.amISender ? 'me' : 'talker'}>
        {!this.amISender && (
          <div className="author-thumb">
            <img
              src={user.profilePhoto}
              alt="author"
              className="mCS_img_loaded"
            />
          </div>
        )}
        <div className="notification-event">
          <span className="chat-message-item">{message.text}</span>
          <span className="notification-date">
            <time className="entry-date updated">{this.date}</time>
          </span>
        </div>
      </li>
    );
  }
}

export default createFragmentContainer(
  Message,
  graphql`
    fragment Message_message on Message {
      id
      text
      unread
      createdAt
      from
      to
    }
  `
);
