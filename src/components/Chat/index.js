import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'found';
import { Transition } from 'semantic-ui-react';
import { graphql, createPaginationContainer } from 'react-relay';
import { throttle } from 'lodash';

import Message from './Message';
import { toggleChatState } from '../../actions';
import SendMessageMutation from '../../graphql/mutations/SendMessageMutation';
import SendTypingNotificationMutation from '../../graphql/mutations/SendTypingNotificationMutation';
import NewMessageSubscription from '../../graphql/subscriptions/NewMessageSubscription';
import IsTypingSubscription from '../../graphql/subscriptions/IsTypingSubscription';
import getAllowableCodes from '../../utils/getAllowableCodes';

const { PUBLIC_URL } = process.env;

class Chat extends Component {
  state = { isTyping: false };

  chatElement: HTMLElement;
  chatScrollable: HTMLElement;
  textArea: HTMLElement;
  typingTimeoutID;
  allowableCodes = getAllowableCodes();

  onKeyDown = e => {
    if (e.key === 'Escape') {
      this.props.closeChat();
    }
  };

  dragMouseDown = e => {
    // clicked on close button or user's full name
    if (e.target.closest('.close-block') || e.target.closest('.full-name')) {
      return;
    }

    this.previousClientX = e.clientX;
    this.previousClientY = e.clientY;

    document.onmouseup = this.closeDragElement;
    document.onmousemove = this.elementDrag;
  };

  closeDragElement = () => {
    document.onmouseup = null;
    document.onmousemove = null;
  };

  elementDrag = e => {
    const movedX = this.previousClientX - e.clientX;
    const movedY = this.previousClientY - e.clientY;

    this.previousClientX = e.clientX;
    this.previousClientY = e.clientY;

    const el = this.chatElement;

    el.style.left = el.offsetLeft - movedX + 'px';
    el.style.top = el.offsetTop - movedY + 'px';
  };

  sendMessage = async text => {
    await SendMessageMutation.commit(
      this.props.relay.environment,
      {
        text,
        receiverId: this.props.chat.user.id
      },
      this.props.root.id
    );

    this.scrollToBottom();
    this._notifyAboutTyping.cancel();
  };

  handleTextAreaKeyDown = e => {
    if (e.key === 'Enter') {
      if (e.shiftKey) {
        e.target.value = e.target.value + '\n';
        return;
      }

      e.preventDefault();
      const message = e.target.value.trim();

      if (message) {
        this.sendMessage(message);
        this.textArea.value = '';
      }
    }

    if (this.allowableCodes.includes(e.keyCode)) {
      this._notifyAboutTyping();
    }
  };

  handleScroll = e => {
    const { isLoading, hasMore } = this.props.relay;

    if (isLoading() || !hasMore()) {
      return;
    }

    const chat = e.target;
    const scrollable = chat.scrollHeight - chat.clientHeight;
    const scrolled = chat.scrollTop;

    if (scrolled / scrollable * 100 < 25) {
      this._loadMore();
    }
  };

  _loadMore = throttle(() => this.props.relay.loadMore(10), 1000);
  _notifyAboutTyping = throttle(() => {
    SendTypingNotificationMutation.commit(this.props.relay.environment, {
      userId: this.props.chat.user.id
    });
  }, 2000);

  setTyping = () => {
    if (this.state.isTyping) {
      clearTimeout(this.typingTimeoutID);
      this.setTimeoutToClearTyping();
      return;
    }

    this.setState({ isTyping: true });
    this.setTimeoutToClearTyping();
  };

  setTimeoutToClearTyping = (timer = 2500) => {
    this.typingTimeoutID = setTimeout(() => {
      this.setState({ isTyping: false });
    }, timer);
  };

  clearTyping = () => {
    this.setState({ isTyping: false });
    clearTimeout(this.typingTimeoutID);
  };

  scrollToBottom = () => {
    if (!this.chatScrollable) {
      return;
    }

    this.chatScrollable.scrollTop = this.chatScrollable.scrollHeight;
  };

  componentDidUpdate(prevProps, prevState) {
    const { chat } = this.props;

    // chat is closed
    if (!chat.open) {
      document.removeEventListener('keydown', this.onKeyDown);
      this.newMessageDisposable.dispose();
      this.isTypingDisposable.dispose();
      return;
    }

    if (chat.user.id === prevProps.chat.user.id) {
      return;
    }

    document.addEventListener('keydown', this.onKeyDown);

    this.props.relay.refetchConnection(10, () => {
      this.scrollToBottom();
    });

    this.newMessageDisposable = NewMessageSubscription.request(
      this.props.relay.environment,
      { userId: chat.user.id },
      this.props.root.id,
      () => {
        this.clearTyping();
        this.scrollToBottom();
      }
    );

    this.isTypingDisposable = IsTypingSubscription.request(
      this.props.relay.environment,
      { userId: chat.user.id },
      () => {
        this.setTyping();
        this.scrollToBottom();
      }
    );
  }

  render() {
    const {
      closeChat,
      chat: { open, user = {}, viewer = {} },
      root: { messages }
    } = this.props;

    return (
      <Transition
        visible={open}
        onShow={() => {
          this.textArea && this.textArea.focus();
        }}
        animation="fade down"
        duration={200}
        unmountOnHide
      >
        <div
          className={`ui-block popup-chat popup-chat-responsive`}
          ref={node => {
            this.chatElement = node;
          }}
        >
          <div className="chat-header" onMouseDown={this.dragMouseDown}>
            <div>
              <span className="icon-status online" />
              <h6 className="title">
                <Link to={`/${user.id}`} className="full-name">
                  {user.fullName}
                </Link>
              </h6>
            </div>
            <div className="close-block" onClick={closeChat}>
              <svg className="olymp-little-delete">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                />
              </svg>
            </div>
          </div>
          <div className="chat-message-block">
            {messages.edges.length ? (
              <ul
                className="notification-list chat-message chat-message-field"
                ref={node => {
                  this.chatScrollable = node;
                }}
                onScroll={this.handleScroll}
              >
                {messages.edges.map(message => (
                  <Message
                    key={message.node.id}
                    message={message.node}
                    user={user}
                    viewer={viewer}
                  />
                ))}
              </ul>
            ) : (
              <p className="no-messages">You've had no messages yet</p>
            )}
            {this.state.isTyping && (
              <p style={{ margin: '8px 0 10px 8px' }}>
                {user.firstName} is typing...
              </p>
            )}
          </div>
          <form>
            <div className="form-group">
              <textarea
                className="form-control chat-input"
                placeholder="Type a message..."
                ref={node => {
                  this.textArea = node;
                }}
                onKeyDown={this.handleTextAreaKeyDown}
              />
            </div>
          </form>
        </div>
      </Transition>
    );
  }
}

const ChatwithPagination = createPaginationContainer(
  Chat,
  {
    root: graphql`
      fragment Chat_root on Root
        @argumentDefinitions(
          count: { type: "Int", defaultValue: 10 }
          cursor: { type: "String" }
          userId: { type: "ID!", defaultValue: "VXNlcjoy" }
        ) {
        messages(last: $count, before: $cursor, userId: $userId)
          @connection(key: "Chat_messages") {
          edges {
            cursor
            node {
              id
              ...Message_message
            }
          }
        }
        id
      }
    `
  },
  {
    direction: 'backward',
    getConnectionFromProps(props) {
      return props.root && props.root.messages;
    },
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount
      };
    },
    getVariables(props, { count, cursor }, fragmentVariables) {
      fragmentVariables.userId = props.chat.user.id;

      return {
        count,
        cursor,
        userId: props.chat.user.id
      };
    },
    query: graphql`
      query ChatPaginationQuery($count: Int!, $cursor: String, $userId: ID!) {
        root {
          ...Chat_root
            @arguments(count: $count, cursor: $cursor, userId: $userId)
        }
      }
    `
  }
);

export default connect(state => ({ chat: state.chat }), {
  closeChat: () => toggleChatState(false)
})(ChatwithPagination);
