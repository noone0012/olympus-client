import React, { Component } from 'react';
import { createFragmentContainer, graphql } from 'react-relay';
import { isEqual } from 'lodash';
import DocumentTitle from 'react-document-title';

import DashboardWindow from './DashboardWindow';
import FormErrors from '../shared/FormErrors';
import Form from '../shared/Form';
import Select from '../shared/Select';
import Toggle from '../shared/Toggle';
import AccountSettingsMutation from '../../graphql/mutations/AccountSettingsMutation';
import setFormErrors from '../../utils/setFormErrors';

class AccountSettings extends Component {
  state = { showSuccessMessage: false };

  handleSubmit = async (values, e, formApi) => {
    try {
      // if values have not been changed
      if (isEqual(this.props.viewer, values)) {
        return;
      }

      await AccountSettingsMutation.commit(
        this.props.relay.environment,
        values
      );

      this.setState({ showSuccessMessage: true });
    } catch ({ state }) {
      setFormErrors(state, formApi);
    }
  };

  render() {
    return (
      <DocumentTitle title="Account Settings">
        <Form onSubmit={this.handleSubmit} defaultValues={this.props.viewer}>
          {formApi => (
            <div className="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
              <DashboardWindow
                title="Account Settings"
                showSuccessMessage={this.state.showSuccessMessage}
              >
                <form onSubmit={formApi.submitForm}>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Select
                        field="friendAccess"
                        label="Who Can Friend You?"
                        options={[
                          { text: 'All', value: 'ALL' },
                          {
                            text: 'Friend of Friends',
                            value: 'FRIENDS_OF_FRIENDS'
                          }
                        ]}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Select
                        field="viewPostsAccess"
                        label="Who Can View Your Posts"
                        options={[
                          { text: 'All', value: 'ALL' },
                          {
                            text: 'Friend of Friends',
                            value: 'FRIENDS_OF_FRIENDS'
                          }
                        ]}
                      />
                    </div>
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <div className="description-toggle">
                        <div className="description-toggle-content">
                          <div className="h6">Notifications Email</div>
                          <p>
                            We’ll send you an email to your account each time
                            you receive a new activity notification
                          </p>
                        </div>
                        <Toggle field="notificationsEmail" />
                      </div>
                      <div className="description-toggle">
                        <div className="description-toggle-content">
                          <div className="h6">Chat Message Sound</div>
                          <p>
                            A sound will be played each time you receive a new
                            message on an inactive chat window
                          </p>
                        </div>
                        <Toggle field="chatMessageSound" />
                      </div>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <button
                        className="btn btn-secondary btn-lg full-width"
                        onClick={e => {
                          e.preventDefault();
                          formApi.setAllValues(this.props.viewer);
                        }}
                      >
                        Restore all Attributes
                      </button>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <button
                        className="btn btn-primary btn-lg full-width"
                        type="submit"
                      >
                        Save all Changes
                      </button>
                    </div>
                    <FormErrors {...formApi} />
                  </div>
                </form>
              </DashboardWindow>
            </div>
          )}
        </Form>
      </DocumentTitle>
    );
  }
}

export default createFragmentContainer(
  AccountSettings,
  graphql`
    fragment AccountSettings_viewer on Viewer {
      friendAccess
      viewPostsAccess
      chatMessageSound
      notificationsEmail
    }
  `
);
