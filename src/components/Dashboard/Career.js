import React, { Component } from 'react';
import { NestedField } from 'react-form';
import { capitalize, isEqual } from 'lodash';

import DashboardWindow from './DashboardWindow';
import Form from '../shared/Form';
import Input from '../shared/Input';
import Select from '../shared/Select';
import TextArea from '../shared/TextArea';
import { required } from '../../utils/validators';

import EditCareersMutations from '../../graphql/mutations/EditCareersMutation';
import DeleteCareersMutations from '../../graphql/mutations/DeleteCareersMutation';
import AddCareersMutation from '../../graphql/mutations/AddCareersMutation';
import environment from './../../graphql/environment';

class Career extends Component {
  state = { removableIds: [] };
  formApi;
  capitalizedType: String;

  renderCareerItem = (career = {}, formIndex: Number) => (
    <div className="row">
      <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <Input field="title" label="Title or Place" validate={required} />
        <i
          className="fa fa-times career-remove"
          aria-hidden="true"
          onClick={() => {
            if (typeof formIndex === 'number') {
              return this.formApi.removeValue('_new', formIndex);
            }

            if (this.state.removableIds.includes(career.id)) {
              return;
            }

            this.setState({
              removableIds: [...this.state.removableIds, career.id]
            });
          }}
        />
      </div>
      <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <Select
          renderEmpty
          validate={required}
          field="from"
          label="From"
          options={this.props.years}
        />
      </div>
      <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <Select
          renderEmpty
          validate={required}
          field="to"
          label="To"
          options={this.props.years}
        />
      </div>
      <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <TextArea field="description" label="Description" />
      </div>
    </div>
  );

  componentDidMount() {
    const { careers, type } = this.props;
    const _saved = careers.filter(c => c.type === type);

    this.setFormValues(_saved);
    this.capitalizedType = capitalize(type);
  }

  setFormValues = _saved => {
    this.formApi.setAllValues({
      _saved,
      _new: _saved.length ? [] : [{}]
    });
  };

  handleAddCareers = async (_saved: [], _new: []) => {
    const careersToAdd = _new.map(career => ({
      ...career,
      type: this.props.type
    }));

    const { addCareers } = await AddCareersMutation.commit(environment, {
      careers: careersToAdd
    });

    const newSaved = [..._saved, ...addCareers.addedCareers];
    this.setFormValues(newSaved);
  };

  handleEditCareers = async (_saved: []) => {
    await EditCareersMutations.commit(environment, {
      careers: _saved.map(career => {
        const updatedCareer = { ...career };
        delete updatedCareer.type;

        return updatedCareer;
      })
    });
  };

  handleDeleteCareers = async (_saved: [], careerIds: string[]) => {
    await DeleteCareersMutations.commit(environment, { careerIds });
    this.setState({ removableIds: [] });

    const newSaved = _saved.filter(c => !careerIds.includes(c.id));
    this.setFormValues(newSaved);
  };

  handleSubmit = async ({ _saved, _new }, e, formApi) => {
    const { careers, type } = this.props;

    if (_new.length) {
      await this.handleAddCareers(_saved, _new);
    }

    if (!isEqual(careers.filter(c => c.type === type), _saved)) {
      await this.handleEditCareers(_saved);
    }

    if (this.state.removableIds.length) {
      await this.handleDeleteCareers(_saved, this.state.removableIds);
    }
  };

  render() {
    return (
      <Form
        onSubmit={this.handleSubmit}
        getApi={formApi => (this.formApi = formApi)}
      >
        {formApi => {
          const { _saved = [], _new = [] } = formApi.values;
          return (
            <DashboardWindow title={`Your ${this.capitalizedType} History`}>
              <form onSubmit={formApi.submitForm}>
                {_saved.map((career, i) => (
                  <NestedField field={['_saved', i]} key={i}>
                    {this.state.removableIds.includes(career.id) ? (
                      <p className="align-center">
                        This entry will be deleted once you've saved your
                        changes.{' '}
                        <span
                          className="anchor underline"
                          onClick={() => {
                            this.setState({
                              removableIds: this.state.removableIds.filter(
                                id => id !== career.id
                              )
                            });
                          }}
                        >
                          Undo
                        </span>
                      </p>
                    ) : (
                      this.renderCareerItem(career)
                    )}
                  </NestedField>
                ))}
                {_new.map((career, i) => (
                  <NestedField field={['_new', i]} key={i}>
                    {this.renderCareerItem(career, i)}
                  </NestedField>
                ))}
                <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 mb-3 ml-2">
                  <span
                    className="anchor"
                    onClick={e => {
                      e.preventDefault();
                      formApi.addValue(`_new`, {});
                    }}
                  >
                    <i
                      className="fa fa-plus mr-1"
                      aria-hidden="true"
                      style={{ fontSize: '12px' }}
                    />
                    Add {this.capitalizedType} Field
                  </span>
                </div>
                <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                  <button className="btn btn-primary btn-lg full-width">
                    Save all Changes
                  </button>
                </div>
              </form>
            </DashboardWindow>
          );
        }}
      </Form>
    );
  }
}

export default Career;
