import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';

import DashboardWindow from './DashboardWindow';
import Input from '../shared/Input';
import Form from '../shared/Form';
import FormErrors from '../shared/FormErrors';
import { required, shouldMatch } from '../../utils/validators';
import ChangePasswordMutation from '../../graphql/mutations/ChangePasswordMutation';
import setFormErrors from '../../utils/setFormErrors';
import environment from '../../graphql/environment';

class ChangePassword extends Component {
  state = { showSuccessMessage: false };

  handleSubmit = async (values, e, formApi) => {
    try {
      const response = await ChangePasswordMutation.commit(environment, {
        currentPassword: values.currentPassword,
        newPassword: values.newPassword
      });

      formApi.resetAll();
      this.setState({ showSuccessMessage: true });
      localStorage.setItem('rfrt', response.changePassword.refreshToken);
    } catch ({ state }) {
      setFormErrors(state, formApi);
    }
  };

  render() {
    return (
      <DocumentTitle title="Change Password">
        <Form onSubmit={this.handleSubmit}>
          {formApi => (
            <div className="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
              <DashboardWindow
                title="Change Password"
                showSuccessMessage={this.state.showSuccessMessage}
              >
                <form onSubmit={formApi.submitForm}>
                  <div className="row">
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <Input
                        type="password"
                        field="currentPassword"
                        label="Current Password"
                        validate={required}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Input
                        type="password"
                        field="newPassword"
                        label="Your New Password"
                        validate={required}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Input
                        type="password"
                        field="confirmNewPassword"
                        label="Confirm New Password"
                        validate={[
                          required,
                          shouldMatch(formApi.values.newPassword)
                        ]}
                      />
                    </div>
                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <button className="btn btn-primary btn-lg full-width">
                        Change Password Now!
                      </button>
                    </div>
                    <FormErrors {...formApi} />
                  </div>
                </form>
              </DashboardWindow>
            </div>
          )}
        </Form>
      </DocumentTitle>
    );
  }
}

export default ChangePassword;
