import React from 'react';
import SuccessMessage from './SuccessMessage';

const DashboardWindow = ({ title, children, showSuccessMessage }) => (
  <div className="ui-block">
    <div className="ui-block-title">
      <h6 className="title">{title}</h6>
    </div>
    {showSuccessMessage && <SuccessMessage />}
    <div className="ui-block-content">{children}</div>
  </div>
);

export default DashboardWindow;
