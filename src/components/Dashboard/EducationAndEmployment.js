import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';
import { createFragmentContainer, graphql } from 'react-relay';

import Career from './Career';

class EducationAndEmployment extends Component {
  years = [];

  componentWillMount() {
    for (let year = new Date().getFullYear(); year >= 2000; year--) {
      this.years.push({ value: year, text: year });
    }
  }

  render() {
    const { careers } = this.props.viewer;

    return (
      <DocumentTitle title="Education and Employment">
        <div className="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
          <Career type="EDUCATION" careers={careers} years={this.years} />
          <Career type="EMPLOYEMENT" careers={careers} years={this.years} />
        </div>
      </DocumentTitle>
    );
  }
}

export default createFragmentContainer(
  EducationAndEmployment,
  graphql`
    fragment EducationAndEmployment_viewer on Viewer {
      id
      careers {
        id
        type
        title
        from
        to
        description
      }
    }
  `
);
