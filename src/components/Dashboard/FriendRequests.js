import React, { Fragment, Component } from 'react';
import { createPaginationContainer, graphql } from 'react-relay';
import { Link } from 'found';
import DocumentTitle from 'react-document-title';

import AddFriendMutation from '../../graphql/mutations/AddFriendMutation';
import DeleteFriendMutation from '../../graphql/mutations/DeleteFriendMutation';
import MarkFriendshipAsViewedMutation from '../../graphql/mutations/MarkFriendshipAsViewedMutation';
import ScrollToPaginate from '../shared/ScrollToPaginate';

const { PUBLIC_URL } = process.env;

const FriendRequestItem = ({
  friendship: {
    requester: { id: requesterId, profilePhoto, fullName, commonFriends },
    id: friendshipId,
    accepted
  },
  onAddFriend,
  onDeleteFriend,
  onMarkFriendshipAsViewed
}) => {
  const renderCommonFriendsField = () => {
    const { edges, totalCount } = commonFriends;

    if (totalCount === 0) {
      return null;
    }

    const onlyCommonFriend = edges[0].node;

    return totalCount === 1 ? (
      <span className="chat-message-item">
        Mutual Friend:{' '}
        <Link to={`/${onlyCommonFriend.id}`}>{onlyCommonFriend.fullName}</Link>
      </span>
    ) : (
      <span className="chat-message-item">{totalCount} Friends in Common</span>
    );
  };

  return (
    <li className={accepted ? 'accepted' : ''}>
      <div className="author-thumb">
        <img src={profilePhoto} alt="author" />
      </div>
      {accepted ? (
        <Fragment>
          <div className="notification-event">
            You and{' '}
            <Link to={`/${requesterId}`} className="h6 notification-friend">
              {fullName}
            </Link>{' '}
            just became friends.
          </div>{' '}
          <span className="notification-icon">
            <svg className="olymp-happy-face-icon">
              <use
                xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
              />
            </svg>
          </span>
          <div className="more">
            <svg
              className="olymp-little-delete"
              onClick={() => {
                onMarkFriendshipAsViewed(requesterId);
              }}
            >
              <use
                xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
              />
            </svg>
          </div>
        </Fragment>
      ) : (
        <Fragment>
          <div className="notification-event">
            <Link to={`/${requesterId}`} className="h6 notification-friend">
              {fullName}
            </Link>
            {renderCommonFriendsField()}
          </div>
          <span className="notification-icon">
            <a
              className="accept-request pointer"
              onClick={() => onAddFriend(requesterId, friendshipId)}
            >
              <span className="icon-add">
                <svg className="olymp-happy-face-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                  />
                </svg>
              </span>
              Accept Friend Request
            </a>
            <a
              className="accept-request request-del pointer"
              onClick={() => onDeleteFriend(requesterId, friendshipId)}
            >
              <span className="icon-minus">
                <svg className="olymp-happy-face-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                  />
                </svg>
              </span>
            </a>
          </span>
        </Fragment>
      )}
    </li>
  );
};

class FriendRequests extends Component {
  handleAddFriendClick = (userId: string, friendshipId: string) => {
    AddFriendMutation.commit(
      this.props.relay.environment,
      {
        userId
      },
      friendshipId
    );
  };

  handleDeleteFriendClick = (userId: string, friendshipId: string) => {
    DeleteFriendMutation.commit(
      this.props.relay.environment,
      {
        userId
      },
      friendshipId
    );
  };

  handleMarkFriendshipAsViewed = (userId: string) => {
    MarkFriendshipAsViewedMutation.commit(this.props.relay.environment, {
      userId
    });
  };

  render() {
    const { viewer: { incomingRequests }, relay } = this.props;

    return (
      <DocumentTitle title="Friend Requests">
        <div className="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
          <div className="ui-block">
            <div className="ui-block-title">
              <h6 className="title">Friend Requests</h6>
            </div>
            <ScrollToPaginate
              className="notification-list friend-requests"
              relay={relay}
              pageSize={10}
            >
              {incomingRequests.edges.length ? (
                incomingRequests.edges.map(({ node }) => (
                  <FriendRequestItem
                    friendship={node}
                    key={node.id}
                    onAddFriend={this.handleAddFriendClick}
                    onDeleteFriend={this.handleDeleteFriendClick}
                    onMarkFriendshipAsViewed={this.handleMarkFriendshipAsViewed}
                  />
                ))
              ) : (
                <h3 className="not-found">You've had no requests yet</h3>
              )}
            </ScrollToPaginate>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}
export default createPaginationContainer(
  FriendRequests,
  {
    viewer: graphql`
      fragment FriendRequests_viewer on Viewer
        @argumentDefinitions(
          count: { type: "Int", defaultValue: 15 }
          cursor: { type: "String" }
        ) {
        incomingRequests(first: $count, after: $cursor, order: DESC)
          @connection(key: "FriendRequests_incomingRequests") {
          edges {
            cursor
            node {
              id
              accepted
              viewed
              requester {
                id
                fullName
                profilePhoto
                commonFriends {
                  edges {
                    node {
                      id
                      fullName
                    }
                  }
                  totalCount
                }
              }
            }
          }
        }
        id
      }
    `
  },
  {
    direction: 'forward',
    getConnectionFromProps(props) {
      return props.viewer && props.viewer.incomingRequests;
    },
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount
      };
    },
    getVariables(props, { count, cursor }, fragmentVariables) {
      return {
        count,
        cursor
      };
    },
    query: graphql`
      query FriendRequestsPaginationQuery($count: Int!, $cursor: String) {
        viewer {
          ...FriendRequests_viewer @arguments(count: $count, cursor: $cursor)
        }
      }
    `
  }
);
