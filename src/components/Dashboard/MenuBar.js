import React, { Component } from 'react';
import { Link } from 'found';
import { createFragmentContainer, graphql } from 'react-relay';

const { PUBLIC_URL } = process.env;

class MenuBar extends Component {
  state = { isOpen: true };

  render() {
    const { incomingRequests } = this.props.viewer;

    return (
      <div className="ui-block">
        <div className="your-profile">
          <div className="ui-block-title ui-block-title-small">
            <h6 className="title">Your Profile</h6>
          </div>
          <div className="accordion">
            <div className="card">
              <div className="card-header" role="tab" id="headingOne">
                <h6 className="mb-0">
                  <a
                    className="pointer"
                    onClick={() => {
                      this.setState({ isOpen: !this.state.isOpen });
                    }}
                  >
                    Profile Settings
                    <svg className="olymp-dropdown-arrow-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-dropdown-arrow-icon`}
                      />
                    </svg>
                  </a>
                </h6>
              </div>
              <div
                className={`profile-settings-dropdown ${
                  this.state.isOpen ? 'open' : ''
                }`}
              >
                <ul className="your-profile-menu">
                  <li>
                    <Link
                      activeClassName="active"
                      to="/dashboard/personal-information"
                    >
                      Personal Information
                    </Link>
                  </li>
                  <li>
                    <Link
                      activeClassName="active"
                      to="/dashboard/account-settings"
                    >
                      Account Settings
                    </Link>
                  </li>
                  <li>
                    <Link
                      activeClassName="active"
                      to="/dashboard/change-password"
                    >
                      Change Password
                    </Link>
                  </li>
                  <li>
                    <Link
                      activeClassName="active"
                      to="/dashboard/education-and-employment"
                    >
                      Education and Employement
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div className="ui-block-title">
            <Link to="/dashboard/notifications" className="h6 title">
              Notifications
            </Link>
            <a
              href=""
              onClick={e => e.preventDefault()}
              className="items-round-little bg-primary"
            >
              8
            </a>
          </div>
          <div className="ui-block-title">
            <Link to="/dashboard/messages" className="h6 title">
              Messages
            </Link>
          </div>
          <div className="ui-block-title">
            <Link to="/dashboard/friend-requests" className="h6 title">
              Friend Requests
            </Link>
            {!!incomingRequests.totalCount && (
              <Link
                to="/dashboard/friend-requests"
                className="items-round-little bg-blue"
              >
                {incomingRequests.totalCount}
              </Link>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default createFragmentContainer(
  MenuBar,
  graphql`
    fragment MenuBar_viewer on Viewer {
      incomingRequests {
        totalCount
      }
    }
  `
);
