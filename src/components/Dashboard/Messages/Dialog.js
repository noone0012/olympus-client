import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';

class Dialog extends Component {
  render() {
    return (
      <DocumentTitle title="Messages">
        <div className="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
          <div className="ui-block">
            <div className="chat-field">
              <div className="ui-block-title">
                <h6 className="title">Elaine Dreyfuss</h6>
              </div>
              <div className="mCustomScrollbar" data-mcs-theme="dark">
                <ul className="notification-list chat-message chat-message-field">
                  <li>
                    <div className="author-thumb">
                      <img
                        src={`${process.env.PUBLIC_URL}/img/avatar10-sm.jpg`}
                        alt="author"
                      />
                    </div>
                    <div className="notification-event">
                      <a href="#" className="h6 notification-friend">
                        Elaine Dreyfuss
                      </a>
                      <span className="notification-date">
                        <time
                          className="entry-date updated"
                          dateTime="2004-07-24T18:18"
                        >
                          Yesterday at 8:10pm
                        </time>
                      </span>
                      <span className="chat-message-item">
                        Hi James, How are your doing? Please remember that next
                        week we are going to Gotham Bar to celebrate Marina’s
                        Birthday.
                      </span>
                    </div>
                  </li>
                  <li>
                    <div className="author-thumb">
                      <img
                        src={`${process.env.PUBLIC_URL}/img/author-page.jpg`}
                        alt="author"
                      />
                    </div>
                    <div className="notification-event">
                      <a href="#" className="h6 notification-friend">
                        James Spiegel
                      </a>
                      <span className="notification-date">
                        <time
                          className="entry-date updated"
                          dateTime="2004-07-24T18:18"
                        >
                          Yesterday at 8:30pm
                        </time>
                      </span>
                      <span className="chat-message-item">
                        Hi Elaine! I have a question, do you think that tomorrow
                        we could talk to the client to iron out some details
                        before the presentation? I already finished the first
                        screen but I need to ask him something before moving on.
                        Anyway, here’s a preview!
                      </span>
                      <div className="added-photos">
                        <img
                          src={`${
                            process.env.PUBLIC_URL
                          }/img/photo-message1.jpg`}
                          alt="photo"
                        />
                        <img
                          src={`${
                            process.env.PUBLIC_URL
                          }/img/photo-message2.jpg`}
                          alt="photo"
                        />
                        <span className="photos-name">
                          icons.jpeg; bunny.jpeg
                        </span>
                      </div>
                    </div>
                  </li>
                  <li>
                    <div className="author-thumb">
                      <img
                        src={`${process.env.PUBLIC_URL}/img/avatar10-sm.jpg`}
                        alt="author"
                      />
                    </div>
                    <div className="notification-event">
                      <a href="#" className="h6 notification-friend">
                        Elaine Dreyfuss
                      </a>
                      <span className="notification-date">
                        <time
                          className="entry-date updated"
                          dateTime="2004-07-24T18:18"
                        >
                          Yesterday at 9:56pm
                        </time>
                      </span>
                      <span className="chat-message-item">
                        We’ll have to check that at the office and see if the
                        client is on board with it. I think That looks really
                        good!
                      </span>
                    </div>
                  </li>
                </ul>
              </div>
              <form>
                <div className="form-group label-floating is-empty">
                  <label className="control-label">
                    Write your reply here...
                  </label>
                  <textarea className="form-control" defaultValue={''} />
                </div>
                <div className="add-options-message">
                  <button className="btn btn-primary btn-sm">
                    Send Message
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

export default Dialog;
