import React from 'react';
import DocumentTitle from 'react-document-title';

const { PUBLIC_URL } = process.env;

const ChatMessages = () => (
  <DocumentTitle title="Messages">
    <div className="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
      <div className="ui-block">
        <div className="ui-block-title">
          <h6 className="title">Messages</h6>
        </div>
        <div>
          <ul className="notification-list chat-message">
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar8-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <a href="#" className="h6 notification-friend">
                  Diana Jameson
                </a>
                <span className="chat-message-item">
                  Hi James! It’s Diana, I just wanted to let you know that we
                  have to reschedule...
                </span>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    4 hours ago
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-chat---messages-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                  />
                </svg>
              </span>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </DocumentTitle>
);

export default ChatMessages;
