import React, { Component } from 'react';
import { graphql, createFragmentContainer } from 'react-relay';
import { isEqual } from 'lodash';
import { fetchQuery } from 'react-relay';
import DocumentTitle from 'react-document-title';
import $ from 'jquery';

import { required, isUrl } from '../../utils/validators';
import DashboardWindow from './DashboardWindow';
import FormErrors from '../shared/FormErrors';
import Form from '../shared/Form';
import DatePicker from '../shared/DatePicker';
import Input from '../shared/Input';
import Select from '../shared/Select';
import TextArea from '../shared/TextArea';
import setFormErrors from '../../utils/setFormErrors';
import PersonalInformationMutation from '../../graphql/mutations/PersonalInformationMutation';
import { gender, status } from '../../utils/options';

class PersonalInformation extends Component {
  state = { showSuccessMessage: false, cities: [] };
  formApi: Object;

  handleSubmit = async (values, e, formApi) => {
    try {
      // if values have not been changed
      if (isEqual(this.constructFormValues(this.props.viewer), values)) {
        return;
      }

      const data = {
        ...values,
        cityId: values.city
      };

      delete data.country;
      delete data.city;

      await PersonalInformationMutation.commit(
        this.props.relay.environment,
        data
      );

      this.setState({ showSuccessMessage: true });
      document.documentElement.scrollTop = 0;
    } catch ({ state }) {
      setFormErrors(state, formApi);
    }
  };

  fetchCities = (countryId, firstRender) => {
    const promise = () => {
      if (!countryId) {
        return Promise.resolve({ root: { cities: [] } });
      }

      return fetchQuery(
        this.props.relay.environment,
        graphql`
          query PersonalInformationQuery($countryId: ID!) {
            root {
              cities(countryId: $countryId) {
                id
                name
              }
            }
          }
        `,
        { countryId }
      );
    };

    promise().then(({ root: { cities } }) => {
      this.setState({ cities });
      $('#city').selectpicker('refresh');

      if (cities.length === 0) {
        this.formApi.setValue('city', null);
      } else {
        if (firstRender) {
          return;
        }

        this.formApi.setValue('city', cities[0].id);
      }
    });
  };

  constructFormValues = viewer => {
    return {
      ...viewer,
      country: viewer.country && viewer.country.id,
      city: viewer.city && viewer.city.id
    };
  };

  componentDidMount() {
    const { country } = this.props.viewer;

    if (country) {
      this.fetchCities(country.id, true);
    }
  }

  render() {
    const { viewer } = this.props;

    return (
      <DocumentTitle title="Personal Information">
        <Form
          onSubmit={this.handleSubmit}
          defaultValues={this.constructFormValues(viewer)}
          getApi={api => (this.formApi = api)}
        >
          {formApi => (
            <div className="col-xl-9 order-xl-2 col-lg-9 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
              <DashboardWindow
                title="Personal Information"
                showSuccessMessage={this.state.showSuccessMessage}
              >
                <form onSubmit={formApi.submitForm}>
                  <div className="row">
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Input
                        field="firstName"
                        label="First Name"
                        validate={required}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Input
                        field="lastName"
                        label="Last Name"
                        validate={required}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Input
                        field="website"
                        label="Your Website"
                        validate={[isUrl]}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <DatePicker
                        field="birthday"
                        label="Your Birthday"
                        validate={required}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Select
                        renderEmpty
                        field="country"
                        label="Your Country"
                        onChange={this.fetchCities}
                        options={this.props.root.countries.map(country => ({
                          text: country.name,
                          value: country.id
                        }))}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Select
                        disabled={!this.state.cities.length}
                        field="city"
                        label="Your City"
                        options={this.state.cities.map(country => ({
                          text: country.name,
                          value: country.id
                        }))}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Select
                        renderEmpty
                        field="status"
                        label="Status"
                        options={status}
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <Select
                        field="gender"
                        label="Your Gender"
                        validate={required}
                        options={gender}
                      />
                    </div>

                    <div className="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <TextArea
                        field="description"
                        label="Write a little description about you"
                      />
                    </div>

                    <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                      <Input
                        field="facebookAccount"
                        icon="facebook"
                        label="Your Facebook Account"
                        prefix="www.facebook.com/"
                      />
                      <Input
                        field="twitterAccount"
                        icon="twitter"
                        label="Your Twitter Account"
                        prefix="www.twitter.com/"
                      />
                      <Input
                        field="instagramAccount"
                        icon="instagram"
                        label="Your Instagram Account"
                        prefix="www.instagram.com/"
                      />
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <button
                        className="btn btn-secondary btn-lg full-width"
                        onClick={e => {
                          e.preventDefault();
                          formApi.setAllValues(
                            this.constructFormValues(viewer)
                          );
                        }}
                      >
                        Restore all Attributes
                      </button>
                    </div>
                    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                      <button
                        type="submit"
                        className="btn btn-primary btn-lg full-width"
                      >
                        Save all Changes
                      </button>
                    </div>

                    <FormErrors {...formApi} />
                  </div>
                </form>
              </DashboardWindow>
            </div>
          )}
        </Form>
      </DocumentTitle>
    );
  }
}

export default createFragmentContainer(PersonalInformation, {
  viewer: graphql`
    fragment PersonalInformation_viewer on Viewer {
      firstName
      lastName
      website
      birthday
      description
      status
      gender
      facebookAccount
      twitterAccount
      instagramAccount
      country {
        id
        name
      }
      city {
        id
        name
      }
    }
  `,
  root: graphql`
    fragment PersonalInformation_root on Root {
      countries @relay(plural: true) {
        id
        name
      }
    }
  `
});
