import React from 'react';

const SuccessMessage = () => (
  <div className="webform-confirmation ui-block-content">
    <div className="success-container">
      <img
        src={`${process.env.PUBLIC_URL}/img/checkmark.png`}
        alt="check"
        width="32"
        height="32"
      />
      <div className="success-copy">
        <p>
          Changes saved. <br /> Your profile has been successfully updated.
        </p>
      </div>
    </div>
  </div>
);

export default SuccessMessage;
