import React, { Component } from 'react';
import { createFragmentContainer, graphql } from 'react-relay';

import MenuBar from './MenuBar';

class Dashboard extends Component {
  state = { isOpen: false };

  render() {
    console.log(this.props);
    return (
      <div>
        <div className="container">
          <div className="row">
            {this.props.children}
            <div className="col-xl-3 order-xl-1 col-lg-3 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12 responsive-display-none">
              <MenuBar viewer={this.props.viewer} />
            </div>
            <div
              className={`profile-settings-responsive ${
                this.state.isOpen ? 'open' : ''
              }`}
            >
              <a
                className="profile-settings-open"
                onClick={() => {
                  this.setState({ isOpen: !this.state.isOpen });
                }}
              >
                <i className="fa fa-angle-right" aria-hidden="true" />
                <i className="fa fa-angle-left" aria-hidden="true" />
              </a>
              <div className="mCustomScrollbar" data-mcs-theme="dark">
                <MenuBar viewer={this.props.viewer} />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default createFragmentContainer(
  Dashboard,
  graphql`
    fragment Dashboard_viewer on Viewer {
      id
      ...MenuBar_viewer
    }
  `
);
