import React from 'react';

const FoundUserItem = ({
  fullName,
  profilePhoto,
  id,
  areFriends,
  router,
  onClick
}) => (
  <div className="selectize-dropdown-content" onClick={onClick}>
    <div className="inline-items">
      <div className="author-thumb">
        <img src={profilePhoto} style={{ width: '100%' }} alt="avatar" />
      </div>
      <div className="notification-event">
        <span className="h6 notification-friend">{fullName}</span>
        <span className="chat-message-item">2 Friends in Common</span>
      </div>
      {areFriends === 'IS_A_FRIEND' && (
        <span className="notification-icon">
          <svg className="olymp-happy-face-icon">
            <use
              xmlnsXlink="http://www.w3.org/1999/xlink"
              xlinkHref="icons/icons.svg#olymp-happy-face-icon"
            />
          </svg>
        </span>
      )}
    </div>
  </div>
);

export default FoundUserItem;
