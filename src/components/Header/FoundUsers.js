import React, { Component } from 'react';
import { createPaginationContainer, graphql } from 'react-relay';
import { throttle } from 'lodash';
import FoundUserItem from './FoundUserItem';

class FoundUsers extends Component {
  componentDidUpdate(prevProps, prevState) {
    if (prevProps.fullName !== this.props.fullName) {
      if (this.props.fullName === '') {
        return;
      }

      this._refetch();
    }
  }

  _refetch = throttle(() => {
    this.props.relay.refetchConnection(5);
  }, 1000);

  _loadMore = throttle(() => this.props.relay.loadMore(5), 1000);

  handleScroll = e => {
    const { isLoading, hasMore } = this.props.relay;

    if (isLoading() || !hasMore()) {
      return;
    }

    const bar = e.target;
    const scrollable = bar.scrollHeight - bar.clientHeight;
    const scrolled = bar.scrollTop;

    if (scrolled / scrollable * 100 > 70) {
      this._loadMore();
    }
  };

  render() {
    const { fullName, isShown, root: { users } } = this.props;

    return (
      !!fullName && (
        <div
          onScroll={this.handleScroll}
          className="selectize-dropdown multi form-control"
          style={{
            display: isShown && users.edges.length ? 'block' : 'none',
            top: 70,
            left: 0,
            visibility: 'visible'
          }}
        >
          {users.edges.map(edge => (
            <FoundUserItem
              key={edge.node.id}
              {...edge.node}
              onClick={() => {
                this.props.router.push(`/${edge.node.id}`);
                this.props.onToggle(false);
              }}
            />
          ))}
        </div>
      )
    );
  }
}

export default createPaginationContainer(
  FoundUsers,
  {
    root: graphql`
      fragment FoundUsers_root on Root
        @argumentDefinitions(
          count: { type: "Int", defaultValue: 5 }
          cursor: { type: "String" }
          fullName: { type: "String!", defaultValue: "" }
        ) {
        users(first: $count, after: $cursor, fullName: $fullName)
          @connection(key: "FoundUsers_users") {
          edges {
            node {
              id
              fullName
              profilePhoto
              areFriends
            }
          }
        }
        id
      }
    `
  },
  {
    direction: 'forward',
    getConnectionFromProps(props) {
      return props.root && props.root.users;
    },
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount
      };
    },
    getVariables(props, { count, cursor }, fragmentVariables) {
      fragmentVariables.fullName = props.fullName;

      return {
        count,
        cursor,
        fullName: props.fullName
      };
    },
    query: graphql`
      query FoundUsersPaginationQuery(
        $count: Int!
        $cursor: String
        $fullName: String!
      ) {
        root {
          ...FoundUsers_root
            @arguments(count: $count, cursor: $cursor, fullName: $fullName)
        }
      }
    `
  }
);
