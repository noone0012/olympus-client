import React, { Component } from 'react';
import { Link } from 'found';

const { PUBLIC_URL } = process.env;

class FriendRequests extends Component {
  render() {
    return (
      <div className={`tab-pane ${this.props.isActive ? 'active' : ''}`}>
        <div className="mCustomScrollbar">
          <div className="ui-block-title ui-block-title-small">
            <h6 className="title">FRIEND REQUESTS</h6>
          </div>
          <ul className="notification-list friend-requests">
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar55-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <a href="#" className="h6 notification-friend">
                  Tamara Romanoff
                </a>
                <span className="chat-message-item">
                  Mutual Friend: Sarah Hetfield
                </span>
              </div>
              <span className="notification-icon">
                <a href="#" className="accept-request">
                  <span className="icon-add without-text">
                    <svg className="olymp-happy-face-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                      />
                    </svg>
                  </span>
                </a>
                <a href="#" className="accept-request request-del">
                  <span className="icon-minus">
                    <svg className="olymp-happy-face-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                      />
                    </svg>
                  </span>
                </a>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </li>
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar56-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <a href="#" className="h6 notification-friend">
                  Tony Stevens
                </a>
                <span className="chat-message-item">4 Friends in Common</span>
              </div>
              <span className="notification-icon">
                <a href="#" className="accept-request">
                  <span className="icon-add without-text">
                    <svg className="olymp-happy-face-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                      />
                    </svg>
                  </span>
                </a>
                <a href="#" className="accept-request request-del">
                  <span className="icon-minus">
                    <svg className="olymp-happy-face-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                      />
                    </svg>
                  </span>
                </a>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </li>
            <li className="accepted">
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar57-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                You and
                <a href="#" className="h6 notification-friend">
                  Mary Jane Stark
                </a>{' '}
                just became friends. Write on
                <a href="#" className="notification-link">
                  her wall
                </a>.
              </div>
              <span className="notification-icon">
                <svg className="olymp-happy-face-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
                <svg className="olymp-little-delete">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                  />
                </svg>
              </div>
            </li>
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar58-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <a href="#" className="h6 notification-friend">
                  Stagg Clothing
                </a>
                <span className="chat-message-item">9 Friends in Common</span>
              </div>
              <span className="notification-icon">
                <a href="#" className="accept-request">
                  <span className="icon-add without-text">
                    <svg className="olymp-happy-face-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                      />
                    </svg>
                  </span>
                </a>
                <a href="#" className="accept-request request-del">
                  <span className="icon-minus">
                    <svg className="olymp-happy-face-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                      />
                    </svg>
                  </span>
                </a>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </li>
          </ul>
          <Link to="/dashboard/friend-requests" className="view-all bg-blue">
            Check all your Requests
          </Link>
        </div>
      </div>
    );
  }
}

export default FriendRequests;
