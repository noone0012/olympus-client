import React, { Component, Fragment } from 'react';
import { Link } from 'found';
import { createFragmentContainer, graphql } from 'react-relay';

const FriendRequest = ({
  accepted,
  requester: { profilePhoto, id, fullName }
}) => {
  return (
    <li className={accepted ? 'accepted' : ''}>
      <div className="author-thumb">
        <img src={profilePhoto} alt="author" />
      </div>

      {accepted ? (
        <Fragment>
          <div className="notification-event">
            You and{' '}
            <Link to={`/${id}`} className="h6 notification-friend">
              {fullName}
            </Link>{' '}
            just became friends.
          </div>
          <span className="notification-icon">
            <svg className="olymp-happy-face-icon">
              <use
                xlinkHref={`${
                  process.env.PUBLIC_URL
                }/icons/icons.svg#olymp-happy-face-icon`}
              />
            </svg>
          </span>
        </Fragment>
      ) : (
        <Fragment>
          <div className="notification-event">
            <Link to={`/${id}`} className="h6 notification-friend">
              {fullName}
            </Link>
            <span className="chat-message-item">
              Mutual Friend: Sarah Hetfield
            </span>
          </div>
          <span className="notification-icon">
            <a href="#" className="accept-request">
              <span className="icon-add without-text">
                <svg className="olymp-happy-face-icon">
                  <use
                    xlinkHref={`${
                      process.env.PUBLIC_URL
                    }/icons/icons.svg#olymp-happy-face-icon`}
                  />
                </svg>
              </span>
            </a>
            <a href="#" className="accept-request request-del">
              <span className="icon-minus">
                <svg className="olymp-happy-face-icon">
                  <use
                    xlinkHref={`${
                      process.env.PUBLIC_URL
                    }/icons/icons.svg#olymp-happy-face-icon`}
                  />
                </svg>
              </span>
            </a>
          </span>
        </Fragment>
      )}
    </li>
  );
};

class FriendRequests extends Component {
  render() {
    const { incomingRequests } = this.props.viewer;

    return (
      <div className="control-icon more has-items">
        <svg className="olymp-happy-face-icon">
          <use
            xlinkHref={`${
              process.env.PUBLIC_URL
            }/icons/icons.svg#olymp-happy-face-icon`}
          />
        </svg>
        <div className="label-avatar bg-blue">
          {incomingRequests.totalCount}
        </div>
        <div className="more-dropdown more-with-triangle triangle-top-center">
          <div className="mCustomScrollbar">
            <div className="ui-block-title ui-block-title-small">
              <h6 className="title">FRIEND REQUESTS</h6>
            </div>
            <ul className="notification-list friend-requests">
              {!!incomingRequests.edges.length ? (
                incomingRequests.edges.map(edge => (
                  <FriendRequest key={edge.node.id} {...edge.node} />
                ))
              ) : (
                <p>No incoming requests</p>
              )}
            </ul>
            <Link to="/dashboard/friend-requests" className="view-all bg-blue">
              Check all your Requests
            </Link>
          </div>
        </div>
      </div>
    );
  }
}

export default createFragmentContainer(
  FriendRequests,
  graphql`
    fragment FriendRequestsTab_viewer on Viewer {
      incomingRequests(last: 5)
        @relay(plural: true)
        @connection(key: "FriendRequests_incomingRequests") {
        edges {
          node {
            id
            accepted
            requester {
              id
              fullName
              profilePhoto
            }
          }
        }
        totalCount
      }
    }
  `
);
