import React from 'react';
import { withRouter, Link } from 'found';
import { createFragmentContainer, graphql } from 'react-relay';

import SearchBar from './SearchBar';
import FriendRequestsTab from './FriendRequestsTab';

const { PUBLIC_URL } = process.env;

const Header = ({ viewer, root, router }) => (
  <header className="header" id="site-header">
    <div className="header-content-wrapper">
      <SearchBar router={router} root={root} />
      <div className="control-block">
        <FriendRequestsTab viewer={viewer} />

        <div className="control-icon more has-items">
          <svg className="olymp-chat---messages-icon">
            <use
              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
            />
          </svg>
          <div className="label-avatar bg-purple">2</div>
          <div className="more-dropdown more-with-triangle triangle-top-center">
            <div className="ui-block-title ui-block-title-small">
              <h6 className="title">Messages</h6>
              <a href="#">Mark all as read</a>
              <a href="#">Settings</a>
            </div>
            <div className="mCustomScrollbar" data-mcs-theme="dark">
              <ul className="notification-list chat-message">
                <li className="message-unread">
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar59-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Diana Jameson
                    </a>
                    <span className="chat-message-item">
                      Hi James! It’s Diana, I just wanted to let you know that
                      we have to reschedule...
                    </span>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        4 hours ago
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-chat---messages-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                      />
                    </svg>
                  </span>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar60-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Jake Parker
                    </a>
                    <span className="chat-message-item">
                      Great, I’ll see you tomorrow!.
                    </span>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        4 hours ago
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-chat---messages-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                      />
                    </svg>
                  </span>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar61-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Elaine Dreyfuss
                    </a>
                    <span className="chat-message-item">
                      We’ll have to check that at the office and see if the
                      client is on board with...
                    </span>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        Yesterday at 9:56pm
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-chat---messages-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                      />
                    </svg>
                  </span>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                  </div>
                </li>
                <li className="chat-group">
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar11-sm.jpg`}
                      alt="author"
                    />
                    <img
                      src={`${PUBLIC_URL}/img/avatar12-sm.jpg`}
                      alt="author"
                    />
                    <img
                      src={`${PUBLIC_URL}/img/avatar13-sm.jpg`}
                      alt="author"
                    />
                    <img
                      src={`${PUBLIC_URL}/img/avatar10-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      You, Faye, Ed &amp; Jet +3
                    </a>
                    <span className="last-message-author">Ed:</span>
                    <span className="chat-message-item">
                      Yeah! Seems fine by me!
                    </span>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        March 16th at 10:23am
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-chat---messages-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                      />
                    </svg>
                  </span>
                </li>
              </ul>
            </div>
            <a href="#" className="view-all bg-purple">
              View All Messages
            </a>
          </div>
        </div>
        <div className="control-icon more has-items">
          <svg className="olymp-thunder-icon">
            <use
              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-thunder-icon`}
            />
          </svg>
          <div className="label-avatar bg-primary">8</div>
          <div className="more-dropdown more-with-triangle triangle-top-center">
            <div className="ui-block-title ui-block-title-small">
              <h6 className="title">Notifications</h6>
            </div>
            <div className="mCustomScrollbar" data-mcs-theme="dark">
              <ul className="notification-list">
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar62-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <div>
                      <a href="#" className="h6 notification-friend">
                        Mathilda Brinker
                      </a>{' '}
                      commented on your new{' '}
                      <a href="#" className="notification-link">
                        profile status
                      </a>.
                    </div>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        4 hours ago
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-comments-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                      />
                    </svg>
                  </span>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <svg className="olymp-little-delete">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                      />
                    </svg>
                  </div>
                </li>
                <li className="un-read">
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar63-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <div>
                      You and{' '}
                      <a href="#" className="h6 notification-friend">
                        Nicholas Grissom
                      </a>{' '}
                      just became friends. Write on{' '}
                      <a href="#" className="notification-link">
                        his wall
                      </a>.
                    </div>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        9 hours ago
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-happy-face-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                      />
                    </svg>
                  </span>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <svg className="olymp-little-delete">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                      />
                    </svg>
                  </div>
                </li>
                <li className="with-comment-photo">
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar64-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <div>
                      <a href="#" className="h6 notification-friend">
                        Sarah Hetfield
                      </a>{' '}
                      commented on your
                      <a href="#" className="notification-link">
                        photo
                      </a>.
                    </div>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        Yesterday at 5:32am
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-comments-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                      />
                    </svg>
                  </span>
                  <div className="comment-photo">
                    <img
                      src={`${PUBLIC_URL}/img/comment-photo1.jpg`}
                      alt="photo"
                    />
                    <span>
                      “She looks incredible in that outfit! We should see
                      each...”
                    </span>
                  </div>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <svg className="olymp-little-delete">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                      />
                    </svg>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar65-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <div>
                      <a href="#" className="h6 notification-friend">
                        Green Goo Rock
                      </a>{' '}
                      invited you to attend to his event Goo in
                      <a href="#" className="notification-link">
                        Gotham Bar
                      </a>.
                    </div>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        March 5th at 6:43pm
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-happy-face-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                      />
                    </svg>
                  </span>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <svg className="olymp-little-delete">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                      />
                    </svg>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar66-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <div>
                      <a href="#" className="h6 notification-friend">
                        James Summers
                      </a>{' '}
                      commented on your new
                      <a href="#" className="notification-link">
                        profile status
                      </a>.
                    </div>
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        March 2nd at 8:29pm
                      </time>
                    </span>
                  </div>
                  <span className="notification-icon">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                  </span>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <svg className="olymp-little-delete">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                      />
                    </svg>
                  </div>
                </li>
              </ul>
            </div>
            <a href="#" className="view-all bg-primary">
              View All Notifications
            </a>
          </div>
        </div>
        <div className="author-page author vcard inline-items more">
          <div className="author-thumb">
            <img alt="author" src={viewer.profilePhoto} className="avatar" />
            <span className="icon-status online" />
            <div className="more-dropdown more-with-triangle">
              <div className="mCustomScrollbar" data-mcs-theme="dark">
                <div className="ui-block-title ui-block-title-small">
                  <h6 className="title">Your Account</h6>
                </div>
                <ul className="account-settings">
                  <li>
                    <Link to="/dashboard/personal-information">
                      <svg className="olymp-menu-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-menu-icon`}
                        />
                      </svg>
                      <span>Profile Settings</span>
                    </Link>
                  </li>
                  <li>
                    <a
                      href=""
                      onClick={e => {
                        e.preventDefault();
                        localStorage.clear();

                        router.push('/');
                      }}
                    >
                      <svg className="olymp-logout-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-logout-icon`}
                        />
                      </svg>
                      <span>Log Out</span>
                    </a>
                  </li>
                </ul>
                <div className="ui-block-title ui-block-title-small">
                  <h6 className="title">Chat Settings</h6>
                </div>
                <ul className="chat-settings">
                  <li>
                    <a href="#">
                      <span className="icon-status online" />
                      <span>Online</span>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <span className="icon-status away" />
                      <span>Away</span>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <span className="icon-status disconected" />
                      <span>Disconnected</span>
                    </a>
                  </li>
                  <li>
                    <a href="#">
                      <span className="icon-status status-invisible" />
                      <span>Invisible</span>
                    </a>
                  </li>
                </ul>
                <div className="ui-block-title ui-block-title-small">
                  <h6 className="title">Custom Status</h6>
                </div>
                <form className="form-group with-button custom-status">
                  <input
                    className="form-control"
                    type="text"
                    defaultValue="Space Cowboy"
                  />
                  <button className="bg-purple">
                    <svg className="olymp-check-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-check-icon`}
                      />
                    </svg>
                  </button>
                </form>
              </div>
            </div>
          </div>
          <a className="author-name fn">
            <div className="author-title">
              {viewer.fullName}
              <svg className="olymp-dropdown-arrow-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-dropdown-arrow-icon`}
                />
              </svg>
            </div>
            <span className="author-subtitle">SPACE COWBOY</span>
          </a>
        </div>
      </div>
    </div>
  </header>
);

export default createFragmentContainer(
  withRouter(Header),
  graphql`
    fragment Header_viewer on Viewer {
      id
      fullName
      profilePhoto
      ...FriendRequestsTab_viewer
    }
  `
);
