import React, { Component } from 'react';

const { PUBLIC_URL } = process.env;

class Messages extends Component {
  render() {
    return (
      <div className={`tab-pane ${this.props.isActive ? 'active' : ''}`}>
        <div className="mCustomScrollbar">
          <div className="ui-block-title ui-block-title-small">
            <h6 className="title">Messages</h6>
          </div>
          <ul className="notification-list chat-message">
            <li className="message-unread">
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar59-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <a href="#" className="h6 notification-friend">
                  Diana Jameson
                </a>
                <span className="chat-message-item">
                  Hi James! It’s Diana, I just wanted to let you know that we
                  have to reschedule...
                </span>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    4 hours ago
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-chat---messages-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </li>
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar60-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <a href="#" className="h6 notification-friend">
                  Jake Parker
                </a>
                <span className="chat-message-item">
                  Great, I’ll see you tomorrow!.
                </span>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    4 hours ago
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-chat---messages-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </li>
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar61-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <a href="#" className="h6 notification-friend">
                  Elaine Dreyfuss
                </a>
                <span className="chat-message-item">
                  We’ll have to check that at the office and see if the client
                  is on board with...
                </span>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    Yesterday at 9:56pm
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-chat---messages-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </li>
            <li className="chat-group">
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar11-sm.jpg`} alt="author" />
                <img src={`${PUBLIC_URL}/img/avatar12-sm.jpg`} alt="author" />
                <img src={`${PUBLIC_URL}/img/avatar13-sm.jpg`} alt="author" />
                <img src={`${PUBLIC_URL}/img/avatar10-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <a href="#" className="h6 notification-friend">
                  You, Faye, Ed &amp; Jet +3
                </a>
                <span className="last-message-author">Ed:</span>
                <span className="chat-message-item">
                  Yeah! Seems fine by me!
                </span>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    March 16th at 10:23am
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-chat---messages-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </li>
          </ul>
          <a href="#" className="view-all bg-purple">
            View All Messages
          </a>
        </div>
      </div>
    );
  }
}

export default Messages;
