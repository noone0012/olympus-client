import React, { Component } from 'react';

const { PUBLIC_URL } = process.env;

class Notifications extends Component {
  render() {
    return (
      <div className={`tab-pane ${this.props.isActive ? 'active' : ''}`}>
        <div className="mCustomScrollbar">
          <div className="ui-block-title ui-block-title-small">
            <h6 className="title">Notifications</h6>
          </div>
          <ul className="notification-list">
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar62-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <div>
                  <a href="#" className="h6 notification-friend">
                    Mathilda Brinker
                  </a>{' '}
                  commented on your new
                  <a href="#" className="notification-link">
                    profile status
                  </a>.
                </div>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    4 hours ago
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-comments-post-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
                <svg className="olymp-little-delete">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                  />
                </svg>
              </div>
            </li>
            <li className="un-read">
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar63-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <div>
                  You and
                  <a href="#" className="h6 notification-friend">
                    Nicholas Grissom
                  </a>{' '}
                  just became friends. Write on
                  <a href="#" className="notification-link">
                    his wall
                  </a>.
                </div>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    9 hours ago
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-happy-face-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
                <svg className="olymp-little-delete">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                  />
                </svg>
              </div>
            </li>
            <li className="with-comment-photo">
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar64-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <div>
                  <a href="#" className="h6 notification-friend">
                    Sarah Hetfield
                  </a>{' '}
                  commented on your
                  <a href="#" className="notification-link">
                    photo
                  </a>.
                </div>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    Yesterday at 5:32am
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-comments-post-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                  />
                </svg>
              </span>
              <div className="comment-photo">
                <img src={`${PUBLIC_URL}/img/comment-photo1.jpg`} alt="photo" />
                <span>
                  “She looks incredible in that outfit! We should see each...”
                </span>
              </div>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
                <svg className="olymp-little-delete">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                  />
                </svg>
              </div>
            </li>
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar65-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <div>
                  <a href="#" className="h6 notification-friend">
                    Green Goo Rock
                  </a>{' '}
                  invited you to attend to his event Goo in
                  <a href="#" className="notification-link">
                    Gotham Bar
                  </a>.
                </div>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    March 5th at 6:43pm
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-happy-face-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
                <svg className="olymp-little-delete">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                  />
                </svg>
              </div>
            </li>
            <li>
              <div className="author-thumb">
                <img src={`${PUBLIC_URL}/img/avatar66-sm.jpg`} alt="author" />
              </div>
              <div className="notification-event">
                <div>
                  <a href="#" className="h6 notification-friend">
                    James Summers
                  </a>{' '}
                  commented on your new
                  <a href="#" className="notification-link">
                    profile status
                  </a>.
                </div>
                <span className="notification-date">
                  <time
                    className="entry-date updated"
                    dateTime="2004-07-24T18:18"
                  >
                    March 2nd at 8:29pm
                  </time>
                </span>
              </div>
              <span className="notification-icon">
                <svg className="olymp-heart-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                  />
                </svg>
              </span>
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
                <svg className="olymp-little-delete">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-little-delete`}
                  />
                </svg>
              </div>
            </li>
          </ul>
          <a href="#" className="view-all bg-primary">
            View All Notifications
          </a>
        </div>
      </div>
    );
  }
}

export default Notifications;
