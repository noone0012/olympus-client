import React, { Component } from 'react';
import { withRouter } from 'found';

import FriendRequestsResponsive from './FriendRequestsResponsive';
import Messages from './Messages';
import Notifications from './Notifications';
import SearchBar from './SearchBar';

const { PUBLIC_URL } = process.env;

class ResponsiveHeader extends Component {
  state = { activeTab: null };

  componentDidUpdate(prevProps, prevState) {
    const previousActive = prevState.activeTab;
    const currentActive = this.state.activeTab;

    if (currentActive === null) {
      return;
    }

    if (previousActive === currentActive) {
      this.removeTransitionHook && this.removeTransitionHook();
      this.setState({ activeTab: null });
    } else {
      this.removeTransitionHook = this.props.router.addTransitionHook(() => {
        this.removeTransitionHook && this.removeTransitionHook();
        this.setState({ activeTab: null });
      });
    }
  }

  render() {
    const { activeTab } = this.state;

    return (
      <header className="header header-responsive">
        <div className="header-content-wrapper">
          <ul className="nav nav-tabs mobile-app-tabs" role="tablist">
            <li className="nav-item">
              <a
                className={`nav-link pointer ${
                  activeTab === 'FRIEND_REQUESTS' ? 'active' : ''
                }`}
                onClick={() => {
                  this.setState({ activeTab: 'FRIEND_REQUESTS' });
                }}
              >
                <div className="control-icon has-items">
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                  <div className="label-avatar bg-blue">6</div>
                </div>
              </a>
            </li>
            <li className="nav-item">
              <a
                className={`nav-link pointer ${
                  activeTab === 'MESSAGES' ? 'active' : ''
                }`}
                onClick={() => {
                  this.setState({ activeTab: 'MESSAGES' });
                }}
              >
                <div className="control-icon has-items">
                  <svg className="olymp-chat---messages-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                    />
                  </svg>
                  <div className="label-avatar bg-purple">2</div>
                </div>
              </a>
            </li>
            <li className="nav-item">
              <a
                className={`nav-link pointer ${
                  activeTab === 'NOTIFICATIONS' ? 'active' : ''
                }`}
                onClick={() => {
                  this.setState({ activeTab: 'NOTIFICATIONS' });
                }}
              >
                <div className="control-icon has-items">
                  <svg className="olymp-thunder-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-thunder-icon`}
                    />
                  </svg>
                  <div className="label-avatar bg-primary">8</div>
                </div>
              </a>
            </li>
            <li className="nav-item">
              <a
                className={`nav-link pointer ${
                  activeTab === 'SEARCH_BAR' ? 'active' : ''
                }`}
                onClick={() => {
                  this.setState({ activeTab: 'SEARCH_BAR' });
                }}
              >
                <svg className="olymp-magnifying-glass-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-magnifying-glass-icon`}
                  />
                </svg>
                <svg className="olymp-close-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-close-icon`}
                  />
                </svg>
              </a>
            </li>
          </ul>
        </div>
        <div className="tab-content tab-content-responsive">
          <FriendRequestsResponsive
            isActive={activeTab === 'FRIEND_REQUESTS'}
          />
          <Messages isActive={activeTab === 'MESSAGES'} />
          <Notifications isActive={activeTab === 'NOTIFICATIONS'} />
          <div
            className={`tab-pane ${activeTab === 'SEARCH_BAR' ? 'active' : ''}`}
          >
            <SearchBar router={this.props.router} root={this.props.root} />
          </div>
        </div>
      </header>
    );
  }
}

export default withRouter(ResponsiveHeader);
