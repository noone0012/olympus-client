import React, { Component } from 'react';
import FoundUsers from './FoundUsers';

class SearchBar extends Component {
  state = {
    fullName: '',
    isShown: false
  };

  onClick = e => {
    if (e.target.closest('.search-bar')) {
      return;
    }

    if (this.state.isShown) {
      this.setState({ isShown: false });
    }
  };

  componentDidMount() {
    document.addEventListener('click', this.onClick);
  }

  componentWillUnmount() {
    document.removeEventListener('click', this.onClick);
  }

  render() {
    return (
      <React.Fragment>
        <div className="search-bar w-search notification-list friend-requests">
          <div className="form-group with-button">
            <div className="selectize-control form-control js-user-search multi">
              <div className={`selectize-input items not-full has-options`}>
                <input
                  value={this.state.fullName}
                  onInput={e => this.setState({ fullName: e.target.value })}
                  onFocus={() => this.setState({ isShown: true })}
                  autoComplete="off"
                  placeholder="Search here people or pages..."
                />
              </div>
              <FoundUsers
                {...this.state}
                {...this.props}
                onToggle={isShown => this.setState({ isShown })}
              />
            </div>
            <button>
              <svg className="olymp-magnifying-glass-icon">
                <use
                  xlinkHref={`${
                    process.env.PUBLIC_URL
                  }/icons/icons.svg#olymp-magnifying-glass-icon`}
                />
              </svg>
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default SearchBar;
