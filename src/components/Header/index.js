import React from 'react';

import ResponsiveHeader from '../Header/ResponsiveHeader';
import HeaderMain from './Header';

const Header = ({ viewer, root }) => (
  <div>
    <HeaderMain viewer={viewer} root={root} />
    <ResponsiveHeader root={root} />
    <div className="header-spacer" />
  </div>
);

export default Header;
