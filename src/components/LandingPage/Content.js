import React from 'react';

const Content = () => (
  <div className="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
    <div className="landing-content">
      <h1>Welcome to the Biggest Social Network in the World</h1>
      <p>
        We are the best and biggest social network with 5 billion active users
        all around the world. Share you thoughts, write blog posts, show your
        favourite music via Stopify, earn badges and much more!
      </p>
      {/* <Link to="/" className="btn btn-md btn-border c-white">
        Register Now!
      </Link> */}
    </div>
  </div>
);

export default Content;
