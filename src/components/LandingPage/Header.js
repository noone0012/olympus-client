import React from 'react';

const Header = () => (
  <div className="container">
    <div className="row">
      <div className="col-xl-12 col-lg-12 col-md-12">
        <div id="site-header-landing" className="header-landing">
          <a className="logo">
            <img src={`${process.env.PUBLIC_URL}/img/logo.png`} alt="Olympus" />
            <h5 className="logo-title">olympus</h5>
          </a>
        </div>
      </div>
    </div>
  </div>
);

export default Header;
