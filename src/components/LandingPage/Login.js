import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Actions as FarceActions } from 'farce';

import Form from '../shared/Form';
import Input from '../shared/Input';
import FormErrors from '../shared/FormErrors';
import { required, email } from '../../utils/validators';
import setFormErrors from '../../utils/setFormErrors';
import LoginMutation from '../../graphql/mutations/LoginMutation';
import environment from '../../graphql/environment';

class Login extends Component {
  handleSubmit = async (values, e, formApi) => {
    try {
      const response = await LoginMutation.commit(environment, values);

      localStorage.setItem('acst', response.login.accessToken);
      localStorage.setItem('rfrt', response.login.refreshToken);

      this.props.push('/');
    } catch ({ state }) {
      setFormErrors(state, formApi);
    }
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        {formApi => (
          <div
            className="tab-pane active"
            id="profile"
            role="tabpanel"
            data-mh="log-tab"
          >
            <div className="title h6">Login to your Account</div>
            <form className="content" onSubmit={formApi.submitForm}>
              <div className="row">
                <div className="col-xl-12 col-lg-12 col-md-12">
                  <Input
                    autoFocus
                    field="email"
                    label="Your Email"
                    validate={[required, email]}
                  />
                  <Input
                    onEnter={formApi.submitForm}
                    field="password"
                    type="password"
                    label="Your Password"
                    validate={[required]}
                  />

                  {/* <div className="remember"> */}
                  {/* <Link to="/recovery" className="forgot">
              Forgot my Password
            </Link>{' '} */}
                  {/* </div> */}

                  <button className="btn btn-lg btn-primary full-width">
                    Login
                  </button>
                  <FormErrors {...formApi} />
                </div>
              </div>
            </form>
          </div>
        )}
      </Form>
    );
  }
}

export default connect(null, {
  push: FarceActions.push
})(Login);
