import React from 'react';
import { Link } from 'found';

const NavTabs = () => (
  <ul className="nav nav-tabs" role="tablist">
    <li className="nav-item">
      <Link
        to="/login"
        className="nav-link"
        activeClassName="active"
        data-toggle="tab"
        role="tab"
      >
        <svg className="olymp-login-icon">
          <use
            xlinkHref={`${
              process.env.PUBLIC_URL
            }/icons/icons.svg#olymp-login-icon`}
          />
        </svg>
      </Link>
    </li>
    <li className="nav-item">
      <Link
        exact
        to="/"
        className="nav-link"
        activeClassName="active"
        data-toggle="tab"
        role="tab"
      >
        <svg className="olymp-register-icon">
          <use
            xlinkHref={`${
              process.env.PUBLIC_URL
            }/icons/icons.svg#olymp-register-icon`}
          />
        </svg>
      </Link>
    </li>
  </ul>
);

export default NavTabs;
