import React from 'react';

const { PUBLIC_URL } = process.env;

const Login = () => (
  <div
    className="tab-pane active"
    id="profile"
    role="tabpanel"
    data-mh="log-tab"
  >
    <div className="title h6">Recover your Account</div>
    <form className="content">
      <div className="row">
        <div className="col-xl-12 col-lg-12 col-md-12">
          <div className="form-group label-floating is-empty">
            <label className="control-label">Your Email</label>
            <input className="form-control" type="email" />
          </div>

          <a href="#" className="btn btn-lg btn-primary full-width">
            Recover
          </a>
        </div>
      </div>
    </form>
  </div>
);

export default Login;
