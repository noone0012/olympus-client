import React, { Component } from 'react';

import FormErrors from '../shared/FormErrors';
import Form from '../shared/Form';
import { required, email } from '../../utils/validators';
import setFormErrors from '../../utils/setFormErrors';
import Select from '../shared/Select';
import Input from '../shared/Input';
import DatePicker from '../shared/DatePicker';
import { gender } from '../../utils/options';

import RegisterMutation from '../../graphql/mutations/RegisterMutation';
import environment from '../../graphql/environment';

class Register extends Component {
  state = { message: null };
  handleSubmit = async (values, e, formApi) => {
    try {
      const response = await RegisterMutation.commit(environment, values);
      this.setState({ message: response.register.message });

      formApi.resetAll();
    } catch ({ state }) {
      setFormErrors(state, formApi);
    }
  };

  render() {
    return (
      <Form onSubmit={this.handleSubmit}>
        {formApi => (
          <div
            className="tab-pane active"
            id="home"
            role="tabpanel"
            data-mh="log-tab"
          >
            <div className="title h6">Register to Olympus</div>
            <form className="content" onSubmit={formApi.submitForm}>
              <div className="row">
                <div className="col-lg-6 col-md-6">
                  <Input
                    field="firstName"
                    label="First Name"
                    validate={required}
                  />
                </div>
                <div className="col-lg-6 col-md-6">
                  <Input
                    field="lastName"
                    label="Last Name"
                    validate={required}
                  />
                </div>
                <div className="col-xl-12 col-lg-12 col-md-12">
                  <Input
                    field="email"
                    label="Your Email"
                    validate={[required, email]}
                  />
                  <Input
                    field="password"
                    type="password"
                    label="Your Password"
                    validate={[required]}
                  />
                  <DatePicker
                    field="birthday"
                    label="Your Birthday"
                    validate={required}
                  />
                  <Select
                    field="gender"
                    renderEmpty
                    label="Your Gender"
                    validate={[required]}
                    options={gender}
                  />

                  <button className="btn btn-purple btn-lg full-width mt-3">
                    Complete Registration!
                  </button>

                  <FormErrors {...formApi} />

                  {this.state.message && (
                    <p className="success">{this.state.message}</p>
                  )}
                </div>
              </div>
            </form>
          </div>
        )}
      </Form>
    );
  }
}

export default Register;
