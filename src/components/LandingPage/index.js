import React, { Component } from 'react';
import DocumentTitle from 'react-document-title';

import Header from './Header';
import Content from './Content';
import NavTabs from './NavTabs';

class LandingPage extends Component {
  render() {
    return (
      <DocumentTitle title="Welcome! | Olympus">
        <div className="landing-page">
          <div className="content-bg-wrap">
            <div className="content-bg" />
          </div>

          <Header />

          <div className="container">
            <div className="row display-flex" style={{ paddingBottom: '90px' }}>
              <Content />
              <div className="col-xl-5 col-lg-6 col-md-12 col-sm-12 col-xs-12">
                <div className="registration-login-form">
                  <NavTabs />
                  <div className="tab-content">{this.props.children}</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

export default LandingPage;
