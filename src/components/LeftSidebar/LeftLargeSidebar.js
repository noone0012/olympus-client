import React from 'react';
import { Link } from 'found';

const { PUBLIC_URL } = process.env;

const SidebarLeftLarge = ({ toggleOpenState, viewer }) => (
  <div className="fixed-sidebar-left sidebar--large">
    <Link to={`/${viewer.id}`} className="logo">
      <img src={`${PUBLIC_URL}/img/logo.png`} alt="Olympus" />
      <h6 className="logo-title">olympus</h6>
    </Link>

    <div className="mCustomScrollbar">
      <ul className="left-menu">
        <li>
          <a
            href=""
            className="pointer"
            onClick={e => {
              e.preventDefault();
              toggleOpenState();
            }}
          >
            <svg className="olymp-close-icon left-menu-icon">
              <use
                xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-close-icon`}
              />
            </svg>
            <span className="left-menu-title">Collapse Menu</span>
          </a>
        </li>
        <li>
          <Link to="/">
            <svg
              className="olymp-newsfeed-icon left-menu-icon"
              data-toggle="tooltip"
              data-placement="right"
              data-original-title="NEWSFEED"
            >
              <use
                xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-newsfeed-icon`}
              />
            </svg>
            <span className="left-menu-title">Newsfeed</span>
          </Link>
        </li>
      </ul>
    </div>
  </div>
);

export default SidebarLeftLarge;
