import React, { Component } from 'react';
import { Link } from 'found';

const { PUBLIC_URL } = process.env;

class LeftResponsiveSidebar extends Component {
  state = { isOpen: false };

  onClick = e => {
    if (
      this.state.isOpen &&
      !e.target.closest('.sidebar--large.fixed-sidebar-left')
    ) {
      this.setState({ isOpen: false });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { isOpen } = this.state;

    if (isOpen) {
      document.addEventListener('click', this.onClick);
    } else {
      document.removeEventListener('click', this.onClick);
    }
  }

  render() {
    const { id, fullName, profilePhoto } = this.props.viewer;
    const { router } = this.props;

    return (
      <div
        className={`fixed-sidebar fixed-sidebar-responsive ${
          this.state.isOpen ? 'open' : ''
        }`}
      >
        <div className="fixed-sidebar-left sidebar--small">
          <a
            className="logo pointer"
            onClick={() => {
              this.setState({ isOpen: true });
            }}
          >
            <img src={`${PUBLIC_URL}/img/logo.png`} alt="Olympus" />
          </a>
        </div>

        <div className="fixed-sidebar-left sidebar--large">
          <Link to={`/${id}`} className="logo">
            <img src={`${PUBLIC_URL}/img/logo.png`} alt="Olympus" />
            <h6 className="logo-title">olympus</h6>
          </Link>

          <div className="mCustomScrollbar">
            <div className="control-block">
              <Link
                to={`/${id}`}
                className="author-page author vcard inline-items"
              >
                <div className="author-thumb">
                  <img alt="author" src={profilePhoto} className="avatar" />
                  <span className="icon-status online" />
                </div>
                <div className="author-name fn">
                  <div className="author-title">
                    {fullName}
                    <svg className="olymp-dropdown-arrow-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-dropdown-arrow-icon`}
                      />
                    </svg>
                  </div>
                  <span className="author-subtitle">SPACE COWBOY</span>
                </div>
              </Link>
            </div>

            <div className="ui-block-title ui-block-title-small">
              <h6 className="title">MAIN SECTIONS</h6>
            </div>

            <ul className="left-menu">
              <li>
                <a
                  href=""
                  onClick={e => {
                    e.preventDefault();
                    this.setState({ isOpen: false });
                  }}
                >
                  <svg className="olymp-close-icon left-menu-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-close-icon`}
                    />
                  </svg>
                  <span className="left-menu-title">Collapse Menu</span>
                </a>
              </li>
              <li>
                <Link to="/">
                  <svg
                    className="olymp-newsfeed-icon left-menu-icon"
                    data-toggle="tooltip"
                    data-placement="right"
                    data-original-title="NEWSFEED"
                  >
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-newsfeed-icon`}
                    />
                  </svg>
                  <span className="left-menu-title">Newsfeed</span>
                </Link>
              </li>
            </ul>

            <div className="ui-block-title ui-block-title-small">
              <h6 className="title">YOUR ACCOUNT</h6>
            </div>

            <ul className="account-settings">
              <li>
                <Link to="/dashboard/account-settings">
                  <svg className="olymp-menu-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-menu-icon`}
                    />
                  </svg>

                  <span>Profile Settings</span>
                </Link>
              </li>
              <li>
                <a
                  href=""
                  onClick={e => {
                    e.preventDefault();
                    localStorage.clear();

                    router.push('/');
                  }}
                >
                  <svg className="olymp-logout-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-logout-icon`}
                    />
                  </svg>
                  <span>Log Out</span>
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }
}

export default LeftResponsiveSidebar;
