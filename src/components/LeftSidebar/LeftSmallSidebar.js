import React from 'react';
import { Link } from 'found';

const { PUBLIC_URL } = process.env;

const SidebarLeftSmall = ({ viewer, toggleOpenState }) => (
  <div className="fixed-sidebar-left sidebar--small">
    <Link to={`/${viewer.id}`} className="logo">
      <img src={`${PUBLIC_URL}/img/logo.png`} alt="Olympus" />
    </Link>

    <div className="mCustomScrollbar">
      <ul className="left-menu">
        <li>
          <a className="js-sidebar-open pointer" onClick={toggleOpenState}>
            <svg
              className="olymp-menu-icon left-menu-icon"
              data-toggle="tooltip"
              data-placement="right"
              data-original-title="OPEN MENU"
            >
              <use
                xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-menu-icon`}
              />
            </svg>
          </a>
        </li>
        <li>
          <Link to="/">
            <svg
              className="olymp-newsfeed-icon left-menu-icon"
              data-toggle="tooltip"
              data-placement="right"
              data-original-title="NEWSFEED"
            >
              <use
                xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-newsfeed-icon`}
              />
            </svg>
          </Link>
        </li>
      </ul>
    </div>
  </div>
);

export default SidebarLeftSmall;
