import React, { Component } from 'react';
import { createFragmentContainer, graphql } from 'react-relay';

import LeftSmallSidebar from './LeftSmallSidebar';
import LeftLargeSidebar from './LeftLargeSidebar';
import LeftResponsiveSidebar from './LeftResponsiveSidebar';

class LeftSidebar extends Component {
  state = { isOpen: false };

  toggleOpenState = () => {
    const { isOpen } = this.state;

    this.setState({ isOpen: !isOpen });
  };

  onClick = e => {
    if (
      this.state.isOpen &&
      !e.target.closest('.sidebar--large.fixed-sidebar-left')
    ) {
      this.setState({ isOpen: false });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { isOpen } = this.state;

    if (isOpen) {
      document.addEventListener('click', this.onClick);
    } else {
      document.removeEventListener('click', this.onClick);
    }
  }

  render() {
    const { viewer, router } = this.props;

    return (
      <div>
        <div className={`fixed-sidebar ${this.state.isOpen ? 'open' : ''}`}>
          <LeftSmallSidebar
            viewer={viewer}
            toggleOpenState={this.toggleOpenState}
          />
          <LeftLargeSidebar
            viewer={viewer}
            toggleOpenState={this.toggleOpenState}
          />
        </div>
        <LeftResponsiveSidebar viewer={viewer} router={router} />
      </div>
    );
  }
}

export default createFragmentContainer(
  LeftSidebar,
  graphql`
    fragment LeftSidebar_viewer on Viewer {
      id
      fullName
      profilePhoto
    }
  `
);
