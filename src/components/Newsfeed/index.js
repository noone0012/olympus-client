import React from 'react';
import DocumentTitle from 'react-document-title';
import { createFragmentContainer, graphql } from 'react-relay';

const { PUBLIC_URL } = process.env;

const Newsfeed = props => {
  return (
    <DocumentTitle title="Newsfeed">
      <div className="container">
        <div className="row">
          {/* Main Content */}
          <main className="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">
            <div className="ui-block">
              <div className="news-feed-form">
                {/* Nav tabs */}
                <ul className="nav nav-tabs" role="tablist">
                  <li className="nav-item">
                    <a
                      className="nav-link active inline-items"
                      data-toggle="tab"
                      href="#home-1"
                      role="tab"
                      aria-expanded="true"
                    >
                      <svg className="olymp-status-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-status-icon`}
                        />
                      </svg>
                      <span>Status</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link inline-items"
                      data-toggle="tab"
                      href="#profile-1"
                      role="tab"
                      aria-expanded="false"
                    >
                      <svg className="olymp-multimedia-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-multimedia-icon`}
                        />
                      </svg>
                      <span>Multimedia</span>
                    </a>
                  </li>
                  <li className="nav-item">
                    <a
                      className="nav-link inline-items"
                      data-toggle="tab"
                      href="#blog"
                      role="tab"
                      aria-expanded="false"
                    >
                      <svg className="olymp-blog-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-blog-icon`}
                        />
                      </svg>
                      <span>Blog Post</span>
                    </a>
                  </li>
                </ul>
                {/* Tab panes */}
                <div className="tab-content">
                  <div
                    className="tab-pane active"
                    id="home-1"
                    role="tabpanel"
                    aria-expanded="true"
                  >
                    <form>
                      <div className="author-thumb">
                        <img
                          src={`${PUBLIC_URL}/img/author-page.jpg`}
                          alt="author"
                        />
                      </div>
                      <div className="form-group with-icon label-floating is-empty">
                        <label className="control-label">
                          Share what you are thinking here...
                        </label>
                        <textarea className="form-control" defaultValue={''} />
                      </div>
                      <div className="add-options-message">
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="ADD PHOTOS"
                        >
                          <svg
                            className="olymp-camera-icon"
                            data-toggle="modal"
                            data-target="#update-header-photo"
                          >
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-camera-icon`}
                            />
                          </svg>
                        </a>
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="TAG YOUR FRIENDS"
                        >
                          <svg className="olymp-computer-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-computer-icon`}
                            />
                          </svg>
                        </a>
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="ADD LOCATION"
                        >
                          <svg className="olymp-small-pin-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-small-pin-icon`}
                            />
                          </svg>
                        </a>
                        <button className="btn btn-primary btn-md-2">
                          Post Status
                        </button>
                        <button className="btn btn-md-2 btn-border-think btn-transparent c-grey">
                          Preview
                        </button>
                      </div>
                    </form>
                  </div>
                  <div
                    className="tab-pane"
                    id="profile-1"
                    role="tabpanel"
                    aria-expanded="true"
                  >
                    <form>
                      <div className="author-thumb">
                        <img
                          src={`${PUBLIC_URL}/img/author-page.jpg`}
                          alt="author"
                        />
                      </div>
                      <div className="form-group with-icon label-floating is-empty">
                        <label className="control-label">
                          Share what you are thinking here...
                        </label>
                        <textarea className="form-control" defaultValue={''} />
                      </div>
                      <div className="add-options-message">
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="ADD PHOTOS"
                        >
                          <svg
                            className="olymp-camera-icon"
                            data-toggle="modal"
                            data-target="#update-header-photo"
                          >
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-camera-icon`}
                            />
                          </svg>
                        </a>
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="TAG YOUR FRIENDS"
                        >
                          <svg className="olymp-computer-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-computer-icon`}
                            />
                          </svg>
                        </a>
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="ADD LOCATION"
                        >
                          <svg className="olymp-small-pin-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-small-pin-icon`}
                            />
                          </svg>
                        </a>
                        <button className="btn btn-primary btn-md-2">
                          Post Status
                        </button>
                        <button className="btn btn-md-2 btn-border-think btn-transparent c-grey">
                          Preview
                        </button>
                      </div>
                    </form>
                  </div>
                  <div
                    className="tab-pane"
                    id="blog"
                    role="tabpanel"
                    aria-expanded="true"
                  >
                    <form>
                      <div className="author-thumb">
                        <img
                          src={`${PUBLIC_URL}/img/author-page.jpg`}
                          alt="author"
                        />
                      </div>
                      <div className="form-group with-icon label-floating is-empty">
                        <label className="control-label">
                          Share what you are thinking here...
                        </label>
                        <textarea className="form-control" defaultValue={''} />
                      </div>
                      <div className="add-options-message">
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="ADD PHOTOS"
                        >
                          <svg
                            className="olymp-camera-icon"
                            data-toggle="modal"
                            data-target="#update-header-photo"
                          >
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-camera-icon`}
                            />
                          </svg>
                        </a>
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="TAG YOUR FRIENDS"
                        >
                          <svg className="olymp-computer-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-computer-icon`}
                            />
                          </svg>
                        </a>
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="tooltip"
                          data-placement="top"
                          data-original-title="ADD LOCATION"
                        >
                          <svg className="olymp-small-pin-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-small-pin-icon`}
                            />
                          </svg>
                        </a>
                        <button className="btn btn-primary btn-md-2">
                          Post Status
                        </button>
                        <button className="btn btn-md-2 btn-border-think btn-transparent c-grey">
                          Preview
                        </button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
            <div id="newsfeed-items-grid">
              <div className="ui-block">
                <article className="hentry post video">
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/avatar7-sm.jpg`}
                      alt="author"
                    />
                    <div className="author-date">
                      <a className="h6 post__author-name fn" href="#">
                        Marina Valentine
                      </a>{' '}
                      shared a
                      <a href="#">link</a>
                      <div className="post__date">
                        <time className="published" dateTime="2004-07-24T18:18">
                          March 4 at 2:05pm
                        </time>
                      </div>
                    </div>
                    <div className="more">
                      <svg className="olymp-three-dots-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                        />
                      </svg>
                      <ul className="more-dropdown">
                        <li>
                          <a href="#">Edit Post</a>
                        </li>
                        <li>
                          <a href="#">Delete Post</a>
                        </li>
                        <li>
                          <a href="#">Turn Off Notifications</a>
                        </li>
                        <li>
                          <a href="#">Select as Featured</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <p>
                    Hey
                    <a href="#">Cindi</a>, you should really check out this new
                    song by Iron Maid. The next time they come to the city we
                    should totally go!
                  </p>
                  <div className="post-video">
                    <div className="video-thumb">
                      <img
                        src={`${PUBLIC_URL}/img/video-youtube1.jpg`}
                        alt="photo"
                      />
                      <a
                        href="https://youtube.com/watch?v=excVFQ2TWig"
                        className="play-video"
                      >
                        <svg className="olymp-play-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                          />
                        </svg>
                      </a>
                    </div>
                    <div className="video-content">
                      <a href="#" className="h4 title">
                        Iron Maid - ChillGroves
                      </a>
                      <p>
                        Lorem ipsum dolor sit amet, consectetur ipisicing elit,
                        sed do eiusmod tempor incididunt ut labore et dolore
                        magna aliqua...
                      </p>
                      <a href="#" className="link-site">
                        YOUTUBE.COM
                      </a>
                    </div>
                  </div>
                  <div className="post-additional-info inline-items">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>18</span>
                    </a>
                    <ul className="friends-harmonic">
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic9.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic10.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic7.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic8.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic11.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                    </ul>
                    <div className="names-people-likes">
                      <a href="#">Jenny</a>,
                      <a href="#">Robert</a> and
                      <br />18 more liked this
                    </div>
                    <div className="comments-shared">
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-speech-balloon-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                          />
                        </svg>
                        <span>0</span>
                      </a>
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-share-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                          />
                        </svg>
                        <span>16</span>
                      </a>
                    </div>
                  </div>
                  <div className="control-block-button post-control-button">
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-like-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-comments-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                </article>
              </div>
              <div className="ui-block">
                <article className="hentry post">
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/avatar10-sm.jpg`}
                      alt="author"
                    />
                    <div className="author-date">
                      <a className="h6 post__author-name fn" href="#">
                        Elaine Dreyfuss
                      </a>
                      <div className="post__date">
                        <time className="published" dateTime="2004-07-24T18:18">
                          9 hours ago
                        </time>
                      </div>
                    </div>
                    <div className="more">
                      <svg className="olymp-three-dots-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                        />
                      </svg>
                      <ul className="more-dropdown">
                        <li>
                          <a href="#">Edit Post</a>
                        </li>
                        <li>
                          <a href="#">Delete Post</a>
                        </li>
                        <li>
                          <a href="#">Turn Off Notifications</a>
                        </li>
                        <li>
                          <a href="#">Select as Featured</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempo incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris consequat.
                  </p>
                  <div className="post-additional-info inline-items">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>24</span>
                    </a>
                    <ul className="friends-harmonic">
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic7.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic8.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic9.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic10.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic11.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                    </ul>
                    <div className="names-people-likes">
                      <a href="#">You</a>,
                      <a href="#">Elaine</a> and
                      <br />22 more liked this
                    </div>
                    <div className="comments-shared">
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-speech-balloon-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                          />
                        </svg>
                        <span>17</span>
                      </a>
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-share-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                          />
                        </svg>
                        <span>24</span>
                      </a>
                    </div>
                  </div>
                  <div className="control-block-button post-control-button">
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-like-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-comments-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                </article>
                <ul className="comments-list">
                  <li>
                    <div className="post__author author vcard inline-items">
                      <img
                        src={`${PUBLIC_URL}/img/author-page.jpg`}
                        alt="author"
                      />
                      <div className="author-date">
                        <a
                          className="h6 post__author-name fn"
                          href="02-ProfilePage.html"
                        >
                          James Spiegel
                        </a>
                        <div className="post__date">
                          <time
                            className="published"
                            dateTime="2004-07-24T18:18"
                          >
                            38 mins ago
                          </time>
                        </div>
                      </div>
                      <a href="#" className="more">
                        <svg className="olymp-three-dots-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                          />
                        </svg>
                      </a>
                    </div>
                    <p>
                      Sed ut perspiciatis unde omnis iste natus error sit
                      voluptatem accusantium der doloremque laudantium.
                    </p>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>3</span>
                    </a>
                    <a href="#" className="reply">
                      Reply
                    </a>
                  </li>
                  <li>
                    <div className="post__author author vcard inline-items">
                      <img
                        src={`${PUBLIC_URL}/img/avatar1-sm.jpg`}
                        alt="author"
                      />
                      <div className="author-date">
                        <a className="h6 post__author-name fn" href="#">
                          Mathilda Brinker
                        </a>
                        <div className="post__date">
                          <time
                            className="published"
                            dateTime="2004-07-24T18:18"
                          >
                            1 hour ago
                          </time>
                        </div>
                      </div>
                      <a href="#" className="more">
                        <svg className="olymp-three-dots-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                          />
                        </svg>
                      </a>
                    </div>
                    <p>
                      Ratione voluptatem sequi en lod nesciunt. Neque porro
                      quisquam est, quinder dolorem ipsum quia dolor sit amet,
                      consectetur adipisci velit en lorem ipsum duis aute irure
                      dolor in reprehenderit in voluptate velit esse cillum.
                    </p>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>8</span>
                    </a>
                    <a href="#" className="reply">
                      Reply
                    </a>
                  </li>
                </ul>
                <a href="#" className="more-comments">
                  View more comments
                  <span>+</span>
                </a>
                <form className="comment-form inline-items">
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/author-page.jpg`}
                      alt="author"
                    />
                    <div className="form-group with-icon-right ">
                      <textarea className="form-control" defaultValue={''} />
                      <div className="add-options-message">
                        <a
                          href="#"
                          className="options-message"
                          data-toggle="modal"
                          data-target="#update-header-photo"
                        >
                          <svg className="olymp-camera-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-camera-icon`}
                            />
                          </svg>
                        </a>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div className="ui-block">
                <article className="hentry post has-post-thumbnail">
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/avatar5-sm.jpg`}
                      alt="author"
                    />
                    <div className="author-date">
                      <a className="h6 post__author-name fn" href="#">
                        Green Goo Rock
                      </a>
                      <div className="post__date">
                        <time className="published" dateTime="2004-07-24T18:18">
                          March 8 at 6:42pm
                        </time>
                      </div>
                    </div>
                    <div className="more">
                      <svg className="olymp-three-dots-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                        />
                      </svg>
                      <ul className="more-dropdown">
                        <li>
                          <a href="#">Edit Post</a>
                        </li>
                        <li>
                          <a href="#">Delete Post</a>
                        </li>
                        <li>
                          <a href="#">Turn Off Notifications</a>
                        </li>
                        <li>
                          <a href="#">Select as Featured</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <p>
                    Hey guys! We are gona be playing this Saturday of
                    <a href="#">The Marina Bar</a> for their new Mystic Deer
                    Party. If you wanna hang out and have a really good time,
                    come and join us. We’l be waiting for you!
                  </p>
                  <div className="post-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/post__thumb1.jpg`}
                      alt="photo"
                    />
                  </div>
                  <div className="post-additional-info inline-items">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>49</span>
                    </a>
                    <ul className="friends-harmonic">
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic9.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic10.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic7.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic8.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic11.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                    </ul>
                    <div className="names-people-likes">
                      <a href="#">Jimmy</a>,
                      <a href="#">Andrea</a> and
                      <br />47 more liked this
                    </div>
                    <div className="comments-shared">
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-speech-balloon-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                          />
                        </svg>
                        <span>264</span>
                      </a>
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-share-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                          />
                        </svg>
                        <span>37</span>
                      </a>
                    </div>
                  </div>
                  <div className="control-block-button post-control-button">
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-like-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-comments-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                </article>
              </div>
              <div className="ui-block">
                <article className="hentry post has-post-thumbnail">
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/avatar3-sm.jpg`}
                      alt="author"
                    />
                    <div className="author-date">
                      <a className="h6 post__author-name fn" href="#">
                        Sarah Hetfield
                      </a>
                      <div className="post__date">
                        <time className="published" dateTime="2004-07-24T18:18">
                          March 2 at 9:06am
                        </time>
                      </div>
                    </div>
                    <div className="more">
                      <svg className="olymp-three-dots-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                        />
                      </svg>
                      <ul className="more-dropdown">
                        <li>
                          <a href="#">Edit Post</a>
                        </li>
                        <li>
                          <a href="#">Delete Post</a>
                        </li>
                        <li>
                          <a href="#">Turn Off Notifications</a>
                        </li>
                        <li>
                          <a href="#">Select as Featured</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <p>
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum. Sed ut perspiciatis
                    unde omnis iste natus error sit voluptatem accusantium
                    doloremque.
                  </p>
                  <div className="post-additional-info inline-items">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>0 Likes</span>
                    </a>
                    <div className="comments-shared">
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-speech-balloon-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                          />
                        </svg>
                        <span>0 Comments</span>
                      </a>
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-share-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                          />
                        </svg>
                        <span>2 Shares</span>
                      </a>
                    </div>
                  </div>
                  <div className="control-block-button post-control-button">
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-like-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-comments-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                </article>
              </div>
              <div className="ui-block">
                <article className="hentry post has-post-thumbnail">
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/avatar2-sm.jpg`}
                      alt="author"
                    />
                    <div className="author-date">
                      <a className="h6 post__author-name fn" href="#">
                        Nicholas Grissom
                      </a>
                      <div className="post__date">
                        <time className="published" dateTime="2004-07-24T18:18">
                          March 2 at 8:34am
                        </time>
                      </div>
                    </div>
                    <div className="more">
                      <svg className="olymp-three-dots-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                        />
                      </svg>
                      <ul className="more-dropdown">
                        <li>
                          <a href="#">Edit Post</a>
                        </li>
                        <li>
                          <a href="#">Delete Post</a>
                        </li>
                        <li>
                          <a href="#">Turn Off Notifications</a>
                        </li>
                        <li>
                          <a href="#">Select as Featured</a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <p>
                    Duis aute irure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum. Sed ut perspiciatis
                    unde omnis iste natus error sit voluptatem accusantium
                    doloremque.
                  </p>
                  <div className="post-additional-info inline-items">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>22</span>
                    </a>
                    <ul className="friends-harmonic">
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic9.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic10.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic7.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic8.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                      <li>
                        <a href="#">
                          <img
                            src={`${PUBLIC_URL}/img/friend-harmonic11.jpg`}
                            alt="friend"
                          />
                        </a>
                      </li>
                    </ul>
                    <div className="names-people-likes">
                      <a href="#">Jimmy</a>,
                      <a href="#">Andrea</a> and
                      <br />47 more liked this
                    </div>
                    <div className="comments-shared">
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-speech-balloon-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                          />
                        </svg>
                        <span>0</span>
                      </a>
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-share-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                          />
                        </svg>
                        <span>2</span>
                      </a>
                    </div>
                  </div>
                  <div className="control-block-button post-control-button">
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-like-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-comments-post-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                        />
                      </svg>
                    </a>
                    <a href="#" className="btn btn-control">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                </article>
              </div>
            </div>
            <a
              id="load-more-button"
              href="#"
              className="btn btn-control btn-more"
              data-load-link="items-to-load.html"
              data-container="newsfeed-items-grid"
            >
              <svg className="olymp-three-dots-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                />
              </svg>
            </a>
          </main>
          {/* ... end Main Content */}
          {/* Left Sidebar */}
          <aside className="col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-xs-12">
            <div className="ui-block">
              <div className="ui-block-title">
                <h6 className="title">Friend Suggestions</h6>
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
              </div>
              <ul className="widget w-friend-pages-added notification-list friend-requests">
                <li className="inline-items">
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar38-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Francine Smith
                    </a>
                    <span className="chat-message-item">
                      8 Friends in Common
                    </span>
                  </div>
                  <span className="notification-icon">
                    <a href="#" className="accept-request">
                      <span className="icon-add without-text">
                        <svg className="olymp-happy-face-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                          />
                        </svg>
                      </span>
                    </a>
                  </span>
                </li>
                <li className="inline-items">
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar39-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Hugh Wilson
                    </a>
                    <span className="chat-message-item">
                      6 Friends in Common
                    </span>
                  </div>
                  <span className="notification-icon">
                    <a href="#" className="accept-request">
                      <span className="icon-add without-text">
                        <svg className="olymp-happy-face-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                          />
                        </svg>
                      </span>
                    </a>
                  </span>
                </li>
                <li className="inline-items">
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar40-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Karen Masters
                    </a>
                    <span className="chat-message-item">
                      6 Friends in Common
                    </span>
                  </div>
                  <span className="notification-icon">
                    <a href="#" className="accept-request">
                      <span className="icon-add without-text">
                        <svg className="olymp-happy-face-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                          />
                        </svg>
                      </span>
                    </a>
                  </span>
                </li>
              </ul>
            </div>
          </aside>
          {/* ... end Left Sidebar */}
          {/* Right Sidebar */}
          <aside className="col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-xs-12">
            <div className="ui-block">
              <div className="ui-block-title">
                <h6 className="title">Activity Feed</h6>
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
              </div>
              <ul className="widget w-activity-feed notification-list">
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar49-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Marina Polson
                    </a>{' '}
                    commented on Jason Mark’s
                    <a href="#" className="notification-link">
                      photo.
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        2 mins ago
                      </time>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar9-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Jake Parker{' '}
                    </a>{' '}
                    liked Nicholas Grissom’s
                    <a href="#" className="notification-link">
                      status update.
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        5 mins ago
                      </time>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar50-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Mary Jane Stark{' '}
                    </a>{' '}
                    added 20 new photos to her
                    <a href="#" className="notification-link">
                      gallery album.
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        12 mins ago
                      </time>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar51-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Nicholas Grissom{' '}
                    </a>{' '}
                    updated his profile
                    <a href="#" className="notification-link">
                      photo
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        1 hour ago
                      </time>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar48-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Marina Valentine{' '}
                    </a>{' '}
                    commented on Chris Greyson’s
                    <a href="#" className="notification-link">
                      status update
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        1 hour ago
                      </time>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar52-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Green Goo Rock{' '}
                    </a>{' '}
                    posted a
                    <a href="#" className="notification-link">
                      status update
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        1 hour ago
                      </time>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar10-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Elaine Dreyfuss{' '}
                    </a>{' '}
                    liked your
                    <a href="#" className="notification-link">
                      blog post
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        2 hours ago
                      </time>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar10-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Elaine Dreyfuss{' '}
                    </a>{' '}
                    commented on your
                    <a href="#" className="notification-link">
                      blog post
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        2 hours ago
                      </time>
                    </span>
                  </div>
                </li>
                <li>
                  <div className="author-thumb">
                    <img
                      src={`${PUBLIC_URL}/img/avatar53-sm.jpg`}
                      alt="author"
                    />
                  </div>
                  <div className="notification-event">
                    <a href="#" className="h6 notification-friend">
                      Bruce Peterson{' '}
                    </a>{' '}
                    changed his
                    <a href="#" className="notification-link">
                      profile picture
                    </a>.
                    <span className="notification-date">
                      <time
                        className="entry-date updated"
                        dateTime="2004-07-24T18:18"
                      >
                        15 hours ago
                      </time>
                    </span>
                  </div>
                </li>
              </ul>
            </div>
          </aside>
        </div>
      </div>
    </DocumentTitle>
  );
};

export default createFragmentContainer(
  Newsfeed,
  graphql`
    fragment Newsfeed_viewer on Viewer {
      id
      email
      fullName
      birthday
      profilePhoto
      headerPhoto
    }
  `
);
