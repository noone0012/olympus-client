import React from 'react';

import LeftSidebar from '../LeftSidebar';
import RightSidebar from '../RightSidebar';
import Header from '../Header';

import Chat from '../Chat';
import ChooseFromMyPhoto from '../popups/ChooseFromMyPhoto';

const Olympus = ({ children, viewer, root, router }) => (
  <React.Fragment>
    <LeftSidebar viewer={viewer} router={router} />
    <RightSidebar viewer={viewer} />
    <Header viewer={viewer} root={root} />

    <Chat root={root} />
    <ChooseFromMyPhoto />

    {children}
  </React.Fragment>
);

export default Olympus;
