import React from 'react';

const PersonalInfoItem = ({ url, title, text }) => (
  <li>
    <span className="title">{title}:</span>
    {url ? (
      text ? (
        <a className="text" href={text} target="_blank">
          {text}
        </a>
      ) : (
        <span className="text">-</span>
      )
    ) : (
      <span className="text">{text || '-'}</span>
    )}
  </li>
);

export default PersonalInfoItem;
