import React, { Component } from 'react';
import { createFragmentContainer, graphql } from 'react-relay';
import moment from 'moment';

import SocialLinks from '../SocialLinks';
import PersonalInfoItem from './PersonalInfoItem';
import getLivesInValue from '../../../utils/getLivesInValue';
import {
  gender as genderOptions,
  status as statusOptions
} from '../../../utils/options';

class About extends Component {
  componentWillMount() {
    const {
      user: { birthday, city, country, gender, status, joined }
    } = this.props;

    const birthdayISO = new Date(birthday).getTime();
    this.formattedBirthday = moment(birthdayISO).format('MMMM D, YYYY');

    const joinedISO = new Date(joined).getTime();
    this.formattedJoined = moment(joinedISO).format('MMMM D, YYYY');

    this.livesIn = getLivesInValue(city, country);

    try {
      this.genderText = genderOptions.find(g => g.value === gender).text;
      this.statusText = statusOptions.find(g => g.value === status).text;
    } catch (e) {}
  }

  renderCareers = careers => (
    <div className="col-lg-6 col-md-6 col-sm-12 col-xs-12">
      <ul className="widget w-personal-info item-block">
        {!!careers.length ? (
          careers.map(c => (
            <li key={c.id}>
              <span className="title">{c.title}</span>
              <span className="date">
                {c.from} - {c.to}
              </span>
              <span className="text">{c.description}</span>
            </li>
          ))
        ) : (
          <li>-</li>
        )}
      </ul>
    </div>
  );

  render() {
    const {
      user: {
        facebookAccount,
        instagramAccount,
        twitterAccount,
        description,
        website,
        email,
        careers
      }
    } = this.props;
    const educations = careers.filter(c => c.type === 'EDUCATION');
    const employements = careers.filter(c => c.type === 'EMPLOYEMENT');

    return (
      <div className="container about">
        <div className="row">
          <div className="col-xl-8 order-xl-2 col-lg-8 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
            <div className="ui-block">
              <div className="ui-block-title">
                <h6 className="title">Education and Employement</h6>
              </div>
              <div className="ui-block-content">
                <div className="row">
                  {this.renderCareers(educations)}
                  {this.renderCareers(employements)}
                </div>
              </div>
            </div>
          </div>
          <div className="col-xl-4 order-xl-1 col-lg-4 order-lg-1 col-md-12 order-md-2 col-sm-12 col-xs-12">
            <div className="ui-block">
              <div className="ui-block-title">
                <h6 className="title">Personal Info</h6>
              </div>
              <div className="ui-block-content">
                <ul className="widget w-personal-info">
                  <PersonalInfoItem title="About me" text={description} />
                  <PersonalInfoItem
                    title="Birthday"
                    text={this.formattedBirthday}
                  />
                  <PersonalInfoItem title="Lives in" text={this.livesIn} />
                  <PersonalInfoItem
                    title="Joined"
                    text={this.formattedJoined}
                  />
                  <PersonalInfoItem title="Gender" text={this.genderText} />
                  <PersonalInfoItem title="Status" text={this.statusText} />
                  <PersonalInfoItem url title="Website" text={website} />
                  <PersonalInfoItem title="Email" text={email} />
                </ul>
                <SocialLinks
                  facebookAccount={facebookAccount}
                  instagramAccount={instagramAccount}
                  twitterAccount={twitterAccount}
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default createFragmentContainer(
  About,
  graphql`
    fragment About_user on User {
      description
      facebookAccount
      instagramAccount
      twitterAccount
      birthday
      city {
        name
      }
      country {
        name
      }
      gender
      status
      website
      email
      joined
      careers {
        id
        type
        title
        from
        to
        description
      }
    }
  `
);
