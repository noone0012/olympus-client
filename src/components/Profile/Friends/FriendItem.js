import React from 'react';
import { Link } from 'found';

import FriendshipButton from '../FriendshipButton';

const FriendItem = ({
  livesIn,
  fullName,
  profilePhoto,
  headerPhoto,
  id,
  areFriends,
  isViewer
}) => (
  <div className="col-xl-3 col-lg-4 col-md-6 col-sm-6 col-xs-6">
    <div className="ui-block">
      <div className="friend-item">
        <div className="friend-header-thumb">
          <img src={headerPhoto} alt="friend" />
        </div>
        <div className="friend-item-content">
          <div className="friend-avatar">
            <div className="author-thumb">
              <img src={profilePhoto} alt="author" />
            </div>
            <div className="author-content">
              <Link to={`/${id}`} className="h5 author-name">
                {fullName}
              </Link>
              <div className="country">{livesIn}</div>
            </div>
          </div>
          <div>
            <div className="friend-count">
              <Link to={`/${id}/friends`} className="friend-count-item">
                <div className="h6">{10}</div>
                <div className="title">Friends</div>
              </Link>
              <Link to={`/${id}/photos`} className="friend-count-item">
                <div className="h6">240</div>
                <div className="title">Photos</div>
              </Link>
              <Link to={`/${id}/videos`} className="friend-count-item">
                <div className="h6">16</div>
                <div className="title">Videos</div>
              </Link>
            </div>
            <div className="control-block-button">
              <FriendshipButton
                top
                friendshipStatus={areFriends}
                isViewer={isViewer}
                userId={id}
              />
              <a href="#" className="btn btn-control bg-purple">
                <svg className="olymp-chat---messages-icon">
                  <use
                    xlinkHref={`${
                      process.env.PUBLIC_URL
                    }/icons/icons.svg#olymp-chat---messages-icon`}
                  />
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default FriendItem;
