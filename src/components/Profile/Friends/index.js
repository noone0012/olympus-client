import React, { Component } from 'react';
import { createPaginationContainer, graphql } from 'react-relay';
import FriendItem from './FriendItem';
import { Element, scroller } from 'react-scroll';

const { PUBLIC_URL } = process.env;

class Friends extends Component {
  state = { fullName: '' };

  handleLoadMoreClick = () => {
    const { hasMore, isLoading, loadMore } = this.props.relay;

    if (!hasMore() || isLoading()) {
      return;
    }

    loadMore(12);
  };

  handleSearchFriendsClick = e => {
    const { router, params: { id } } = this.props;
    const fullName = encodeURIComponent(this.state.fullName);

    router.push(`/${id}/friends/${fullName}`);
  };

  componentWillMount() {
    this.setState({ fullName: this.props.params.fullName || '' });
  }

  componentDidUpdate(prevProps, prevState) {
    if (prevProps.params.fullName !== this.props.params.fullName) {
      scroller.scrollTo('search-box', {
        duration: 500,
        offset: -80,
        smooth: true
      });
    }
  }

  render() {
    const { user: { friends: { edges, totalCount }, firstName } } = this.props;

    return (
      <div>
        <Element name="search-box">
          <div className="container">
            <div className="row">
              <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div className="ui-block responsive-flex">
                  <div className="ui-block-title">
                    <div className="h6 title">
                      {firstName}’s Friends ({totalCount})
                    </div>
                    <div className="w-search">
                      <div className="form-group with-button">
                        <input
                          className="form-control"
                          placeholder="Search Friends..."
                          value={this.state.fullName}
                          onChange={e =>
                            this.setState({ fullName: e.target.value })
                          }
                          onKeyDown={e =>
                            e.key === 'Enter' && this.handleSearchFriendsClick()
                          }
                        />
                        <button
                          className="outline"
                          onClick={this.handleSearchFriendsClick}
                        >
                          <svg className="olymp-magnifying-glass-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-magnifying-glass-icon`}
                            />
                          </svg>
                        </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </Element>
        <div className="container">
          <div className="row">
            {edges.length ? (
              edges.map(edge => (
                <FriendItem key={edge.node.id} {...edge.node} />
              ))
            ) : this.props.params.fullName ? (
              <h3 className="not-found">Your search returned no results</h3>
            ) : (
              <h3 className="not-found">You've had no friend yet</h3>
            )}
          </div>
          {this.props.relay.hasMore() && (
            <a
              className="btn btn-control btn-more"
              onClick={this.handleLoadMoreClick}
            >
              <svg className="olymp-three-dots-icon">
                <use xlinkHref="/icons/icons.svg#olymp-three-dots-icon" />
              </svg>
            </a>
          )}
        </div>
      </div>
    );
  }
}

export default createPaginationContainer(
  Friends,
  {
    user: graphql`
      fragment Friends_user on User
        @argumentDefinitions(
          count: { type: "Int", defaultValue: 12 }
          cursor: { type: "String" }
          fullName: { type: "String" }
        ) {
        friends(first: $count, after: $cursor, fullName: $fullName)
          @connection(key: "Friends_friends") {
          edges {
            node {
              id
              fullName
              profilePhoto
              headerPhoto
              city {
                name
              }
              country {
                name
              }
              isViewer
              areFriends
            }
          }
          totalCount
        }
        firstName
      }
    `
  },
  {
    direction: 'forward',
    getConnectionFromProps(props) {
      return props.user && props.user.friends;
    },
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount
      };
    },
    getVariables(props, { count, cursor }, fragmentVariables) {
      return {
        count,
        cursor,
        fullName: props.params.fullName,
        id: props.params.id
      };
    },
    query: graphql`
      query FriendsPaginationQuery(
        $count: Int!
        $cursor: String
        $fullName: String
        $id: ID!
      ) {
        user: node(id: $id) {
          ...Friends_user
            @arguments(count: $count, cursor: $cursor, fullName: $fullName)
        }
      }
    `
  }
);
