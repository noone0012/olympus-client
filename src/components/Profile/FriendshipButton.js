import React, { Component } from 'react';

import AddFriendMutation from '../../graphql/mutations/AddFriendMutation';
import DeleteFriendMutation from '../../graphql/mutations/DeleteFriendMutation';
import environment from '../../graphql/environment';

type Props = {
  friendshipStatus: Boolean,
  isViewer: Boolean,
  top?: Boolean
};

class FriendshipButton extends Component<Props> {
  handleAddFriendClick = () => {
    AddFriendMutation.commit(environment, { userId: this.props.userId });
  };

  handleDeleteFriendClick = () => {
    DeleteFriendMutation.commit(environment, { userId: this.props.userId });
  };

  render() {
    if (this.props.isViewer) {
      return null;
    }

    switch (this.props.friendshipStatus) {
      case 'NOT_A_FRIEND':
        return (
          <a className="add-friend mr-4" onClick={this.handleAddFriendClick}>
            Add to friends
          </a>
        );
      case 'REQUEST_WAS_SENT':
        return (
          <div className="more mr-4" style={{ display: 'inline-block' }}>
            <a className="add-friend">
              Request sent <i className="fa fa-check ml-1" aria-hidden="true" />
            </a>
            <ul
              className={`more-dropdown more-with-triangle more-friend-button ${
                this.props.top ? 'more-top triangle-bottom-left' : ''
              }`}
            >
              <li>
                <a onClick={this.handleDeleteFriendClick}>Cancel request</a>
              </li>
            </ul>
          </div>
        );
      case 'INCOMING_REQUEST':
        return (
          <div className="more mr-4" style={{ display: 'inline-block' }}>
            <a className="add-friend">
              Incoming request<i
                className="fa fa-question ml-1"
                aria-hidden="true"
              />
            </a>
            <ul
              className={`more-dropdown more-with-triangle more-friend-button ${
                this.props.top ? 'more-top triangle-bottom-left' : ''
              }`}
            >
              <li>
                <a onClick={this.handleDeleteFriendClick}>Cancel request</a>
              </li>
              <li>
                <a onClick={this.handleAddFriendClick}>Accept request</a>
              </li>
            </ul>
          </div>
        );
      case 'IS_A_FRIEND':
        return (
          <div className="more" style={{ display: 'inline-block' }}>
            <a className="btn btn-control bg-blue">
              <svg className="olymp-happy-face-icon">
                <use
                  xlinkHref={`${
                    process.env.PUBLIC_URL
                  }/icons/icons.svg#olymp-happy-face-icon`}
                />
              </svg>
              <div className="ripple-container" />
            </a>
            <ul
              className={`more-dropdown more-with-triangle more-friend-button ${
                this.props.top ? 'more-top triangle-bottom-left' : ''
              }`}
            >
              <li>
                <a onClick={this.handleDeleteFriendClick}>
                  Delete from friends
                </a>
              </li>
            </ul>
          </div>
        );

      default:
        return null;
    }
  }
}

export default FriendshipButton;
