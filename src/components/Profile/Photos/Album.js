import React from 'react';

const { PUBLIC_URL } = process.env;

const Album = ({ name, photos }) => (
  <div className="photo-album-item-wrap col-4-width">
    <div className="photo-album-item">
      <div className="photo-item">
        <img src={`${PUBLIC_URL}/img/photo-album5.jpg`} alt="photo" />
        <div className="overlay overlay-dark" />
        <a href="#" className="more">
          <svg className="olymp-three-dots-icon">
            <use
              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
            />
          </svg>
        </a>
        <a href="#" className="post-add-icon">
          <svg className="olymp-heart-icon">
            <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
          </svg>
          <span>324</span>
        </a>
        <a
          href="#"
          data-toggle="modal"
          data-target="#open-photo-popup-v1"
          className="full-block"
        />
      </div>
      <div className="content">
        <a href="#" className="title h5 ellipsis">
          {name}
        </a>
        <span className="sub-title">Last Added: 1 year ago</span>
        <div className="swiper-container" data-slide="fade">
          <div className="swiper-wrapper">
            <div className="swiper-slide">
              <div className="friend-count">
                <a href="#" className="friend-count-item">
                  <div className="h6">{photos.totalCount}</div>
                  <div className="title">Photos</div>
                </a>
                <a href="#" className="friend-count-item">
                  <div className="h6">86</div>
                  <div className="title">Comments</div>
                </a>
                <a href="#" className="friend-count-item">
                  <div className="h6">16</div>
                  <div className="title">Share</div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Album;
