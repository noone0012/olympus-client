import React, { Component } from 'react';
import Album from './Album';
import CreatePhotoAlbum from './CreatePhotoAlbum';
import { graphql, createPaginationContainer } from 'react-relay';

class AlbumsPage extends Component {
  handleLoadMoreClick = () => {
    const { hasMore, isLoading, loadMore } = this.props.relay;

    if (!hasMore() || isLoading()) {
      return;
    }

    loadMore(8);
  };

  render() {
    const { user: { albums: { edges } } } = this.props;

    return (
      <div className="photo-album-wrapper">
        {!!edges.length && (
          <div className="photo-album-item-wrap col-4-width">
            <CreatePhotoAlbum
              userId={this.props.user.id}
              trigger={handleToggle => (
                <div
                  className="photo-album-item create-album"
                  style={{ cursor: 'pointer' }}
                  onClick={handleToggle}
                >
                  <div className="content">
                    <a href="" className="btn btn-control bg-primary">
                      <svg className="olymp-plus-icon">
                        <use
                          xlinkHref={`${
                            process.env.PUBLIC_URL
                          }/icons/icons.svg#olymp-plus-icon`}
                        />
                      </svg>
                    </a>
                    <a href="" className="title h5">
                      Create an Album
                    </a>
                    <span className="sub-title">
                      It only takes a few minutes!
                    </span>
                  </div>
                </div>
              )}
            />
          </div>
        )}

        {edges.length ? (
          edges.map(edge => <Album key={edge.node.id} {...edge.node} />)
        ) : (
          <h3 className="not-found">You've had no album yet</h3>
        )}
        {this.props.relay.hasMore() && (
          <a
            className="btn btn-control btn-more"
            onClick={this.handleLoadMoreClick}
          >
            <svg className="olymp-three-dots-icon">
              <use xlinkHref="/icons/icons.svg#olymp-three-dots-icon" />
            </svg>
          </a>
        )}
      </div>
    );
  }
}

export default createPaginationContainer(
  AlbumsPage,
  {
    user: graphql`
      fragment AlbumsPage_user on User
        @argumentDefinitions(
          count: { type: "Int", defaultValue: 7 }
          cursor: { type: "String" }
        ) {
        albums(first: $count, after: $cursor)
          @connection(key: "AlbumsPage_albums") {
          edges {
            node {
              id
              name
              photos {
                totalCount
              }
            }
          }
          totalCount
        }
        id
      }
    `
  },
  {
    direction: 'forward',
    getConnectionFromProps(props) {
      return props.user && props.user.albums;
    },
    getFragmentVariables(prevVars, totalCount) {
      return {
        ...prevVars,
        count: totalCount
      };
    },
    getVariables(props, { count, cursor }, fragmentVariables) {
      return {
        count,
        cursor,
        id: props.params.id
      };
    },
    query: graphql`
      query AlbumsPagePaginationQuery($count: Int!, $cursor: String, $id: ID!) {
        user: node(id: $id) {
          ...AlbumsPage_user @arguments(count: $count, cursor: $cursor)
        }
      }
    `
  }
);
