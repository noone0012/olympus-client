import React, { Component } from 'react';

import Modal from '../../shared/Modal';
import Form from '../../shared/Form';
import Input from '../../shared/Input';
import TextArea from '../../shared/TextArea';
import { required } from '../../../utils/validators';
import { connect } from 'react-redux';
import { toggleValidationMessageState } from './../../../actions/index';
import FormErrors from './../../shared/FormErrors';
import setFormErrors from './../../../utils/setFormErrors';
import CreateAlbumMutation from '../../../graphql/mutations/CreateAlbumMutation';
import AddPhotosMutation from '../../../graphql/mutations/AddPhotosMutation';

import environment from './../../../graphql/environment';

const { PUBLIC_URL } = process.env;

const Photo = ({ src, index }) => (
  <div className="photo-album-item-wrap col-3-width">
    <div className="photo-album-item" style={{ height: 'auto' }}>
      <div className="form-group">
        <img src={src} alt="nature" />
        <TextArea
          field={['photoDescription', index]}
          label="Write something..."
          style={{
            minHeight: '80px',
            border: 0
          }}
        />
      </div>
    </div>
  </div>
);

class CreatePhotoAlbum extends Component {
  state = { modalOpen: false, photos: [] };
  input;

  handleToggle = e => {
    // modal is closing so reset all previous actions
    if (this.state.modalOpen) {
      this.reset();
    }

    e && e.preventDefault();
    this.setState({ modalOpen: !this.state.modalOpen });
  };

  reset = () => {
    this.state.photos.forEach(photo => URL.revokeObjectURL(photo.src));
    this.setState({ photos: [] });
    this.formApi.resetAll();
  };

  handleAddPhoto = e => {
    const newPhotos = [];

    Array.from(e.target.files).forEach(file => {
      if (!file.type.startsWith('image')) {
        this.handleToggle();
        const typeName = file.type.replace(/^.+?\//, '');

        return this.props.toggleValidationMessageState(
          `Type "${typeName}" is not supported for this operation.`
        );
      }

      newPhotos.push({
        src: URL.createObjectURL(file),
        file
      });
    });

    this.setState({ photos: [...this.state.photos, ...newPhotos] });
    e.target.value = null;
  };

  handleSubmit = async (values, e, formApi) => {
    try {
      const response = await CreateAlbumMutation.commit(
        environment,
        { name: values.albumName },
        this.props.userId
      );

      const albumId = response.createAlbum.albumEdge.node.id;

      await AddPhotosMutation.commit(
        environment,
        { files: this.state.photos.map(f => f.file), albumId },
        this.props.userId
      );

      this.handleToggle();
    } catch ({ state }) {
      setFormErrors(state, formApi);
    }
  };

  render() {
    const { photos } = this.state;

    return (
      <Modal
        onClose={this.handleToggle}
        trigger={this.props.trigger(this.handleToggle)}
        open={this.state.modalOpen}
        onShow={() => this.input.focus()}
      >
        <div className="modal" id="modal">
          <div className="modal-dialog ui-block window-popup create-photo-album">
            <a className="close icon-close" onClick={this.handleToggle}>
              <svg className="olymp-close-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-close-icon`}
                />
              </svg>
            </a>
            <div className="ui-block-title">
              <h6 className="title">Create Photo Album</h6>
            </div>
            <div className="ui-block-content">
              <Form
                onSubmit={this.handleSubmit}
                getApi={formApi => {
                  this.formApi = formApi;
                }}
              >
                {formApi => (
                  <form onSubmit={formApi.submitForm}>
                    <Input
                      field="albumName"
                      label="Album Name"
                      validate={required}
                      onEnter={formApi.submitForm}
                      getNode={node => {
                        this.input = node;
                      }}
                    />
                    <input
                      type="file"
                      multiple
                      id="photo"
                      style={{ display: 'none' }}
                      onChange={this.handleAddPhoto}
                    />
                    <div className="photo-album-wrapper">
                      <label
                        htmlFor="photo"
                        className="photo-album-item-wrap col-3-width"
                      >
                        <div
                          className="photo-album-item create-album"
                          style={{ height: '250px', cursor: 'pointer' }}
                        >
                          <div className="content">
                            <a className="btn btn-control bg-primary">
                              <svg className="olymp-plus-icon">
                                <use
                                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-plus-icon`}
                                />
                              </svg>
                            </a>
                            <a
                              href=""
                              onClick={e => e.preventDefault()}
                              className="title h5"
                            >
                              Add {!!photos.length && 'More'} Photos...{' '}
                              {!!photos.length && `(${photos.length})`}
                            </a>
                          </div>
                        </div>
                      </label>
                      {photos.map((photo, i) => (
                        <Photo key={i} index={i} {...photo} />
                      ))}
                    </div>
                    <button
                      className="btn btn-secondary btn-lg btn--half-width"
                      onClick={e => {
                        e.preventDefault();
                        this.reset();
                      }}
                    >
                      Discard Everything
                    </button>
                    <button className="btn btn-primary btn-lg btn--half-width">
                      Post Album
                    </button>
                    <FormErrors {...formApi} />
                  </form>
                )}
              </Form>
            </div>
          </div>
        </div>
      </Modal>
    );
  }
}

export default connect(null, { toggleValidationMessageState })(
  CreatePhotoAlbum
);
