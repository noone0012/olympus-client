import React from 'react';

const { PUBLIC_URL } = process.env;

const OpenPhoto2 = () => (
  <div className="modal fade" id="open-photo-popup-v2">
    <div className="modal-dialog ui-block window-popup open-photo-popup open-photo-popup-v2">
      <a
        href="#"
        className="close icon-close"
        data-dismiss="modal"
        aria-label="Close"
      >
        <svg className="olymp-close-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-close-icon`} />
        </svg>
      </a>
      <div className="open-photo-thumb">
        <div
          className="swiper-container"
          data-effect="fade"
          data-autoplay={4000}
        >
          {/* Additional required wrapper */}
          <div className="swiper-wrapper">
            {/* Slides */}
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
            <div className="swiper-slide">
              <div
                className="photo-item"
                data-swiper-parallax={-300}
                data-swiper-parallax-duration={500}
              >
                <img src={`${PUBLIC_URL}/img/open-photo2.jpg`} alt="photo" />
                <div className="overlay" />
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
                <a
                  href="#"
                  className="tag-friends"
                  data-toggle="tooltip"
                  data-placement="top"
                  data-original-title="TAG YOUR FRIENDS"
                >
                  <svg className="olymp-happy-face-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-happy-face-icon`}
                    />
                  </svg>
                </a>
                <div className="content">
                  <a href="#" className="h6 title">
                    Photoshoot 2016
                  </a>
                  <time className="published" dateTime="2017-03-24T18:18">
                    2 weeks ago
                  </time>
                </div>
              </div>
            </div>
          </div>
        </div>
        {/*Pagination tabs*/}
        <div className="slider-slides">
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs1.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs2.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs3.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs4.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs5.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs6.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs7.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs8.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          <a href="#" className="slides-item ">
            <img src={`${PUBLIC_URL}/img/photo-tabs9.jpg`} alt="slide" />
            <div className="overlay overlay-dark" />
          </a>
          {/*Prev Next Arrows*/}
          <svg className="btn-next olymp-popup-right-arrow">
            <use
              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-popup-right-arrow`}
            />
          </svg>
          <svg className="btn-prev olymp-popup-left-arrow">
            <use
              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-popup-left-arrow`}
            />
          </svg>
        </div>
      </div>
      <div className="open-photo-content">
        <article className="hentry post">
          <div className="post__author author vcard inline-items">
            <img src={`${PUBLIC_URL}/img/author-page.jpg`} alt="author" />
            <div className="author-date">
              <a className="h6 post__author-name fn" href="02-ProfilePage.html">
                James Spiegel
              </a>
              <div className="post__date">
                <time className="published" dateTime="2017-03-24T18:18">
                  2 hours ago
                </time>
              </div>
            </div>
            <div className="more">
              <svg className="olymp-three-dots-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                />
              </svg>
              <ul className="more-dropdown">
                <li>
                  <a href="#">Edit Post</a>
                </li>
                <li>
                  <a href="#">Delete Post</a>
                </li>
                <li>
                  <a href="#">Turn Off Notifications</a>
                </li>
                <li>
                  <a href="#">Select as Featured</a>
                </li>
              </ul>
            </div>
          </div>
          <p>
            Here’s a photo from last month’s photoshoot. We really had a great
            time and got a batch of incredible shots for the new catalog.
          </p>
          <p>
            With: <a href="#">Jessy Owen</a>, <a href="#">Marina Valentine</a>
          </p>
          <div className="post-additional-info inline-items">
            <a href="#" className="post-add-icon inline-items">
              <svg className="olymp-heart-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                />
              </svg>
              <span>148</span>
            </a>
            <div className="comments-shared">
              <a href="#" className="post-add-icon inline-items">
                <svg className="olymp-speech-balloon-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                  />
                </svg>
                <span>61</span>
              </a>
              <a href="#" className="post-add-icon inline-items">
                <svg className="olymp-share-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                  />
                </svg>
                <span>32</span>
              </a>
            </div>
          </div>
          <div className="control-block-button post-control-button">
            <a href="#" className="btn btn-control">
              <svg className="olymp-like-post-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                />
              </svg>
            </a>
            <a href="#" className="btn btn-control">
              <svg className="olymp-comments-post-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                />
              </svg>
            </a>
            <a href="#" className="btn btn-control">
              <svg className="olymp-share-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                />
              </svg>
            </a>
          </div>
        </article>
        <div className="mCustomScrollbar" data-mcs-theme="dark">
          <ul className="comments-list">
            <li>
              <div className="post__author author vcard inline-items">
                <img src={`${PUBLIC_URL}/img/avatar48-sm.jpg`} alt="author" />
                <div className="author-date">
                  <a className="h6 post__author-name fn" href="#">
                    Marina Valentine
                  </a>
                  <div className="post__date">
                    <time className="published" dateTime="2017-03-24T18:18">
                      46 mins ago
                    </time>
                  </div>
                </div>
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
              </div>
              <p>I had a great time too!! We should do it again!</p>
              <a href="#" className="post-add-icon inline-items">
                <svg className="olymp-heart-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                  />
                </svg>
                <span>8</span>
              </a>
              <a href="#" className="reply">
                Reply
              </a>
            </li>
            <li>
              <div className="post__author author vcard inline-items">
                <img src={`${PUBLIC_URL}/img/avatar4-sm.jpg`} alt="author" />
                <div className="author-date">
                  <a className="h6 post__author-name fn" href="#">
                    Chris Greyson
                  </a>
                  <div className="post__date">
                    <time className="published" dateTime="2017-03-24T18:18">
                      1 hour ago
                    </time>
                  </div>
                </div>
                <a href="#" className="more">
                  <svg className="olymp-three-dots-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                    />
                  </svg>
                </a>
              </div>
              <p>
                Dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                cupidatat non proident, sunt in culpa qui officia deserunt
                mollit.
              </p>
              <a href="#" className="post-add-icon inline-items">
                <svg className="olymp-heart-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                  />
                </svg>
                <span>7</span>
              </a>
              <a href="#" className="reply">
                Reply
              </a>
            </li>
          </ul>
        </div>
        <form className="comment-form inline-items">
          <div className="post__author author vcard inline-items">
            <img src={`${PUBLIC_URL}/img/avatar73-sm.jpg`} alt="author" />
            <div className="form-group with-icon-right ">
              <textarea
                className="form-control"
                placeholder="Press Enter to post..."
                defaultValue={''}
              />
              <div className="add-options-message">
                <a href="#" className="options-message">
                  <svg className="olymp-camera-icon">
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-camera-icon`}
                    />
                  </svg>
                </a>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
);

export default OpenPhoto2;
