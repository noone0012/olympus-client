import React from 'react';

const { PUBLIC_URL } = process.env;

const PhotosPage = () => (
  <div className="photo-album-wrapper">
    <div className="photo-item half-width">
      <img src={`${PUBLIC_URL}/img/photo-item1.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v1"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item2.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item3.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item4.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item5.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item6.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item7.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item8.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item9.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item10.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <div className="photo-item col-4-width">
      <img src={`${PUBLIC_URL}/img/photo-item11.jpg`} alt="photo" />
      <div className="overlay overlay-dark" />
      <a href="#" className="more">
        <svg className="olymp-three-dots-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
          />
        </svg>
      </a>
      <a href="#" className="post-add-icon inline-items">
        <svg className="olymp-heart-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`} />
        </svg>
        <span>15</span>
      </a>
      <a
        href="#"
        data-toggle="modal"
        data-target="#open-photo-popup-v2"
        className="  full-block"
      />
      <div className="content">
        <a href="#" className="h6 title">
          Header Photos
        </a>
        <time className="published" dateTime="2017-03-24T18:18">
          1 week ago
        </time>
      </div>
    </div>
    <a href="#" className="btn btn-control btn-more">
      <svg className="olymp-three-dots-icon">
        <use
          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
        />
      </svg>
    </a>
  </div>
);

export default PhotosPage;
