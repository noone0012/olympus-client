import React from 'react';
import { Link } from 'found';

import OpenPhoto1 from './OpenPhoto1';
import OpenPhoto2 from './OpenPhoto2';
import CreatePhotoAlbum from './CreatePhotoAlbum';
import { createFragmentContainer, graphql } from 'react-relay';

const { PUBLIC_URL } = process.env;

const Photos = ({ children, user: { id, firstName } }) => (
  <div>
    <OpenPhoto1 />
    <OpenPhoto2 />
    <div className="container">
      <div className="row">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div className="ui-block responsive-flex">
            <div className="ui-block-title">
              <div className="h6 title">{firstName}’s Photo Gallery</div>
              <div className="block-btn align-right">
                <CreatePhotoAlbum
                  trigger={handleToggle => (
                    <button
                      className="btn btn-primary btn-md-3"
                      onClick={handleToggle}
                    >
                      Create Album +
                    </button>
                  )}
                  userId={id}
                />
                <button className="btn btn-md-3 btn-border-think custom-color c-grey">
                  Add Photos
                </button>
              </div>
              <ul className="nav nav-tabs photo-gallery" role="tablist">
                <li className="nav-item">
                  <Link
                    className="nav-link"
                    activeClassName="active"
                    exact
                    to={`/${id}/photos`}
                  >
                    <svg className="olymp-photos-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-photos-icon`}
                      />
                    </svg>
                  </Link>
                </li>
                <li className="nav-item">
                  <Link
                    className="nav-link"
                    activeClassName="active"
                    to={`/${id}/photos/albums`}
                    style={{ borderRight: 0 }}
                  >
                    <svg className="olymp-albums-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-albums-icon`}
                      />
                    </svg>
                  </Link>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container">
      <div className="row">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div className="tab-content">{children}</div>
        </div>
      </div>
    </div>
  </div>
);

export default createFragmentContainer(
  Photos,
  graphql`
    fragment Photos_user on User {
      id
      firstName
    }
  `
);
