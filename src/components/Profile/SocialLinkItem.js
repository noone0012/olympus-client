import React from 'react';

const SocialLinkItem = ({ account, social }) =>
  account && (
    <a
      href={`http://www.${social}.com/${account}`}
      target="_blank"
      className={`social-item bg-${social}`}
    >
      <i className="fa fa-facebook" aria-hidden="true" />
      <span className="capitalize">{social}</span>
    </a>
  );

export default SocialLinkItem;
