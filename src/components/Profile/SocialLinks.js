import React from 'react';

import SocialLinkItem from './SocialLinkItem';

const SocialLinks = ({ facebookAccount, instagramAccount, twitterAccount }) => (
  <div className="widget w-socials">
    <h6 className="title">Other Social Networks:</h6>
    <SocialLinkItem social="facebook" account={facebookAccount} />
    <SocialLinkItem social="twitter" account={twitterAccount} />
    <SocialLinkItem social="instagram" account={instagramAccount} />
    {!facebookAccount &&
      !twitterAccount &&
      !instagramAccount && <span className="text">-</span>}
  </div>
);

export default SocialLinks;
