import React from 'react';

const Block = ({ children, title }) => (
    <div className="ui-block">
        <div className="ui-block-title">
            <h6 className="title">{title}</h6>
        </div>
        <div className="ui-block-content">
            {children}
        </div>
    </div>
)

export default Block;