import React, { Component } from 'react';
import { createFragmentContainer, graphql } from 'react-relay';
import { Link } from 'found';
import Block from './Block';

const { PUBLIC_URL } = process.env;

class FriendsBlock extends Component {
  render() {
    const { id, friends: { totalCount, edges } } = this.props.user;

    return (
      <Block title={`Friends (${totalCount})`}>
        <ul className="widget w-faved-page">
          {edges.map(({ node }) => (
            <li key={node.id}>
              <Link to={`/${node.id}`}>
                <img src={node.profilePhoto} alt="user" />
              </Link>
            </li>
          ))}
          {totalCount > edges.length && (
            <li className="all-users">
              <Link to={`/${id}/friends`}>+{totalCount - edges.length}</Link>
            </li>
          )}
        </ul>
      </Block>
    );
  }
}

export default createFragmentContainer(
  FriendsBlock,
  graphql`
    fragment FriendsBlock_user on User {
      id
      friends(first: 10) @relay(plural: true) {
        totalCount
        edges {
          node {
            id
            fullName
            profilePhoto
          }
        }
      }
    }
  `
);
