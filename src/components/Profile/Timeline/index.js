import React from 'react';
import { createFragmentContainer, graphql } from 'react-relay';

import SocialLinks from '../SocialLinks';
import FriendsBlock from './FriendsBlock';

const { PUBLIC_URL } = process.env;

const Timeline = ({ user }) => {
  console.log(user);
  return (
    <div className="container">
      <div className="row">
        <div className="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">
          <div id="newsfeed-items-grid">
            <div className="ui-block">
              <article className="hentry post">
                <div className="post__author author vcard inline-items">
                  <img src={`${PUBLIC_URL}/img/author-page.jpg`} alt="author" />
                  <div className="author-date">
                    <a
                      className="h6 post__author-name fn"
                      href="02-ProfilePage.html"
                    >
                      James Spiegel
                    </a>
                    <div className="post__date">
                      <time className="published" dateTime="2017-03-24T18:18">
                        19 hours ago
                      </time>
                    </div>
                  </div>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <ul className="more-dropdown">
                      <li>
                        <a href="#">Edit Post</a>
                      </li>
                      <li>
                        <a href="#">Delete Post</a>
                      </li>
                      <li>
                        <a href="#">Turn Off Notifications</a>
                      </li>
                      <li>
                        <a href="#">Select as Featured</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <p>
                  Duis aute irure dolor in reprehenderit in voluptate velit esse
                  cillum dolore eu fugiat nulla pariatur. Excepteur sint
                  occaecat cupidatat non proident, sunt in culpa qui officia
                  deserunt mollit anim id est laborum. Sed ut perspiciatis unde
                  omnis iste natus error sit voluptatem accusantium doloremque.
                </p>
                <div className="post-additional-info inline-items">
                  <a href="#" className="post-add-icon inline-items">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                    <span>8</span>
                  </a>
                  <ul className="friends-harmonic">
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic7.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic8.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic9.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic10.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic11.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                  </ul>
                  <div className="names-people-likes">
                    <a href="#">Jenny</a>,
                    <a href="#">Robert</a> and
                    <br />6 more liked this
                  </div>
                  <div className="comments-shared">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-speech-balloon-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                        />
                      </svg>
                      <span>17</span>
                    </a>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                      <span>24</span>
                    </a>
                  </div>
                </div>
                <div className="control-block-button post-control-button">
                  <a href="#" className="btn btn-control featured-post">
                    <svg className="olymp-trophy-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-trophy-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-like-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-comments-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-share-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                      />
                    </svg>
                  </a>
                </div>
              </article>
            </div>
            <div className="ui-block">
              <article className="hentry post video">
                <div className="post__author author vcard inline-items">
                  <img src={`${PUBLIC_URL}/img/author-page.jpg`} alt="author" />
                  <div className="author-date">
                    <a
                      className="h6 post__author-name fn"
                      href="02-ProfilePage.html"
                    >
                      James Spiegel
                    </a>{' '}
                    shared a
                    <a href="#">link</a>
                    <div className="post__date">
                      <time className="published" dateTime="2017-03-24T18:18">
                        7 hours ago
                      </time>
                    </div>
                  </div>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <ul className="more-dropdown">
                      <li>
                        <a href="#">Edit Post</a>
                      </li>
                      <li>
                        <a href="#">Delete Post</a>
                      </li>
                      <li>
                        <a href="#">Turn Off Notifications</a>
                      </li>
                      <li>
                        <a href="#">Select as Featured</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <p>
                  If someone missed it, check out the new song by System of a
                  Revenge! I thinks they are going back to their roots...
                </p>
                <div className="post-video">
                  <div className="video-thumb">
                    <img src={`${PUBLIC_URL}/img/video5.jpg`} alt="photo" />
                    <a
                      href="https://youtube.com/watch?v=excVFQ2TWig"
                      className="play-video"
                    >
                      <svg className="olymp-play-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                  <div className="video-content">
                    <a href="#" className="h4 title">
                      System of a Revenge - Nothing Else Matters (LIVE)
                    </a>
                    <p>
                      Lorem ipsum dolor sit amet, consectetur ipisicing elit,
                      sed do eiusmod tempo incididunt ut labore..
                    </p>
                    <a href="#" className="link-site">
                      YOUTUBE.COM
                    </a>
                  </div>
                </div>
                <div className="post-additional-info inline-items">
                  <a href="#" className="post-add-icon inline-items">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                    <span>15</span>
                  </a>
                  <ul className="friends-harmonic">
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic9.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic10.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic7.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic8.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic11.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                  </ul>
                  <div className="names-people-likes">
                    <a href="#">Jenny</a>,
                    <a href="#">Robert</a> and
                    <br />13 more liked this
                  </div>
                  <div className="comments-shared">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-speech-balloon-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                        />
                      </svg>
                      <span>1</span>
                    </a>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                      <span>16</span>
                    </a>
                  </div>
                </div>
                <div className="control-block-button post-control-button">
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-like-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-comments-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-share-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                      />
                    </svg>
                  </a>
                </div>
              </article>
            </div>
            <div className="ui-block">
              <article className="hentry post">
                <div className="post__author author vcard inline-items">
                  <img src={`${PUBLIC_URL}/img/author-page.jpg`} alt="author" />
                  <div className="author-date">
                    <a
                      className="h6 post__author-name fn"
                      href="02-ProfilePage.html"
                    >
                      James Spiegel
                    </a>
                    <div className="post__date">
                      <time className="published" dateTime="2017-03-24T18:18">
                        2 hours ago
                      </time>
                    </div>
                  </div>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <ul className="more-dropdown">
                      <li>
                        <a href="#">Edit Post</a>
                      </li>
                      <li>
                        <a href="#">Delete Post</a>
                      </li>
                      <li>
                        <a href="#">Turn Off Notifications</a>
                      </li>
                      <li>
                        <a href="#">Select as Featured</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempo incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris consequat.
                </p>
                <div className="post-additional-info inline-items">
                  <a href="#" className="post-add-icon inline-items">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                    <span>36</span>
                  </a>
                  <ul className="friends-harmonic">
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic7.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic8.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic9.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic10.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic11.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                  </ul>
                  <div className="names-people-likes">
                    <a href="#">You</a>,
                    <a href="#">Elaine</a> and
                    <br />34 more liked this
                  </div>
                  <div className="comments-shared">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-speech-balloon-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                        />
                      </svg>
                      <span>17</span>
                    </a>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                      <span>24</span>
                    </a>
                  </div>
                </div>
                <div className="control-block-button post-control-button">
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-like-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-comments-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-share-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                      />
                    </svg>
                  </a>
                </div>
              </article>
              <ul className="comments-list">
                <li>
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/avatar10-sm.jpg`}
                      alt="author"
                    />
                    <div className="author-date">
                      <a className="h6 post__author-name fn" href="#">
                        Elaine Dreyfuss
                      </a>
                      <div className="post__date">
                        <time className="published" dateTime="2017-03-24T18:18">
                          5 mins ago
                        </time>
                      </div>
                    </div>
                    <a href="#" className="more">
                      <svg className="olymp-three-dots-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                  <p>
                    Sed ut perspiciatis unde omnis iste natus error sit
                    voluptatem accusantium der doloremque laudantium.
                  </p>
                  <a href="#" className="post-add-icon inline-items">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                    <span>8</span>
                  </a>
                  <a href="#" className="reply">
                    Reply
                  </a>
                </li>
                <li className="has-children">
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/avatar5-sm.jpg`}
                      alt="author"
                    />
                    <div className="author-date">
                      <a className="h6 post__author-name fn" href="#">
                        Green Goo Rock
                      </a>
                      <div className="post__date">
                        <time className="published" dateTime="2017-03-24T18:18">
                          1 hour ago
                        </time>
                      </div>
                    </div>
                    <a href="#" className="more">
                      <svg className="olymp-three-dots-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                  <p>
                    Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut
                    odit aut fugiten, sed quia consequuntur magni dolores eos
                    qui ratione voluptatem sequi en lod nesciunt. Neque porro
                    quisquam est, qui dolorem ipsum quia dolor sit amet,
                    consectetur adipisci velit en lorem ipsum der.
                  </p>
                  <a href="#" className="post-add-icon inline-items">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                    <span>5</span>
                  </a>
                  <a href="#" className="reply">
                    Reply
                  </a>
                  <ul className="children">
                    <li>
                      <div className="post__author author vcard inline-items">
                        <img
                          src={`${PUBLIC_URL}/img/avatar8-sm.jpg`}
                          alt="author"
                        />
                        <div className="author-date">
                          <a className="h6 post__author-name fn" href="#">
                            Diana Jameson
                          </a>
                          <div className="post__date">
                            <time
                              className="published"
                              dateTime="2017-03-24T18:18"
                            >
                              39 mins ago
                            </time>
                          </div>
                        </div>
                        <a href="#" className="more">
                          <svg className="olymp-three-dots-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                            />
                          </svg>
                        </a>
                      </div>
                      <p>
                        Duis aute irure dolor in reprehenderit in voluptate
                        velit esse cillum dolore eu fugiat nulla pariatur.
                      </p>
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-heart-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                          />
                        </svg>
                        <span>2</span>
                      </a>
                      <a href="#" className="reply">
                        Reply
                      </a>
                    </li>
                    <li>
                      <div className="post__author author vcard inline-items">
                        <img
                          src={`${PUBLIC_URL}/img/avatar2-sm.jpg`}
                          alt="author"
                        />
                        <div className="author-date">
                          <a className="h6 post__author-name fn" href="#">
                            Nicholas Grisom
                          </a>
                          <div className="post__date">
                            <time
                              className="published"
                              dateTime="2017-03-24T18:18"
                            >
                              24 mins ago
                            </time>
                          </div>
                        </div>
                        <a href="#" className="more">
                          <svg className="olymp-three-dots-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                            />
                          </svg>
                        </a>
                      </div>
                      <p>Excepteur sint occaecat cupidatat non proident.</p>
                      <a href="#" className="post-add-icon inline-items">
                        <svg className="olymp-heart-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                          />
                        </svg>
                        <span>0</span>
                      </a>
                      <a href="#" className="reply">
                        Reply
                      </a>
                    </li>
                  </ul>
                </li>
                <li>
                  <div className="post__author author vcard inline-items">
                    <img
                      src={`${PUBLIC_URL}/img/avatar4-sm.jpg`}
                      alt="author"
                    />
                    <div className="author-date">
                      <a className="h6 post__author-name fn" href="#">
                        Chris Greyson
                      </a>
                      <div className="post__date">
                        <time className="published" dateTime="2017-03-24T18:18">
                          1 hour ago
                        </time>
                      </div>
                    </div>
                    <a href="#" className="more">
                      <svg className="olymp-three-dots-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                        />
                      </svg>
                    </a>
                  </div>
                  <p>
                    Dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                    cupidatat non proident, sunt in culpa qui officia deserunt
                    mollit.
                  </p>
                  <a href="#" className="post-add-icon inline-items">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                    <span>7</span>
                  </a>
                  <a href="#" className="reply">
                    Reply
                  </a>
                </li>
              </ul>
              <a href="#" className="more-comments">
                View more comments
                <span>+</span>
              </a>
              <form className="comment-form inline-items">
                <div className="post__author author vcard inline-items">
                  <img src={`${PUBLIC_URL}/img/author-page.jpg`} alt="author" />
                  <div className="form-group with-icon-right ">
                    <textarea className="form-control" defaultValue={''} />
                    <div className="add-options-message">
                      <a
                        href="#"
                        className="options-message"
                        data-toggle="modal"
                        data-target="#update-header-photo"
                      >
                        <svg className="olymp-camera-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-camera-icon`}
                          />
                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div className="ui-block">
              <article className="hentry post has-post-thumbnail shared-photo">
                <div className="post__author author vcard inline-items">
                  <img src={`${PUBLIC_URL}/img/author-page.jpg`} alt="author" />
                  <div className="author-date">
                    <a
                      className="h6 post__author-name fn"
                      href="02-ProfilePage.html"
                    >
                      James Spiegel
                    </a>{' '}
                    shared
                    <a href="#">Diana Jameson</a>’s
                    <a href="#">photo</a>
                    <div className="post__date">
                      <time className="published" dateTime="2017-03-24T18:18">
                        7 hours ago
                      </time>
                    </div>
                  </div>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <ul className="more-dropdown">
                      <li>
                        <a href="#">Edit Post</a>
                      </li>
                      <li>
                        <a href="#">Delete Post</a>
                      </li>
                      <li>
                        <a href="#">Turn Off Notifications</a>
                      </li>
                      <li>
                        <a href="#">Select as Featured</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <p>
                  Hi! Everyone should check out these amazing photographs that
                  my friend shot the past week. Here’s one of them...leave a
                  kind comment!
                </p>
                <div className="post-thumb">
                  <img src={`${PUBLIC_URL}/img/post-photo6.jpg`} alt="photo" />
                </div>
                <ul className="children single-children">
                  <li>
                    <div className="post__author author vcard inline-items">
                      <img
                        src={`${PUBLIC_URL}/img/avatar8-sm.jpg`}
                        alt="author"
                      />
                      <div className="author-date">
                        <a className="h6 post__author-name fn" href="#">
                          Diana Jameson
                        </a>
                        <div className="post__date">
                          <time
                            className="published"
                            dateTime="2017-03-24T18:18"
                          >
                            16 hours ago
                          </time>
                        </div>
                      </div>
                    </div>
                    <p>
                      Here’s the first photo of our incredible photoshoot from
                      yesterday. If you like it please say so and tel me what
                      you wanna see next!
                    </p>
                  </li>
                </ul>
                <div className="post-additional-info inline-items">
                  <a href="#" className="post-add-icon inline-items">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                    <span>15</span>
                  </a>
                  <ul className="friends-harmonic">
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic5.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic10.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic7.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic8.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <img
                          src={`${PUBLIC_URL}/img/friend-harmonic2.jpg`}
                          alt="friend"
                        />
                      </a>
                    </li>
                  </ul>
                  <div className="names-people-likes">
                    <a href="#">Diana</a>,
                    <a href="#">Nicholas</a> and
                    <br />13 more liked this
                  </div>
                  <div className="comments-shared">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-speech-balloon-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                        />
                      </svg>
                      <span>0</span>
                    </a>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                      <span>16</span>
                    </a>
                  </div>
                </div>
                <div className="control-block-button post-control-button">
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-like-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-comments-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-share-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                      />
                    </svg>
                  </a>
                </div>
              </article>
            </div>
          </div>
          <a
            id="load-more-button"
            href="#"
            className="btn btn-control btn-more"
            data-load-link="items-to-load.html"
            data-container="newsfeed-items-grid"
          >
            <svg className="olymp-three-dots-icon">
              <use
                xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
              />
            </svg>
          </a>
        </div>
        {/* ... end Main Content */}
        {/* Left Sidebar */}
        <div className="col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block">
            <div className="ui-block-title">
              <h6 className="title">Profile Intro</h6>
            </div>
            <div className="ui-block-content">
              <ul className="widget w-personal-info item-block">
                <li>
                  <span className="title">About me:</span>
                  <span className="text">{user.description || '-'}</span>
                </li>
              </ul>
              <SocialLinks
                facebookAccount={user.facebookAccount}
                instagramAccount={user.instagramAccount}
                twitterAccount={user.twitterAccount}
              />
            </div>
          </div>

          <div className="ui-block">
            <div className="ui-block-title">
              <h6 className="title">Last Videos</h6>
            </div>
            <div className="ui-block-content">
              <ul className="widget w-last-video">
                <li>
                  <a
                    href="https://vimeo.com/ondemand/viewfromabluemoon4k/147865858"
                    className="play-video play-video--small"
                  >
                    <svg className="olymp-play-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                      />
                    </svg>
                  </a>
                  <img src={`${PUBLIC_URL}/img/video8.jpg`} alt="video" />
                  <div className="video-content">
                    <div className="title">
                      System of a Revenge - Hypnotize...
                    </div>
                    <time className="published" dateTime="2017-03-24T18:18">
                      3:25
                    </time>
                  </div>
                  <div className="overlay" />
                </li>
                <li>
                  <a
                    href="https://youtube.com/watch?v=excVFQ2TWig"
                    className="play-video play-video--small"
                  >
                    <svg className="olymp-play-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                      />
                    </svg>
                  </a>
                  <img src={`${PUBLIC_URL}/img/video7.jpg`} alt="video" />
                  <div className="video-content">
                    <div className="title">Green Goo - Live at Dan’s Arena</div>
                    <time className="published" dateTime="2017-03-24T18:18">
                      5:48
                    </time>
                  </div>
                  <div className="overlay" />
                </li>
              </ul>
            </div>
          </div>
        </div>
        {/* ... end Left Sidebar */}
        {/* Right Sidebar */}
        <div className="col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block">
            <div className="ui-block-title">
              <h6 className="title">Last Photos</h6>
            </div>
            <div className="ui-block-content">
              <ul className="widget w-last-photo js-zoom-gallery">
                <li>
                  <a href="img/last-photo10-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-photo10-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
                <li>
                  <a href="img/last-phot11-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-phot11-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
                <li>
                  <a href="img/last-phot12-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-phot12-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
                <li>
                  <a href="img/last-phot13-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-phot13-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
                <li>
                  <a href="img/last-phot14-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-phot14-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
                <li>
                  <a href="img/last-phot15-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-phot15-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
                <li>
                  <a href="img/last-phot16-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-phot16-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
                <li>
                  <a href="img/last-phot17-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-phot17-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
                <li>
                  <a href="img/last-phot18-large.jpg">
                    <img
                      src={`${PUBLIC_URL}/img/last-phot18-large.jpg`}
                      alt="photo"
                    />
                  </a>
                </li>
              </ul>
            </div>
          </div>
          <FriendsBlock user={user} />
        </div>
      </div>
    </div>
  );
};

export default createFragmentContainer(
  Timeline,
  graphql`
    fragment Timeline_user on User {
      description
      facebookAccount
      instagramAccount
      twitterAccount
      ...FriendsBlock_user
    }
  `
);
