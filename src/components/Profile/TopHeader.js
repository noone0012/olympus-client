import React, { Component } from 'react';
import { Link } from 'found';
import { graphql, createFragmentContainer } from 'react-relay';
import DocumentTitle from 'react-document-title';

import UpdatePhoto from './UpdatePhoto';
import ChangeProfilePhotoMutation from '../../graphql/mutations/ChangeProfilePhotoMutation';
import ChangeHeaderPhotoMutation from '../../graphql/mutations/ChangeHeaderPhotoMutation';
import getLivesInValue from '../../utils/getLivesInValue';
import FriendshipButton from './FriendshipButton';

const { PUBLIC_URL } = process.env;

class TopHeader extends Component {
  render() {
    const {
      user: {
        fullName,
        profilePhoto,
        headerPhoto,
        id,
        city,
        country,
        isViewer,
        areFriends
      }
    } = this.props;

    return (
      <DocumentTitle title={fullName}>
        <div className="container">
          <div className="row">
            <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
              <div className="ui-block">
                <div className="top-header">
                  <div
                    className="top-header-thumb"
                    style={{ borderRadius: '5px 5px 0 0' }}
                  >
                    <img src={`${headerPhoto}`} alt="nature" />
                  </div>
                  <div className="profile-section">
                    <div className="row">
                      <div className="col-lg-5 col-md-5 ">
                        <ul className="profile-menu">
                          <li>
                            <Link exact to={`/${id}`} activeClassName="active">
                              Timeline
                            </Link>
                          </li>
                          <li>
                            <Link to={`/${id}/about`} activeClassName="active">
                              About
                            </Link>
                          </li>
                          <li>
                            <Link
                              to={`/${id}/friends`}
                              activeClassName="active"
                            >
                              Friends
                            </Link>
                          </li>
                        </ul>
                      </div>
                      <div className="col-lg-5 ml-auto col-md-5">
                        <ul className="profile-menu">
                          <li>
                            <Link to={`/${id}/photos`} activeClassName="active">
                              Photos
                            </Link>
                          </li>
                          <li>
                            <Link to={`/${id}/videos`} activeClassName="active">
                              Videos
                            </Link>
                          </li>
                          <li>
                            {!isViewer && (
                              <div className="more">
                                <svg className="olymp-three-dots-icon">
                                  <use
                                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                                  />
                                </svg>
                                <ul className="more-dropdown more-with-triangle">
                                  <li>
                                    <a href="">Report Profile</a>
                                  </li>
                                  <li>
                                    <a href="">Block Profile</a>
                                  </li>
                                </ul>
                              </div>
                            )}
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div className="control-block-button">
                      {!isViewer && (
                        <FriendshipButton
                          friendshipStatus={areFriends}
                          isViewer={isViewer}
                          userId={id}
                        />
                      )}
                      {!isViewer && (
                        <a className="btn btn-control bg-purple pointer">
                          <svg className="olymp-chat---messages-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
                            />
                          </svg>
                        </a>
                      )}
                      {isViewer && (
                        <div className="btn btn-control bg-primary more">
                          <svg className="olymp-settings-icon">
                            <use
                              xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-settings-icon`}
                            />
                          </svg>
                          <ul className="more-dropdown more-with-triangle triangle-bottom-right">
                            <li>
                              <UpdatePhoto
                                type="Profile"
                                mutation={ChangeProfilePhotoMutation}
                                userId={id}
                              />
                            </li>
                            <li>
                              <UpdatePhoto
                                type="Header"
                                mutation={ChangeHeaderPhotoMutation}
                                userId={id}
                              />
                            </li>
                            <li>
                              <Link
                                activeClassName="active"
                                to="/dashboard/account-settings"
                              >
                                Account Settings
                              </Link>
                            </li>
                          </ul>
                        </div>
                      )}
                    </div>
                  </div>
                  <div className="top-header-author">
                    <a className="author-thumb">
                      <img
                        src={profilePhoto}
                        alt="author"
                        style={{ width: '100%' }}
                      />
                    </a>
                    <div className="author-content">
                      <a href="02-ProfilePage.html" className="h4 author-name">
                        {fullName}
                      </a>
                      <div className="country">
                        {getLivesInValue(city, country)}
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </DocumentTitle>
    );
  }
}

export default createFragmentContainer(
  TopHeader,
  graphql`
    fragment TopHeader_user on User {
      id
      email
      fullName
      birthday
      profilePhoto
      headerPhoto
      city {
        name
      }
      country {
        name
      }
      isViewer
      areFriends
    }
  `
);
