import React, { Component } from 'react';

import environment from '../../graphql/environment';
import Modal from '../shared/Modal';

class UpdateHeaderPhoto extends Component {
  state = { modalOpen: false };

  handleToggle = e => {
    e && e.preventDefault();
    this.setState({ modalOpen: !this.state.modalOpen });
  };

  handleChange = async e => {
    const img = e.target.files[0];

    if (!img.type.startsWith('image')) {
      return this.handleToggle();
    }

    this.props.mutation.commit(
      environment,
      { files: [img] },
      this.props.userId
    );

    this.handleToggle();
  };

  render() {
    return (
      <Modal
        onClose={this.handleToggle}
        trigger={
          <a href="" onClick={this.handleToggle}>
            Update {this.props.type} Photo
          </a>
        }
        open={this.state.modalOpen}
      >
        <div className="modal" id="modal">
          <div className="modal-dialog ui-block window-popup update-header-photo">
            <a className="close icon-close" onClick={this.handleToggle}>
              <svg className="olymp-close-icon">
                <use
                  xlinkHref={`${
                    process.env.PUBLIC_URL
                  }/icons/icons.svg#olymp-close-icon`}
                />
              </svg>
            </a>
            <div className="ui-block-title">
              <h6 className="title">Update {this.props.type} Photo</h6>
            </div>
            <label htmlFor="upload" className="upload-photo-item">
              <svg className="olymp-computer-icon">
                <use
                  xlinkHref={`${
                    process.env.PUBLIC_URL
                  }/icons/icons.svg#olymp-computer-icon`}
                />
              </svg>
              <h6>Upload Photo</h6>
              <span>Browse your computer.</span>
            </label>
            <input
              type="file"
              accept="image/*"
              onChange={this.handleChange}
              id="upload"
              style={{ display: 'none' }}
            />
            <a
              href=""
              className="upload-photo-item"
              data-toggle="modal"
              data-target="#choose-from-my-photo"
            >
              <svg className="olymp-photos-icon">
                <use
                  xlinkHref={`${
                    process.env.PUBLIC_URL
                  }/icons/icons.svg#olymp-photos-icon`}
                />
              </svg>
              <h6>Choose from My Photos</h6>
              <span>Choose from your uploaded photos</span>
            </a>
          </div>
        </div>
      </Modal>
    );
  }
}

export default UpdateHeaderPhoto;
