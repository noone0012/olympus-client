import React from 'react';

const { PUBLIC_URL } = process.env;

const Videos = () => (
  <div>
    <div className="container">
      <div className="row">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div className="ui-block">
            <div className="ui-block-title inline-items">
              <div className="btn btn-control btn-control-small bg-yellow">
                <svg className="olymp-trophy-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-trophy-icon`}
                  />
                </svg>
              </div>
              <h6 className="title">James’s Featured Video</h6>
              <a href="#" className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container">
      <div className="row">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div className="ui-block features-video">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video9.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="video-content">
                <div className="h4 title">Rock Garden Festival - Day 3</div>
                <time className="published" dateTime="2017-03-24T18:18">
                  12:06
                </time>
              </div>
              <div className="overlay" />
            </div>
            <div className="features-video-content">
              <article className="hentry post">
                <div className="post__author author vcard inline-items">
                  <img src={`${PUBLIC_URL}/img/author-page.jpg`} alt="author" />
                  <div className="author-date">
                    <a
                      className="h6 post__author-name fn"
                      href="02-ProfilePage.html"
                    >
                      James Spiegel
                    </a>
                    <div className="post__date">
                      <time className="published" dateTime="2017-03-24T18:18">
                        2 hours ago
                      </time>
                    </div>
                  </div>
                  <div className="more">
                    <svg className="olymp-three-dots-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                      />
                    </svg>
                    <ul className="more-dropdown">
                      <li>
                        <a href="#">Edit Post</a>
                      </li>
                      <li>
                        <a href="#">Delete Post</a>
                      </li>
                      <li>
                        <a href="#">Turn Off Notifications</a>
                      </li>
                      <li>
                        <a href="#">Select as Featured</a>
                      </li>
                    </ul>
                  </div>
                </div>
                <p>
                  Last Saturday we went with <a href="#"> Mathilda Brinker</a>{' '}
                  to the “Rock Garden Festival” and had a blast! Here’s a small
                  video of one of us in the crowd.
                </p>
                <div className="post-additional-info inline-items">
                  <a href="#" className="post-add-icon inline-items">
                    <svg className="olymp-heart-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                      />
                    </svg>
                    <span>14</span>
                  </a>
                  <div className="comments-shared">
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-speech-balloon-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-speech-balloon-icon`}
                        />
                      </svg>
                      <span>19</span>
                    </a>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-share-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                        />
                      </svg>
                      <span>27</span>
                    </a>
                  </div>
                </div>
                <div className="control-block-button post-control-button">
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-like-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-like-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-comments-post-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                      />
                    </svg>
                  </a>
                  <a href="#" className="btn btn-control">
                    <svg className="olymp-share-icon">
                      <use
                        xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-share-icon`}
                      />
                    </svg>
                  </a>
                </div>
              </article>
              <div className="mCustomScrollbar" data-mcs-theme="dark">
                <ul className="comments-list">
                  <li>
                    <div className="post__author author vcard inline-items">
                      <img
                        src={`${PUBLIC_URL}/img/avatar48-sm.jpg`}
                        alt="author"
                      />
                      <div className="author-date">
                        <a className="h6 post__author-name fn" href="#">
                          Marina Valentine
                        </a>
                        <div className="post__date">
                          <time
                            className="published"
                            dateTime="2017-03-24T18:18"
                          >
                            46 mins ago
                          </time>
                        </div>
                      </div>
                      <a href="#" className="more">
                        <svg className="olymp-three-dots-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                          />
                        </svg>
                      </a>
                    </div>
                    <p>I had a great time too!! We should do it again!</p>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>8</span>
                    </a>
                    <a href="#" className="reply">
                      Reply
                    </a>
                  </li>
                  <li>
                    <div className="post__author author vcard inline-items">
                      <img
                        src={`${PUBLIC_URL}/img/avatar4-sm.jpg`}
                        alt="author"
                      />
                      <div className="author-date">
                        <a className="h6 post__author-name fn" href="#">
                          Chris Greyson
                        </a>
                        <div className="post__date">
                          <time
                            className="published"
                            dateTime="2017-03-24T18:18"
                          >
                            1 hour ago
                          </time>
                        </div>
                      </div>
                      <a href="#" className="more">
                        <svg className="olymp-three-dots-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                          />
                        </svg>
                      </a>
                    </div>
                    <p>
                      Dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                      cupidatat non proident, sunt in culpa qui officia deserunt
                      mollit.
                    </p>
                    <a href="#" className="post-add-icon inline-items">
                      <svg className="olymp-heart-icon">
                        <use
                          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-heart-icon`}
                        />
                      </svg>
                      <span>7</span>
                    </a>
                    <a href="#" className="reply">
                      Reply
                    </a>
                  </li>
                </ul>
              </div>
              <form className="comment-form inline-items">
                <div className="post__author author vcard inline-items">
                  <img src={`${PUBLIC_URL}/img/avatar73-sm.jpg`} alt="author" />
                  <div className="form-group with-icon-right ">
                    <textarea
                      className="form-control"
                      placeholder="Press Enter to post..."
                      defaultValue={''}
                    />
                    <div className="add-options-message">
                      <a
                        href="#"
                        className="options-message"
                        data-toggle="modal"
                        data-target="#update-header-photo"
                      >
                        <svg className="olymp-camera-icon">
                          <use
                            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-camera-icon`}
                          />
                        </svg>
                      </a>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container">
      <div className="row">
        <div className="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
          <div className="ui-block">
            <div className="ui-block-title">
              <div className="h6 title">James’s Videos</div>
              <div className="align-right">
                <a
                  href="#"
                  className="btn btn-primary btn-md-2"
                  data-toggle="modal"
                  data-target="#update-header-photo"
                >
                  Upload Video +
                </a>
              </div>
              <a href="#" className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="container">
      <div className="row">
        <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block video-item">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video10.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="overlay overlay-dark" />
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </div>
            <div className="ui-block-content video-content">
              <a href="#" className="h6 title">
                Rock Garden Festival - Day 3
              </a>
              <time className="published" dateTime="2017-03-24T18:18">
                18:44
              </time>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block video-item">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video11.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video play-video--small"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="overlay overlay-dark" />
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </div>
            <div className="ui-block-content video-content">
              <a href="#" className="h6 title">
                Rock Garden Festival - Day 2
              </a>
              <time className="published" dateTime="2017-03-24T18:18">
                13:19
              </time>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block video-item">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video12.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="overlay overlay-dark" />
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </div>
            <div className="ui-block-content video-content">
              <a href="#" className="h6 title">
                Rock Garden Festival - Day 1
              </a>
              <time className="published" dateTime="2017-03-24T18:18">
                15:47
              </time>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block video-item">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video13.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video play-video--small"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="overlay overlay-dark" />
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </div>
            <div className="ui-block-content video-content">
              <a href="#" className="h6 title">
                The Best Burgers in the State!
              </a>
              <time className="published" dateTime="2017-03-24T18:18">
                0:23
              </time>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block video-item">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video14.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video play-video--small"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="overlay overlay-dark" />
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </div>
            <div className="ui-block-content video-content">
              <a href="#" className="h6 title">
                Touring Manhattan Parks
              </a>
              <time className="published" dateTime="2017-03-24T18:18">
                12:08
              </time>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block video-item">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video15.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video play-video--small"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="overlay overlay-dark" />
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </div>
            <div className="ui-block-content video-content">
              <a href="#" className="h6 title">
                Sandwich from Mario’s
              </a>
              <time className="published" dateTime="2017-03-24T18:18">
                5:54
              </time>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block video-item">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video16.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video play-video--small"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="overlay overlay-dark" />
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </div>
            <div className="ui-block-content video-content">
              <a href="#" className="h6 title">
                Into the Amazon Jungle
              </a>
              <time className="published" dateTime="2017-03-24T18:18">
                24:36
              </time>
            </div>
          </div>
        </div>
        <div className="col-xl-3 col-lg-3 col-md-6 col-sm-12 col-xs-12">
          <div className="ui-block video-item">
            <div className="video-player">
              <img src={`${PUBLIC_URL}/img/video17.jpg`} alt="photo" />
              <a
                href="https://youtube.com/watch?v=excVFQ2TWig"
                className="play-video play-video--small"
              >
                <svg className="olymp-play-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-play-icon`}
                  />
                </svg>
              </a>
              <div className="overlay overlay-dark" />
              <div className="more">
                <svg className="olymp-three-dots-icon">
                  <use
                    xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                  />
                </svg>
              </div>
            </div>
            <div className="ui-block-content video-content">
              <a href="#" className="h6 title">
                Record Store Day 2016
              </a>
              <time className="published" dateTime="2017-03-24T18:18">
                7:52
              </time>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
);

export default Videos;
