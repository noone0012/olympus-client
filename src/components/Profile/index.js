import React from 'react';

import TopHeader from './TopHeader';

const Profile = ({ user, children }) => {
  return (
    <div>
      <TopHeader user={user} />
      {children}
    </div>
  );
};

export default Profile;
