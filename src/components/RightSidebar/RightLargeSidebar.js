import React from 'react';
import { Link } from 'found';
import { connect } from 'react-redux';

import { toggleChatState } from '../../actions';

const { PUBLIC_URL } = process.env;

const RightLargeSidebar = ({ viewer, closeLargeSidebar, openChat }) => (
  <div className="fixed-sidebar-right sidebar--large">
    <div className="mCustomScrollbar">
      <div className="ui-block-title ui-block-title-small">
        <span className="title">Friends</span>
      </div>

      <ul className="chat-users">
        {viewer.friends.edges.map(edge => (
          <li className="inline-items" key={edge.node.id}>
            <Link to={`/${edge.node.id}`}>
              <div className="author-thumb">
                <img
                  alt="author"
                  src={edge.node.profilePhoto}
                  className="avatar"
                />
                <span className="icon-status online" />
              </div>
            </Link>

            <Link to={`/${edge.node.id}`}>
              <div className="author-status">
                <span className="h6 author-name">{edge.node.fullName}</span>
                <span className="status">ONLINE</span>
              </div>
            </Link>

            <div className="more">
              <svg className="olymp-three-dots-icon">
                <use
                  xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-three-dots-icon`}
                />
              </svg>

              <ul className="more-icons">
                <li onClick={() => openChat(edge.node)}>
                  <svg
                    data-toggle="tooltip"
                    data-placement="top"
                    data-original-title="START CONVERSATION"
                    className="olymp-comments-post-icon"
                  >
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-comments-post-icon`}
                    />
                  </svg>
                </li>

                <li>
                  <svg
                    data-toggle="tooltip"
                    data-placement="top"
                    data-original-title="ADD TO CONVERSATION"
                    className="olymp-add-to-conversation-icon"
                  >
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-add-to-conversation-icon`}
                    />
                  </svg>
                </li>

                <li>
                  <svg
                    data-toggle="tooltip"
                    data-placement="top"
                    data-original-title="BLOCK FROM CHAT"
                    className="olymp-block-from-chat-icon"
                  >
                    <use
                      xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-block-from-chat-icon`}
                    />
                  </svg>
                </li>
              </ul>
            </div>
          </li>
        ))}
      </ul>
    </div>

    <div className="search-friend inline-items">
      <form className="form-group">
        <input
          className="form-control"
          placeholder="Search Friends..."
          type="text"
        />
      </form>

      <Link to="/dashboard/account-settings" className="settings">
        <svg className="olymp-settings-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-settings-icon`}
          />
        </svg>
      </Link>

      <a onClick={closeLargeSidebar} className="js-sidebar-open pointer">
        <svg className="olymp-close-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-close-icon`} />
        </svg>
      </a>
    </div>

    <Link to="/dashboard/chat-messages" className="olympus-chat inline-items">
      <h6 className="olympus-chat-title">OLYMPUS CHAT</h6>
      <svg className="olymp-chat---messages-icon">
        <use
          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
        />
      </svg>
    </Link>
  </div>
);

const mapDispatchToProps = (dispatch, ownProps) => ({
  openChat: user => dispatch(toggleChatState(true, user, ownProps.viewer))
});

export default connect(null, mapDispatchToProps)(RightLargeSidebar);
