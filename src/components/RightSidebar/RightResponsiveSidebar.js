import React from 'react';
import { Link } from 'found';

const { PUBLIC_URL } = process.env;

const RightResponsiveSidebar = ({ openChat }) => (
  <div className="fixed-sidebar right fixed-sidebar-responsive">
    <div
      className="fixed-sidebar-right sidebar--small"
      id="sidebar-right-responsive"
    >
      <Link to="/dashboard/chat-messages" className="olympus-chat inline-items">
        <svg className="olymp-chat---messages-icon">
          <use
            xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
          />
        </svg>
      </Link>
    </div>
  </div>
);

export default RightResponsiveSidebar;
