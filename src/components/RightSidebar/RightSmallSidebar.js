import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'found';

import { toggleChatState } from '../../actions';

const { PUBLIC_URL } = process.env;

const RightSmallSidebar = ({
  openChat,
  openLargeSidebar,
  viewer: { friends }
}) => (
  <div className="fixed-sidebar-right sidebar--small">
    <div className="mCustomScrollbar">
      <ul className="chat-users">
        {friends.edges.map(edge => (
          <li
            className="inline-items"
            onClick={() => openChat(edge.node)}
            key={edge.node.id}
          >
            <div className="author-thumb">
              <img alt="author" src={edge.node.profilePhoto} />
              <span className="icon-status online" />
            </div>
          </li>
        ))}
      </ul>
    </div>

    <div className="search-friend inline-items">
      <a onClick={openLargeSidebar} className="js-sidebar-open pointer">
        <svg className="olymp-menu-icon">
          <use xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-menu-icon`} />
        </svg>
      </a>
    </div>

    <Link to="/dashboard/chat-messages" className="olympus-chat inline-items">
      <svg className="olymp-chat---messages-icon">
        <use
          xlinkHref={`${PUBLIC_URL}/icons/icons.svg#olymp-chat---messages-icon`}
        />
      </svg>
    </Link>
  </div>
);

const mapDispatchToProps = (dispatch, ownProps) => ({
  openChat: user => dispatch(toggleChatState(true, user, ownProps.viewer))
});

export default connect(null, mapDispatchToProps)(RightSmallSidebar);
