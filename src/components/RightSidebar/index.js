import React, { Component } from 'react';

import RightSmallSidebar from './RightSmallSidebar';
import RightLargeSidebar from './RightLargeSidebar';
import RightResponsiveSidebar from './RightResponsiveSidebar';
import { createFragmentContainer, graphql } from 'react-relay';

class RightSidebar extends Component {
  state = { isOpen: false };

  toggleOpenState = () => {
    this.setState({ isOpen: !this.state.isOpen });
  };

  onClick = e => {
    if (
      this.state.isOpen &&
      !e.target.closest('.sidebar--large.fixed-sidebar-right')
    ) {
      this.setState({ isOpen: false });
    }
  };

  componentDidUpdate(prevProps, prevState) {
    const { isOpen } = this.state;

    if (isOpen) {
      document.addEventListener('click', this.onClick);
    } else {
      document.removeEventListener('click', this.onClick);
    }
  }

  render() {
    const { viewer } = this.props;

    return (
      <div>
        <div
          className={`fixed-sidebar right ${this.state.isOpen ? 'open' : ''}`}
        >
          <RightSmallSidebar
            viewer={viewer}
            openLargeSidebar={this.toggleOpenState}
          />
          <RightLargeSidebar
            viewer={viewer}
            closeLargeSidebar={this.toggleOpenState}
          />
        </div>
        <RightResponsiveSidebar />
      </div>
    );
  }
}

export default createFragmentContainer(
  RightSidebar,
  graphql`
    fragment RightSidebar_viewer on Viewer {
      id
      fullName
      profilePhoto
      friends {
        edges {
          cursor
          node {
            id
            firstName
            fullName
            profilePhoto
          }
        }
      }
    }
  `
);
