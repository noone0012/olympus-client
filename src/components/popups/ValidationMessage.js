import React, { Component } from 'react';

import Modal from '../shared/Modal';
import { connect } from 'react-redux';
import { toggleValidationMessageState as toggle } from '../../actions';

class ValidatonMessage extends Component {
  render() {
    const { toggle, open, message } = this.props;

    return (
      <Modal onClose={toggle} open={open} trigger={null}>
        <div className="modal" id="modal">
          <div
            className="modal-dialog ui-block window-popup update-header-photo"
            style={{ marginTop: '200px', width: '400px' }}
          >
            <a className="close icon-close" onClick={() => toggle()}>
              <svg className="olymp-close-icon">
                <use
                  xlinkHref={`${
                    process.env.PUBLIC_URL
                  }/icons/icons.svg#olymp-close-icon`}
                />
              </svg>
            </a>
            <div className="ui-block-title">
              <h6 className="title">Validation Message</h6>
            </div>
            <div className="ui-block-content">{message}</div>
          </div>
        </div>
      </Modal>
    );
  }
}

export default connect(state => state.validationMessage, { toggle })(
  ValidatonMessage
);
