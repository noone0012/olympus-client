import React, { Component } from 'react';
import $ from 'jquery';
import moment from 'moment';
import { Field } from 'react-form';
import 'daterangepicker';

import multipleValidators from '../../utils/multipleValidators';

class DatePicker extends Component {
  setValue: Function;

  componentDidMount() {
    const dateField = $('input.datetimepicker');
    const that = this;

    if (!dateField.length) {
      return;
    }

    dateField.daterangepicker({
      startDate: moment(),
      maxDate: moment(),
      minDate: moment().subtract(70, 'years'),
      autoUpdateInput: false,
      singleDatePicker: true,
      showDropdowns: true
    });

    dateField
      .on('focus', function() {
        $(this)
          .closest('.form-group')
          .addClass('is-focused');
      })
      .on('apply.daterangepicker', function(ev, picker) {
        that.setValue(picker.startDate.format());

        $(this)
          .closest('.form-group')
          .removeClass('is-focused');
      });
  }

  componentWillUnmount() {
    $('input.datetimepicker').off('focus apply.daterangepicker');
  }

  render() {
    const { label, validate, field, ...rest } = this.props;

    return (
      <Field validate={multipleValidators(validate)} field={field}>
        {fieldApi => {
          if (!this.setValue) {
            this.setValue = fieldApi.setValue;
          }

          let { value, touched, setTouched } = fieldApi;
          value = value && new Date(value).toISOString();
          value = value && moment(value).format('DD/MM/YYYY');
          value = value || '';

          return (
            <div className="form-group date-time-picker label-floating">
              <label className="control-label">{label}</label>
              <input
                {...rest}
                readOnly
                className="datetimepicker"
                value={value}
                onBlur={() => {
                  !touched && setTouched();
                }}
              />
              <span className="input-group-addon">
                <svg className="olymp-calendar-icon">
                  <use
                    xlinkHref={`${
                      process.env.PUBLIC_URL
                    }/icons/icons.svg#olymp-calendar-icon`}
                  />
                </svg>
              </span>
            </div>
          );
        }}
      </Field>
    );
  }
}

export default DatePicker;
