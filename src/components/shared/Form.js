import React, { Component } from 'react';
import { Form } from 'react-form';
import { isEqual } from 'lodash';

class CustomForm extends Component {
  state = { previousSubmitErrors: {}, previousValues: {} };

  getErrors = formState => {
    const submitErrors = {};
    const syncErrors = {};

    for (let key in formState.errors) {
      // sync errors
      if (formState.touched[key]) {
        syncErrors[key] = formState.errors[key];
        continue;
      }

      submitErrors[key] = formState.errors[key];
    }

    return { syncErrors, submitErrors };
  };

  getNewErrors = (syncErrors, submitErrors, previousSubmitErrors) => {
    const haveNotBeenShownErrors = {};

    for (let i in submitErrors) {
      if (previousSubmitErrors[i]) {
        continue;
      }

      haveNotBeenShownErrors[i] = submitErrors[i];
    }

    return {
      ...syncErrors,
      ...haveNotBeenShownErrors
    };
  };

  handleChange = (formState, formApi) => {
    if (!formState.errors) {
      return;
    }

    if (isEqual(this.state.previousValues, formState.values)) {
      return;
    }

    const { submitErrors, syncErrors } = this.getErrors(formState);

    this.setState({
      previousValues: formState.values,
      previousSubmitErrors: submitErrors
    });

    const newErrors = this.getNewErrors(
      syncErrors,
      submitErrors,
      this.state.previousSubmitErrors
    );

    formApi.setFormState({
      ...formState,
      errors: newErrors
    });
  };

  render() {
    return <Form {...this.props} onChange={this.handleChange} />;
  }
}

export default CustomForm;
