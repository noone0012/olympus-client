import React from 'react';

const FormErrors = ({ errors, touched }) => {
  for (let key in errors) {
    const err = errors[key];
    const isTouched = touched[key];

    // if this is an array form
    if (Array.isArray(err)) {
      const errors = [];

      for (let i in err) {
        errors.push(FormErrors(err[i], touched[i]));
      }

      return errors[0];
    }

    if (err && isTouched !== false) {
      return <p className="error">{err}</p>;
    }
  }

  const common = errors && errors['_error'];

  if (common) {
    return <p className="error">{common}</p>;
  } else {
    return null;
  }
};

export default FormErrors;
