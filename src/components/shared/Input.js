import React, { Component } from 'react';
import classNames from 'classnames';
import { Field } from 'react-form';

import multipleValidators from '../../utils/multipleValidators';

class Input extends Component {
  state = { isFocused: false };

  toggleFocus = isFocused => {
    this.setState({ isFocused });
  };

  getRealValue = (prefix, value) => {
    return value.replace(new RegExp(`${prefix}?`, 'i'), '');
  };

  componentDidMount() {
    if (this.props.autoFocus) {
      this.toggleFocus(true);
    }
  }

  render() {
    const {
      validate,
      field,
      label,
      type = 'text',
      icon,
      prefix,
      getNode,
      onEnter,
      ...rest
    } = this.props;

    return (
      <Field
        validate={multipleValidators(validate) || function() {}}
        field={field}
        pure={false}
      >
        {fieldApi => {
          let { value, error, setValue, touched, setTouched } = fieldApi;

          if (!value) {
            value = '';
          }

          if (!!prefix && value !== '') {
            value = prefix + value;
          }

          const notEmpty = value && value.trim() !== '';

          return (
            <div
              className={classNames(`form-group label-floating`, {
                'is-focused': this.state.isFocused,
                'is-empty': !notEmpty,
                'has-danger': touched && error,
                // 'has-success': touched && notEmpty,
                'with-icon': !!icon
              })}
            >
              <label className="control-label">{label}</label>
              <input
                {...rest}
                ref={getNode}
                autoComplete="off"
                className="form-control pr-md-5"
                type={type}
                value={value}
                onKeyDown={e => {
                  if (e.key === 'Enter') {
                    e.preventDefault();
                    onEnter && onEnter();
                  }
                }}
                onFocus={() => this.toggleFocus(true)}
                onBlur={() => {
                  this.toggleFocus(false);
                }}
                onChange={e => {
                  !touched && setTouched();

                  if (!prefix) {
                    setValue(e.target.value);
                  } else {
                    setValue(this.getRealValue(prefix, e.target.value));
                  }
                }}
              />
              {icon && (
                <i className={`fa fa-${icon} c-${icon}`} aria-hidden="true" />
              )}
            </div>
          );
        }}
      </Field>
    );
  }
}

export default Input;
