import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Transition } from 'semantic-ui-react';

type Props = {
  open: Boolean,
  onClose: Function,
  trigger: React.Node
};

class Modal extends Component<Props> {
  domNode = document.getElementsByTagName('body')[0];
  node = document.createElement('div');

  componentWillReceiveProps(nextProps) {
    if (nextProps.open) {
      this.domNode.appendChild(this.node);
      document.addEventListener('click', this.tryToClose);
    } else {
      if (!this.domNode.contains(this.node)) {
        return;
      }

      this.domNode.removeChild(this.node);
      document.removeEventListener('click', this.tryToClose);

      document.body.style.overflowY = 'scroll';
      document.querySelector('#modal').style.overflowY = 'hidden';
    }
  }

  tryToClose = e => {
    const clickedToModal = e.target.closest('.modal-dialog');

    if (clickedToModal) {
      return;
    }

    this.props.onClose();
  };

  render() {
    const { trigger, open, onShow, children } = this.props;

    return (
      <React.Fragment>
        {trigger}
        {ReactDOM.createPortal(
          <Transition
            visible={open}
            animation="scale"
            duration={{ hide: 200, show: 300 }}
            unmountOnHide
            onShow={() => {
              document.body.style.overflowY = 'hidden';
              document.querySelector('#modal').style.overflowY = 'scroll';

              onShow && onShow();
            }}
          >
            {children}
          </Transition>,
          this.domNode
        )}
        {ReactDOM.createPortal(
          <div className="modal-backdrop fade show" />,
          this.node
        )}
      </React.Fragment>
    );
  }
}

export default Modal;
