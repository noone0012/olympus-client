import React, { Component } from 'react';

type Props = {
  relay: object,
  className: string,
  pageSize: number
};

class ScrollToPaginate extends Component<Props> {
  handleScroll = e => {
    const { relay: { isLoading, hasMore, loadMore }, pageSize } = this.props;

    if (isLoading() || !hasMore()) {
      return;
    }

    const el = e.target;
    const scrollable = el.scrollHeight - el.clientHeight;
    const scrolled = el.scrollTop;

    if (scrolled / scrollable * 100 > 75) {
      loadMore(pageSize);
    }
  };

  render() {
    const { className } = this.props;

    return (
      <ul className={className} onScroll={this.handleScroll}>
        {this.props.children}
      </ul>
    );
  }
}

export default ScrollToPaginate;
