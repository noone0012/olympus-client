import React, { Component } from 'react';
import $ from 'jquery';
import { Field } from 'react-form';

import multipleValidators from '../../utils/multipleValidators';

class Select extends Component {
  componentDidMount() {
    $('.selectpicker').selectpicker({});
  }

  render() {
    const {
      validate,
      field,
      label,
      options,
      renderEmpty = false,
      onChange,
      ...rest
    } = this.props;

    return (
      <Field validate={multipleValidators(validate)} field={field} pure={false}>
        {fieldApi => {
          let { value, setValue, touched, setTouched, error } = fieldApi;

          if (!value) {
            value = '';
          }

          $(`select#${field}`).selectpicker('val', value);
          $(`select#${field}`).selectpicker(
            'setStyle',
            'border-red',
            (!touched || !error) && 'remove'
          );

          return (
            <div className="form-group label-floating is-select">
              <label className="control-label">{label}</label>
              <select
                {...rest}
                className="selectpicker form-control"
                value={value}
                id={field}
                onBlur={() => {
                  !touched && setTouched();
                }}
                onChange={e => {
                  onChange && onChange(e.target.value);
                  setValue(e.target.value);
                }}
              >
                {renderEmpty && <option value={null} />}
                {options.map(o => (
                  <option key={o.value} value={o.value}>
                    {o.text}
                  </option>
                ))}
              </select>
            </div>
          );
        }}
      </Field>
    );
  }
}

export default Select;
