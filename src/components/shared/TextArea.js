import React, { Component } from 'react';
import { Field } from 'react-form';
import classnames from 'classnames';

import multipleValidators from '../../utils/multipleValidators';

class TextArea extends Component {
  state = { isFocused: false };

  toggleFocus = isFocused => {
    this.setState({ isFocused });
  };

  render() {
    const { validate, field, label, ...rest } = this.props;

    return (
      <Field
        validate={multipleValidators(validate) || function() {}}
        field={field}
        pure={false}
      >
        {fieldApi => {
          let { value, setValue, touched, error, setTouched } = fieldApi;

          if (!value) {
            value = '';
          }

          const isEmpty = value === '';

          return (
            <div
              className={classnames('form-group label-floating', {
                'is-empty': isEmpty,
                'is-focused': this.state.isFocused,
                'border-red': touched && error
              })}
            >
              <label className="control-label">{label}</label>
              <textarea
                {...rest}
                className="form-control"
                value={value}
                onBlur={() => {
                  !touched && setTouched();
                  this.toggleFocus(false);
                }}
                onFocus={() => this.toggleFocus(true)}
                onChange={e => setValue(e.target.value)}
              />
            </div>
          );
        }}
      </Field>
    );
  }
}

export default TextArea;
