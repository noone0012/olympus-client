import React from 'react';
import { Field } from 'react-form';

import multipleValidators from '../../utils/multipleValidators';

const Toggle = ({ validate, field, ...rest }) => {
  return (
    <Field validate={multipleValidators(validate)} field={field}>
      {fieldApi => {
        const { value = false, setValue, touched, setTouched } = fieldApi;

        return (
          <div className="togglebutton">
            <label>
              <input
                {...rest}
                type="checkbox"
                checked={value}
                onBlur={() => {
                  !touched && setTouched();
                }}
                onChange={e => {
                  setValue(e.target.checked);
                }}
              />
              <span className="toggle" />
            </label>
          </div>
        );
      }}
    </Field>
  );
};

export default Toggle;
