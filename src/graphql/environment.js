import { Environment, RecordSource, Store, Network } from 'relay-runtime';
import { SubscriptionClient } from 'subscriptions-transport-ws';

import getTokens from '../utils/getTokens';

const host =
  process.env.NODE_ENV === 'development'
    ? 'localhost:1200'
    : 'ec2-54-152-34-76.compute-1.amazonaws.com';

function setupSubscription(config, variables, cacheConfig, observer) {
  const query = config.text;

  const subscriptionClient = new SubscriptionClient(
    `ws://${host}/subscriptions`,
    {
      reconnect: true,
      connectionParams: {
        accessToken: getTokens().accessToken
      }
    }
  );

  const subscription = subscriptionClient
    .request({ query, variables })
    .subscribe(
      result => {
        observer.onNext(result);
      },
      error => observer.onError(error),
      () => observer.onCompleted()
    );

  return {
    dispose: subscription.unsubscribe
  };
}

function fetchQuery(operation, variables) {
  // initialize body
  let body;

  // initialize headers
  const headers = new Headers();

  const { input } = variables;

  // convert all empty string values into null
  if (input) {
    Object.keys(input).forEach(field => {
      if (typeof input[field] === 'string' && input[field] === '') {
        input[field] = null;
      }
    });
  }

  // get stringified body
  const getBody = (operation, variables) =>
    JSON.stringify({
      query: operation.text,
      variables
    });

  // send formData if needed
  if (variables.input && variables.input.files) {
    body = new FormData();
    body.append('operation', getBody(operation, variables));

    Array.from(variables.input.files).forEach(file => {
      body.append('files', file);
    });
  } else {
    body = getBody(operation, variables);
    headers.set('Content-type', 'application/json');
  }

  // construct request object
  const request = new Request(`http://${host}/graphql`, {
    method: 'POST',
    headers,
    body
  });

  // standart query function
  const sendRequest = () => {
    request.headers.set('x-access-token', getTokens().accessToken);

    return fetch(request.clone()).then(response => {
      if (!response.ok) {
        return Promise.reject(response);
      }

      return response.json();
    });
  };

  // refresh token
  const updateToken = () => {
    return fetch(`http://${host}/refresh_token`, {
      headers: { 'x-refresh-token': getTokens().refreshToken }
    })
      .then(res => {
        if (!res.ok || res.status === 201) {
          return Promise.reject();
        }

        return res.json();
      })
      .then(json => {
        localStorage.setItem('acst', json.accessToken);
        return sendRequest();
      })
      .catch(e => {
        localStorage.clear();
        window.location.href = '/login';
      });
  };

  // finally send request
  return sendRequest().then(json => {
    try {
      if (json.errors[0].state._authError) {
        return updateToken();
      }
    } catch (e) {}

    return json;
  });
}

const environment = new Environment({
  network: Network.create(fetchQuery, setupSubscription),
  store: new Store(new RecordSource())
});

export default environment;
