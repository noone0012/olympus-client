import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation AccountSettingsMutation($input: EditUserInput!) {
    editUser(input: $input) {
      viewer {
        ...AccountSettings_viewer
      }
    }
  }
`;

function commit(environment, input) {
  return commitMutation(environment, {
    mutation,
    variables: { input }
  });
}

export default { commit };
