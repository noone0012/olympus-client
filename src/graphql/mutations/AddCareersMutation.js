import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation AddCareersMutation($input: AddCareersInput!) {
    addCareers(input: $input) {
      addedCareers {
        id
        title
        from
        to
        description
      }
    }
  }
`;

function commit(environment, input) {
  return commitMutation(environment, {
    mutation,
    variables: { input }
  });
}

export default { commit };
