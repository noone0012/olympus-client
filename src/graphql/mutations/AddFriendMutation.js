import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation AddFriendMutation($input: AddFriendInput!) {
    addFriend(input: $input) {
      friendshipStatus
    }
  }
`;

function commit(environment, input, friendshipId) {
  return commitMutation(environment, {
    mutation,
    variables: { input },
    updater: store => {
      const friendshipStatus = store
        .getRootField('addFriend')
        .getValue('friendshipStatus');

      const userProxy = store.get(input.userId);
      userProxy.setValue(friendshipStatus, 'areFriends');

      if (friendshipId) {
        const accepted = friendshipStatus === 'IS_A_FRIEND';
        store.get(friendshipId).setValue(accepted, 'accepted');
      }
    }
  });
}

export default { commit };
