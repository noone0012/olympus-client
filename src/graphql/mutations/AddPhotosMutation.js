import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';
import { ConnectionHandler } from 'relay-runtime';

const mutation = graphql`
  mutation AddPhotosMutation($input: AddPhotosInput!) {
    addPhotos(input: $input) {
      photosEdge {
        cursor
        node {
          id
          url
        }
      }
    }
  }
`;

function commit(environment, input, userId) {
  return commitMutation(environment, {
    mutation,
    variables: { input },
    updater(store) {
      const payload = store.getRootField('addPhotos');

      // if received an error
      if (!payload) {
        return;
      }

      payload.getLinkedRecord('photosEdge');
    }
  });
}

export default { commit };

// const albumsConnection = ConnectionHandler.getConnection(
//   store.get(userId),
//   'AlbumsPage_albums'
// );

// if (albumsConnection) {
//   store
//     .get(input.albumId)
//     .getLinkedRecord('photos')
//     .setValue(photosEdge.length, 'totalCount');
// }
