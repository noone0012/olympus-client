import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation ChangeHeaderPhotoMutation($input: ChangeHeaderPhotoInput!) {
    changeHeaderPhoto(input: $input) {
      viewer {
        headerPhoto
      }
    }
  }
`;

function getOptimisticResponse(photo, userId) {
  return {
    changeHeaderPhoto: {
      viewer: {
        id: userId,
        headerPhoto: window.URL.createObjectURL(photo)
      }
    }
  };
}

function commit(environment, input, userId) {
  return commitMutation(environment, {
    mutation,
    variables: { input },
    updater: store => {
      const headerPhoto = store
        .getRootField('changeHeaderPhoto')
        .getLinkedRecord('viewer')
        .getValue('headerPhoto');

      const userProxy = store.get(userId);
      userProxy.setValue(`${headerPhoto}?${Date.now()}`, 'headerPhoto');
    },
    optimisticResponse: getOptimisticResponse(input.files[0], userId)
  });
}

export default { commit };
