import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation ChangePasswordMutation($input: ChangePasswordInput!) {
    changePassword(input: $input) {
      message
      refreshToken
    }
  }
`;

function commit(environment, input) {
  return commitMutation(environment, {
    mutation,
    variables: { input }
  });
}

export default { commit };
