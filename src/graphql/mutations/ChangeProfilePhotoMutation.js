import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation ChangeProfilePhotoMutation($input: ChangeProfilePhotoInput!) {
    changeProfilePhoto(input: $input) {
      viewer {
        profilePhoto
      }
    }
  }
`;

function getOptimisticResponse(photo, userId) {
  return {
    changeProfilePhoto: {
      viewer: {
        id: userId,
        profilePhoto: window.URL.createObjectURL(photo)
      }
    }
  };
}

function commit(environment, input, userId) {
  return commitMutation(environment, {
    mutation,
    variables: { input },
    updater: store => {
      const profilePhoto = store
        .getRootField('changeProfilePhoto')
        .getLinkedRecord('viewer')
        .getValue('profilePhoto');

      const userProxy = store.get(userId);
      userProxy.setValue(`${profilePhoto}?${Date.now()}`, 'profilePhoto');
    },
    optimisticResponse: getOptimisticResponse(input.files[0], userId)
  });
}

export default { commit };
