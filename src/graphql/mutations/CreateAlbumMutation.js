import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';
import { ConnectionHandler } from 'relay-runtime';

const mutation = graphql`
  mutation CreateAlbumMutation($input: CreateAlbumInput!) {
    createAlbum(input: $input) {
      albumEdge {
        cursor
        node {
          id
          name
        }
      }
    }
  }
`;

function commit(environment, input, userId) {
  return commitMutation(environment, {
    mutation,
    variables: { input },
    updater(store) {
      const payload = store.getRootField('createAlbum');

      const conn = ConnectionHandler.getConnection(
        store.get(userId),
        'AlbumsPage_albums'
      );

      // if received an error or we weren't on Albums page
      if (!payload || !conn) {
        return;
      }

      const newAlbum = payload.getLinkedRecord('albumEdge');
      const photoConnection = store
        .create(`client:newConnection:${Date.now()}`, 'PhotoConnection')
        .setValue(0, 'totalCount');
      newAlbum
        .getLinkedRecord('node')
        .setLinkedRecord(photoConnection, 'photos');

      ConnectionHandler.insertEdgeAfter(conn, newAlbum);
    }
  });
}

export default { commit };
