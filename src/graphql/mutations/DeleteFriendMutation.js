import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation DeleteFriendMutation($input: DeleteFriendInput!) {
    deleteFriend(input: $input) {
      friendshipStatus
    }
  }
`;

function commit(environment, input) {
  return commitMutation(environment, {
    mutation,
    variables: { input },
    updater: store => {
      const friendshipStatus = store
        .getRootField('deleteFriend')
        .getValue('friendshipStatus');

      const userProxy = store.get(input.userId);
      userProxy.setValue(friendshipStatus, 'areFriends');
    }
  });
}

export default { commit };
