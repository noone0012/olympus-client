import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation EditCareersMutation($input: EditCareersInput!) {
    editCareers(input: $input) {
      editedCareers {
        id
        title
        from
        to
        description
      }
    }
  }
`;

function commit(environment, input) {
  return commitMutation(environment, {
    mutation,
    variables: { input }
  });
}

export default { commit };
