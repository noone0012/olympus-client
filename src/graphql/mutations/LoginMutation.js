import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation LoginMutation($input: LoginInput!) {
    login(input: $input) {
      accessToken
      refreshToken
    }
  }
`;

function commit(environment, input) {
  return commitMutation(environment, {
    mutation,
    variables: { input }
  });
}

export default { commit };
