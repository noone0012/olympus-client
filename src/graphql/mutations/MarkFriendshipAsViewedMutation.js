import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation MarkFriendshipAsViewedMutation(
    $input: MarkFriendshipAsViewedInput!
  ) {
    markFriendshipAsViewed(input: $input) {
      friendship {
        viewed
      }
    }
  }
`;

function commit(environment, input) {
  return commitMutation(environment, {
    mutation,
    variables: { input }
  });
}

export default { commit };
