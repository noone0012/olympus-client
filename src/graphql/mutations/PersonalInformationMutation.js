import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation PersonalInformationMutation($input: EditUserInput!) {
    editUser(input: $input) {
      viewer {
        ...PersonalInformation_viewer
      }
    }
  }
`;

function commit(environment, input) {
  return commitMutation(environment, {
    mutation,
    variables: { input }
  });
}

export default { commit };
