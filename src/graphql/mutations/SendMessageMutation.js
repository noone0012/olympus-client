import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';
import { ConnectionHandler } from 'relay-runtime';

const mutation = graphql`
  mutation SendMessageMutation($input: SendMessageInput!) {
    sendMessage(input: $input) {
      message {
        ...Message_message
      }
    }
  }
`;

function commit(environment, input, rootId) {
  return commitMutation(environment, {
    mutation,
    variables: { input },
    updater(store) {
      const payload = store.getRootField('sendMessage');

      // if received an error
      if (!payload) {
        return;
      }

      const conn = ConnectionHandler.getConnection(
        store.get(rootId),
        'Chat_messages',
        { userId: input.receiverId }
      );

      const node = payload.getLinkedRecord('message');

      const newMessageEdge = ConnectionHandler.createEdge(
        store,
        conn,
        node,
        'MessageEdge'
      );

      ConnectionHandler.insertEdgeAfter(conn, newMessageEdge);
    }
  });
}

export default { commit };
