import { graphql } from 'react-relay';
import commitMutation from 'relay-commit-mutation-promise';

const mutation = graphql`
  mutation SendTypingNotificationMutation(
    $input: SendTypingNotificationInput!
  ) {
    sendTypingNotification(input: $input) {
      success
    }
  }
`;

function commit(environment, input, rootId) {
  return commitMutation(environment, {
    mutation,
    variables: { input }
  });
}

export default { commit };
