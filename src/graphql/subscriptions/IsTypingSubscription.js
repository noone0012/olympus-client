import { graphql, requestSubscription } from 'react-relay';

const isTypingSubscription = graphql`
  subscription IsTypingSubscription($input: IsTypingInput!) {
    isTyping(input: $input) {
      isTyping
    }
  }
`;

function request(environment, input, onNext) {
  const subscriptionConfig = {
    subscription: isTypingSubscription,
    variables: { input },
    onNext() {
      onNext();
    }
  };

  return requestSubscription(environment, subscriptionConfig);
}

export default { request };
