import { graphql, requestSubscription } from 'react-relay';
import { ConnectionHandler } from 'relay-runtime';

const newMessageSubscription = graphql`
  subscription NewMessageSubscription($input: NewMessageInput!) {
    newMessage(input: $input) {
      message {
        ...Message_message
      }
    }
  }
`;

function request(environment, input, rootId, onNext) {
  const subscriptionConfig = {
    subscription: newMessageSubscription,
    variables: { input },
    onNext,
    updater(store) {
      const payload = store.getRootField('newMessage');

      // if received an error
      if (!payload) {
        return;
      }

      const conn = ConnectionHandler.getConnection(
        store.get(rootId),
        'Chat_messages',
        { userId: input.userId }
      );

      const node = payload.getLinkedRecord('message');

      const newMessageEdge = ConnectionHandler.createEdge(
        store,
        conn,
        node,
        'MessageEdge'
      );

      ConnectionHandler.insertEdgeAfter(conn, newMessageEdge);
    }
  };

  return requestSubscription(environment, subscriptionConfig);
}

export default { request };
