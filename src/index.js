import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Resolver } from 'found-relay';
import { createConnectedRouter, createRender } from 'found';

import 'babel-polyfill';
import 'semantic-ui-css/semantic.min.css';
import '../node_modules/font-awesome/css/font-awesome.min.css';

import './libs/bootstrap-select';

import store from './store';
import environment from './graphql/environment';

const Router = createConnectedRouter({
  render: createRender({})
});

ReactDOM.render(
  <Provider store={store}>
    <Router resolver={new Resolver(environment)} />
  </Provider>,
  document.getElementById('root')
);
