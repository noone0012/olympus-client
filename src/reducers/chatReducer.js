const chatReducer = (state = { open: false, user: {}, viewer: {} }, action) => {
  switch (action.type) {
    case 'TOGGLE_CHAT_STATE':
      const { open, user = {}, viewer = {} } = action.payload;

      if (state.open === open && state.userId === user.id) {
        return state;
      }

      return {
        ...state,
        open,
        user,
        viewer
      };
    default:
      return state;
  }
};

export default chatReducer;
