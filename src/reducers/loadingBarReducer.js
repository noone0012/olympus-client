const loadingBarReducer = (state = { show: false }, action) => {
  switch (action.type) {
    case 'TOGGLE_BAR_STATE':
      if (state.show === action.payload.show) {
        return state;
      }

      return { ...state, show: action.payload.show };
    default:
      return state;
  }
};

export default loadingBarReducer;
