import { combineReducers } from 'redux';
import { foundReducer } from 'found';

import loadingBarReducer from './loadingBarReducer';
import validationMessageReducer from './validationMessageReducer';
import chatReducer from './chatReducer';

const rootReducer = combineReducers({
  found: foundReducer,
  loadingBar: loadingBarReducer,
  validationMessage: validationMessageReducer,
  chat: chatReducer
});

export default rootReducer;
