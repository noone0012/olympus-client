const validationMessageReducer = (
  state = { open: false, message: null },
  action
) => {
  switch (action.type) {
    case 'TOGGLE_VALIDATION_MESSAGE_STATE':
      if (state.open === action.payload.open) {
        return state;
      }

      return {
        ...state,
        open: !state.open,
        message: action.payload.message
      };
    default:
      return state;
  }
};

export default validationMessageReducer;
