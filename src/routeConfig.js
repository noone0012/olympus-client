import { Route, makeRouteConfig, Redirect, RedirectException } from 'found';
import React from 'react';

import isLoggedIn from './utils/isLoggedIn';

// root
import App from './App';

// Auth Page
import LandingPage from './components/LandingPage';
import Register from './components/LandingPage/Register';
import Login from './components/LandingPage/Login';

// Olympus
import Olympus from './components/Olympus';

// Newsfeed
import Newsfeed from './components/Newsfeed';

// Profile
import Profile from './components/Profile';
import Timeline from './components/Profile/Timeline';
import Friends from './components/Profile/Friends';
import About from './components/Profile/About';
import Photos from './components/Profile/Photos';
import PhotosPage from './components/Profile/Photos/PhotosPage';
import AlbumsPage from './components/Profile/Photos/AlbumsPage';
import Videos from './components/Profile/Videos';

// Dashboard
import Dashboard from './components/Dashboard';
import PersonalInformation from './components/Dashboard/PersonalInformation';
import AccountSettings from './components/Dashboard/AccountSettings';
import ChangePassword from './components/Dashboard/ChangePassword';
import EducationAndEmployment from './components/Dashboard/EducationAndEmployment';
import Notifications from './components/Dashboard/Notifications';
import Messages from './components/Dashboard/Messages';
import Dialog from './components/Dashboard/Messages/Dialog';
import FriendRequests from './components/Dashboard/FriendRequests';

// queries
import * as queries from './routeQueries';
import store from './store';

const toggleLoader = show => {
  store.dispatch({ type: 'TOGGLE_BAR_STATE', payload: { show } });
};

const privateRoute = (Component, shouldRedirect) => ({ props }) => {
  if (isLoggedIn()) {
    if (!props) {
      toggleLoader(true);
      return undefined;
    }

    if (shouldRedirect && shouldRedirect(props)) {
      throw new RedirectException({ pathname: '/' });
    }

    toggleLoader(false);
    return <Component {...props} />;
  }

  throw new RedirectException({ pathname: '/login' });
};

const publicRoute = Component => ({ props }) => {
  if (!isLoggedIn()) {
    if (!props) {
      toggleLoader(true);
      return undefined;
    }

    toggleLoader(false);
    return <Component {...props} />;
  }

  throw new RedirectException({ pathname: '/' });
};

const wheatherRoutes = (C1, C2) => ({ props }) => {
  if (!isLoggedIn()) {
    return <C1 {...props} />;
  }

  if (!props) {
    toggleLoader(true);
    return undefined;
  }

  toggleLoader(false);
  return <C2 {...props} />;
};

export default makeRouteConfig(
  <Route path="/" Component={App}>
    <Route
      render={wheatherRoutes(LandingPage, Olympus)}
      getQuery={() => isLoggedIn() && queries.OlympusQuery}
    >
      <Route
        render={wheatherRoutes(Register, Newsfeed)}
        getQuery={() => isLoggedIn() && queries.NewsfeedQuery}
      />
      <Route path="login" render={publicRoute(Login)} />
      <Route
        path="/:id"
        render={privateRoute(Profile, props => !props.user)}
        query={queries.ProfileQuery}
      >
        <Route render={privateRoute(Timeline)} query={queries.TimelineQuery} />
        <Route
          path="about"
          render={privateRoute(About)}
          query={queries.AboutQuery}
        />
        <Route
          path="friends/:fullName?"
          render={privateRoute(Friends)}
          query={queries.FriendsQuery}
        />
        <Route
          path="photos"
          render={privateRoute(Photos)}
          query={queries.PhotosQuery}
        >
          <Route render={privateRoute(PhotosPage)} />
          <Route
            path="albums"
            render={privateRoute(AlbumsPage)}
            query={queries.AlbumsPageQuery}
          />
        </Route>
        <Route path="videos" render={privateRoute(Videos)} />
      </Route>
      <Route
        path="dashboard"
        Component={Dashboard}
        query={queries.DashboardQuery}
      >
        <Route
          path="personal-information"
          render={privateRoute(PersonalInformation)}
          query={queries.PersonalInformationQuery}
        />
        <Route
          path="account-settings"
          render={privateRoute(AccountSettings)}
          query={queries.AccountSettingsQuery}
        />
        <Route path="change-password" render={privateRoute(ChangePassword)} />
        <Route
          path="education-and-employment"
          render={privateRoute(EducationAndEmployment)}
          query={queries.EducationAndEmploymentQuery}
        />
        <Route path="notifications" render={privateRoute(Notifications)} />
        <Route path="messages" render={privateRoute(Dialog)} />
        <Route
          path="friend-requests"
          render={privateRoute(FriendRequests)}
          query={queries.FriendRequestsQuery}
        />
      </Route>
    </Route>
    <Redirect from="*" to="/" />
  </Route>
);
