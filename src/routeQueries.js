import { graphql } from 'react-relay';

export const OlympusQuery = graphql`
  query routeQueries_Olympus_Query {
    viewer {
      ...RightSidebar_viewer
      ...LeftSidebar_viewer
      ...Header_viewer
    }

    root {
      ...Chat_root
      ...FoundUsers_root
    }
  }
`;

export const NewsfeedQuery = graphql`
  query routeQueries_Newsfeed_Query {
    viewer {
      ...Newsfeed_viewer
    }
  }
`;

export const ProfileQuery = graphql`
  query routeQueries_Profile_Query($id: ID!) {
    user: node(id: $id) {
      ...TopHeader_user
    }
  }
`;

export const DashboardQuery = graphql`
  query routeQueries_Dashboard_Query {
    viewer {
      ...Dashboard_viewer
    }
  }
`;

export const PersonalInformationQuery = graphql`
  query routeQueries_PersonalInformation_Query {
    viewer {
      ...PersonalInformation_viewer
    }
    root {
      ...PersonalInformation_root
    }
  }
`;

export const AccountSettingsQuery = graphql`
  query routeQueries_AccountSettings_Query {
    viewer {
      ...AccountSettings_viewer
    }
  }
`;

export const EducationAndEmploymentQuery = graphql`
  query routeQueries_EducationAndEmployment_Query {
    viewer {
      ...EducationAndEmployment_viewer
    }
  }
`;

export const FriendRequestsQuery = graphql`
  query routeQueries_FriendRequests_Query {
    viewer {
      ...FriendRequests_viewer
    }
  }
`;

export const TimelineQuery = graphql`
  query routeQueries_Timeline_Query($id: ID!) {
    user: node(id: $id) {
      ...Timeline_user
    }
  }
`;

export const AboutQuery = graphql`
  query routeQueries_About_Query($id: ID!) {
    user: node(id: $id) {
      ...About_user
    }
  }
`;

export const FriendsQuery = graphql`
  query routeQueries_Friends_Query($id: ID!, $fullName: String) {
    user: node(id: $id) {
      ...Friends_user @arguments(fullName: $fullName)
    }
  }
`;

export const PhotosQuery = graphql`
  query routeQueries_Photos_Query($id: ID!) {
    user: node(id: $id) {
      ...Photos_user
    }
  }
`;

export const AlbumsPageQuery = graphql`
  query routeQueries_AlbumsPage_Query($id: ID!) {
    user: node(id: $id) {
      ...AlbumsPage_user
    }
  }
`;
