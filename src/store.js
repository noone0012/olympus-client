import { createStore, compose } from 'redux';

import rootReducer from './reducers/rootReducer';
import routeConfig from './routeConfig';

import {
  Actions as FarceActions,
  BrowserProtocol,
  createHistoryEnhancer,
  queryMiddleware
} from 'farce';
import { createMatchEnhancer, Matcher } from 'found';

const store = createStore(
  rootReducer,
  compose(
    createHistoryEnhancer({
      protocol: new BrowserProtocol(),
      middlewares: [queryMiddleware]
    }),
    createMatchEnhancer(new Matcher(routeConfig))
  )
);

store.dispatch(FarceActions.init());

export default store;
