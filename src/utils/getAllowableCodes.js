export default () => {
  const codes = [8, 192, 189, 187, 32, 219, 221, 220, 186, 222, 188, 190, 191];

  // letters
  for (let i = 65; i <= 90; i++) {
    codes.push(i);
  }

  // numbers
  for (let i = 48; i <= 57; i++) {
    codes.push(i);
  }

  // right bar
  for (let i = 96; i <= 111; i++) {
    codes.push(i);
  }

  return codes;
};
