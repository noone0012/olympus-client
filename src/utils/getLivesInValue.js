export default (city, country) => {
  if (!city || !country) {
    return null;
  }

  return `${city.name}, ${country.name}`;
};
