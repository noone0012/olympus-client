export default () => ({
  accessToken: localStorage.getItem('acst'),
  refreshToken: localStorage.getItem('rfrt')
});
