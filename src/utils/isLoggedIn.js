import jwtDecode from 'jwt-decode';

import getTokens from './getTokens';

export default () => {
  const { accessToken } = getTokens();

  try {
    jwtDecode(accessToken);
    return true;
  } catch (e) {
    return false;
  }
};
