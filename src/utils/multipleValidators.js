const multipleValidators = validate => {
  if (!Array.isArray(validate)) {
    return validate;
  }

  return value => {
    for (const i in validate) {
      const validator = validate[i];
      const err = validator(value);

      if (err) {
        return err;
      }
    }
  };
};

export default multipleValidators;
