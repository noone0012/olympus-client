export const gender = [
  { text: 'Male', value: 'MALE' },
  { text: 'Female', value: 'FEMALE' }
];

export const status = [
  { text: 'Single', value: 'SINGLE' },
  { text: 'In a relationship', value: 'IN_A_RELATIONSHIP' },
  { text: 'Engaged', value: 'ENGAGED' },
  { text: 'Married', value: 'MARRIED' }
];
