const setFormErrors = (state, formApi) => {
  if (!state) {
    return;
  }

  for (let key in state) {
    formApi.setError(key, state[key][0]);
  }
};

export default setFormErrors;
