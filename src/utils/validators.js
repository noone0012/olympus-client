import { isEmail, isURL } from 'validator';

export const required = value => (value ? null : 'This field is required');

export const maxLength = max => value =>
  value && value.length > max ? `Must be ${max} characters or less` : null;

export const minLength = min => value =>
  value && value.length < min ? `Must be ${min} characters or more` : null;

export const email = value =>
  value && !isEmail(value) ? 'Invalid email address' : null;

export const isUrl = value => (value && !isURL(value) ? 'Invalid url' : null);

export const shouldMatch = confirmValue => value =>
  confirmValue && value && confirmValue !== value
    ? 'Passwords should match'
    : null;
